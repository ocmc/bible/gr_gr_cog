gr_gr_cog~EZE~C047~001~καὶ εἰσήγαγέν με ἐπὶ τὰ πρόθυρα τοῦ οἴκου καὶ ἰδοὺ ὕδωρ ἐξεπορεύετο ὑποκάτωθεν τοῦ αἰθρίου κατ’ ἀνατολάς ὅτι τὸ πρόσωπον τοῦ οἴκου ἔβλεπεν κατ’ ἀνατολάς καὶ τὸ ὕδωρ κατέβαινεν ἀπὸ τοῦ κλίτους τοῦ δεξιοῦ ἀπὸ νότου ἐπὶ τὸ θυσιαστήριον
gr_gr_cog~EZE~C047~002~καὶ ἐξήγαγέν με κατὰ τὴν ὁδὸν τῆς πύλης τῆς πρὸς βορρᾶν καὶ περιήγαγέν με τὴν ὁδὸν ἔξωθεν πρὸς τὴν πύλην τῆς αὐλῆς τῆς βλεπούσης κατ’ ἀνατολάς καὶ ἰδοὺ τὸ ὕδωρ κατεφέρετο ἀπὸ τοῦ κλίτους τοῦ δεξιοῦ
gr_gr_cog~EZE~C047~003~καθὼς ἔξοδος ἀνδρὸς ἐξ ἐναντίας καὶ μέτρον ἐν τῇ χειρὶ αὐτοῦ καὶ διεμέτρησεν χιλίους ἐν τῷ μέτρῳ καὶ διῆλθεν ἐν τῷ ὕδατι ὕδωρ ἀφέσεως
gr_gr_cog~EZE~C047~004~καὶ διεμέτρησεν χιλίους καὶ διῆλθεν ἐν τῷ ὕδατι ὕδωρ ἕως τῶν μηρῶν καὶ διεμέτρησεν χιλίους καὶ διῆλθεν ὕδωρ ἕως ὀσφύος
gr_gr_cog~EZE~C047~005~καὶ διεμέτρησεν χιλίους καὶ οὐκ ἠδύνατο διελθεῖν ὅτι ἐξύβριζεν τὸ ὕδωρ ὡς ῥοῖζος χειμάρρου ὃν οὐ διαβήσονται
gr_gr_cog~EZE~C047~006~καὶ εἶπεν πρός με εἰ ἑώρακας υἱὲ ἀνθρώπου καὶ ἤγαγέν με ἐπὶ τὸ χεῖλος τοῦ ποταμοῦ
gr_gr_cog~EZE~C047~007~ἐν τῇ ἐπιστροφῇ μου καὶ ἰδοὺ ἐπὶ τοῦ χείλους τοῦ ποταμοῦ δένδρα πολλὰ σφόδρα ἔνθεν καὶ ἔνθεν
gr_gr_cog~EZE~C047~008~καὶ εἶπεν πρός με τὸ ὕδωρ τοῦτο τὸ ἐκπορευόμενον εἰς τὴν Γαλιλαίαν τὴν πρὸς ἀνατολὰς καὶ κατέβαινεν ἐπὶ τὴν Ἀραβίαν καὶ ἤρχετο ἕως ἐπὶ τὴν θάλασσαν ἐπὶ τὸ ὕδωρ τῆς διεκβολῆς καὶ ὑγιάσει τὰ ὕδατα
gr_gr_cog~EZE~C047~009~καὶ ἔσται πᾶσα ψυχὴ τῶν ζῴων τῶν ἐκζεόντων ἐπὶ πάντα ἐφ’ ἃ ἂν ἐπέλθῃ ἐκεῖ ὁ ποταμός ζήσεται καὶ ἔσται ἐκεῖ ἰχθὺς πολὺς σφόδρα ὅτι ἥκει ἐκεῖ τὸ ὕδωρ τοῦτο καὶ ὑγιάσει καὶ ζήσεται πᾶν ἐφ’ ὃ ἂν ἐπέλθῃ ὁ ποταμὸς ἐκεῖ ζήσεται
gr_gr_cog~EZE~C047~010~καὶ στήσονται ἐκεῖ ἁλεεῖς ἀπὸ Αινγαδιν ἕως Αιναγαλιμ ψυγμὸς σαγηνῶν ἔσται καθ’ αὑτὴν ἔσται καὶ οἱ ἰχθύες αὐτῆς ὡς οἱ ἰχθύες τῆς θαλάσσης τῆς μεγάλης πλῆθος πολὺ σφόδρα
gr_gr_cog~EZE~C047~011~καὶ ἐν τῇ διεκβολῇ αὐτοῦ καὶ ἐν τῇ ἐπιστροφῇ αὐτοῦ καὶ ἐν τῇ ὑπεράρσει αὐτοῦ οὐ μὴ ὑγιάσωσιν εἰς ἅλας δέδονται
gr_gr_cog~EZE~C047~012~καὶ ἐπὶ τοῦ ποταμοῦ ἀναβήσεται ἐπὶ τοῦ χείλους αὐτοῦ ἔνθεν καὶ ἔνθεν πᾶν ξύλον βρώσιμον οὐ μὴ παλαιωθῇ ἐπ’ αὐτοῦ οὐδὲ μὴ ἐκλίπῃ ὁ καρπὸς αὐτοῦ τῆς καινότητος αὐτοῦ πρωτοβολήσει διότι τὰ ὕδατα αὐτῶν ἐκ τῶν ἁγίων ταῦτα ἐκπορεύεται καὶ ἔσται ὁ καρπὸς αὐτῶν εἰς βρῶσιν καὶ ἀνάβασις αὐτῶν εἰς ὑγίειαν
gr_gr_cog~EZE~C047~013~τάδε λέγει κύριος θεός ταῦτα τὰ ὅρια κατακληρονομήσετε τῆς γῆς ταῖς δώδεκα φυλαῖς τῶν υἱῶν Ισραηλ πρόσθεσις σχοινίσματος
gr_gr_cog~EZE~C047~014~καὶ κατακληρονομήσετε αὐτὴν ἕκαστος καθὼς ὁ ἀδελφὸς αὐτοῦ εἰς ἣν ἦρα τὴν χεῖρά μου τοῦ δοῦναι αὐτὴν τοῖς πατράσιν αὐτῶν καὶ πεσεῖται ἡ γῆ αὕτη ὑμῖν ἐν κληρονομίᾳ
gr_gr_cog~EZE~C047~015~καὶ ταῦτα τὰ ὅρια τῆς γῆς πρὸς βορρᾶν ἀπὸ τῆς θαλάσσης τῆς μεγάλης τῆς καταβαινούσης καὶ περισχιζούσης τῆς εἰσόδου Ημαθ Σεδδαδα
gr_gr_cog~EZE~C047~016~Βηρωθα Σεβραιμ Ηλιαμ ἀνὰ μέσον ὁρίων Δαμασκοῦ καὶ ἀνὰ μέσον ὁρίων Ημαθ αὐλὴ τοῦ Σαυναν αἵ εἰσιν ἐπάνω τῶν ὁρίων Αυρανίτιδος
gr_gr_cog~EZE~C047~017~ταῦτα τὰ ὅρια ἀπὸ τῆς θαλάσσης ἀπὸ τῆς αὐλῆς τοῦ Αιναν ὅρια Δαμασκοῦ καὶ τὰ πρὸς βορρᾶν
gr_gr_cog~EZE~C047~018~καὶ τὰ πρὸς ἀνατολὰς ἀνὰ μέσον τῆς Αυρανίτιδος καὶ ἀνὰ μέσον Δαμασκοῦ καὶ ἀνὰ μέσον τῆς Γαλααδίτιδος καὶ ἀνὰ μέσον τῆς γῆς τοῦ Ισραηλ ὁ Ιορδάνης διορίζει ἐπὶ τὴν θάλασσαν τὴν πρὸς ἀνατολὰς Φοινικῶνος ταῦτα τὰ πρὸς ἀνατολάς
gr_gr_cog~EZE~C047~019~καὶ τὰ πρὸς νότον καὶ λίβα ἀπὸ Θαιμαν καὶ Φοινικῶνος ἕως ὕδατος Μαριμωθ Καδης παρεκτεῖνον ἐπὶ τὴν θάλασσαν τὴν μεγάλην τοῦτο τὸ μέρος νότος καὶ λίψ
gr_gr_cog~EZE~C047~020~τοῦτο τὸ μέρος τῆς θαλάσσης τῆς μεγάλης ὁρίζει ἕως κατέναντι τῆς εἰσόδου Ημαθ ἕως εἰσόδου αὐτοῦ ταῦτά ἐστιν τὰ πρὸς θάλασσαν Ημαθ
gr_gr_cog~EZE~C047~021~καὶ διαμερίσετε τὴν γῆν ταύτην αὐτοῖς ταῖς φυλαῖς τοῦ Ισραηλ
gr_gr_cog~EZE~C047~022~βαλεῖτε αὐτὴν ἐν κλήρῳ ὑμῖν καὶ τοῖς προσηλύτοις τοῖς παροικοῦσιν ἐν μέσῳ ὑμῶν οἵτινες ἐγέννησαν υἱοὺς ἐν μέσῳ ὑμῶν καὶ ἔσονται ὑμῖν ὡς αὐτόχθονες ἐν τοῖς υἱοῖς τοῦ Ισραηλ μεθ’ ὑμῶν φάγονται ἐν κληρονομίᾳ ἐν μέσῳ τῶν φυλῶν τοῦ Ισραηλ
gr_gr_cog~EZE~C047~023~καὶ ἔσονται ἐν φυλῇ προσηλύτων ἐν τοῖς προσηλύτοις τοῖς μετ’ αὐτῶν ἐκεῖ δώσετε κληρονομίαν αὐτοῖς λέγει κύριος θεός
