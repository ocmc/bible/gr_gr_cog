gr_gr_cog~CH2~C013~001~ἐν τῷ ὀκτωκαιδεκάτῳ ἔτει τῆς βασιλείας Ιεροβοαμ ἐβασίλευσεν Αβια ἐπὶ Ιουδαν
gr_gr_cog~CH2~C013~002~ἔτη τρία ἐβασίλευσεν ἐν Ιερουσαλημ καὶ ὄνομα τῇ μητρὶ αὐτοῦ Μααχα θυγάτηρ Ουριηλ ἀπὸ Γαβαων καὶ πόλεμος ἦν ἀνὰ μέσον Αβια καὶ ἀνὰ μέσον Ιεροβοαμ
gr_gr_cog~CH2~C013~003~καὶ παρετάξατο Αβια τὸν πόλεμον ἐν δυνάμει πολεμισταῖς δυνάμεως τετρακοσίαις χιλιάσιν ἀνδρῶν δυνατῶν καὶ Ιεροβοαμ παρετάξατο πρὸς αὐτὸν πόλεμον ἐν ὀκτακοσίαις χιλιάσιν δυνατοὶ πολεμισταὶ δυνάμεως
gr_gr_cog~CH2~C013~004~καὶ ἀνέστη Αβια ἀπὸ τοῦ ὄρους Σομορων ὅ ἐστιν ἐν τῷ ὄρει Εφραιμ καὶ εἶπεν ἀκούσατε Ιεροβοαμ καὶ πᾶς Ισραηλ
gr_gr_cog~CH2~C013~005~οὐχ ὑμῖν γνῶναι ὅτι κύριος ὁ θεὸς Ισραηλ ἔδωκεν βασιλείαν ἐπὶ τὸν Ισραηλ εἰς τὸν αἰῶνα τῷ Δαυιδ καὶ τοῖς υἱοῖς αὐτοῦ διαθήκην ἁλός
gr_gr_cog~CH2~C013~006~καὶ ἀνέστη Ιεροβοαμ ὁ τοῦ Ναβατ ὁ παῖς Σαλωμων τοῦ Δαυιδ καὶ ἀπέστη ἀπὸ τοῦ κυρίου αὐτοῦ
gr_gr_cog~CH2~C013~007~καὶ συνήχθησαν πρὸς αὐτὸν ἄνδρες λοιμοὶ υἱοὶ παράνομοι καὶ ἀντέστη πρὸς Ροβοαμ τὸν τοῦ Σαλωμων καὶ Ροβοαμ ἦν νεώτερος καὶ δειλὸς τῇ καρδίᾳ καὶ οὐκ ἀντέστη κατὰ πρόσωπον αὐτοῦ
gr_gr_cog~CH2~C013~008~καὶ νῦν λέγετε ὑμεῖς ἀντιστῆναι κατὰ πρόσωπον βασιλείας κυρίου διὰ χειρὸς υἱῶν Δαυιδ καὶ ὑμεῖς πλῆθος πολύ καὶ μεθ’ ὑμῶν μόσχοι χρυσοῖ οὓς ἐποίησεν ὑμῖν Ιεροβοαμ εἰς θεούς
gr_gr_cog~CH2~C013~009~ἦ οὐκ ἐξεβάλετε τοὺς ἱερεῖς κυρίου τοὺς υἱοὺς Ααρων καὶ τοὺς Λευίτας καὶ ἐποιήσατε ἑαυτοῖς ἱερεῖς ἐκ τοῦ λαοῦ τῆς γῆς πᾶς ὁ προσπορευόμενος πληρῶσαι τὰς χεῖρας ἐν μόσχῳ ἐκ βοῶν καὶ κριοῖς ἑπτὰ καὶ ἐγίνετο εἰς ἱερέα τῷ μὴ ὄντι θεῷ
gr_gr_cog~CH2~C013~010~καὶ ἡμεῖς κύριον τὸν θεὸν ἡμῶν οὐκ ἐγκατελίπομεν καὶ οἱ ἱερεῖς αὐτοῦ λειτουργοῦσιν τῷ κυρίῳ οἱ υἱοὶ Ααρων καὶ οἱ Λευῖται ἐν ταῖς ἐφημερίαις αὐτῶν
gr_gr_cog~CH2~C013~011~θυμιῶσιν τῷ κυρίῳ ὁλοκαυτώματα πρωὶ καὶ δείλης καὶ θυμίαμα συνθέσεως καὶ προθέσεις ἄρτων ἐπὶ τῆς τραπέζης τῆς καθαρᾶς καὶ ἡ λυχνία ἡ χρυσῆ καὶ οἱ λυχνοὶ τῆς καύσεως ἀνάψαι δείλης ὅτι φυλάσσομεν ἡμεῖς τὰς φυλακὰς κυρίου τοῦ θεοῦ τῶν πατέρων ἡμῶν καὶ ὑμεῖς ἐγκατελίπετε αὐτόν
gr_gr_cog~CH2~C013~012~καὶ ἰδοὺ μεθ’ ἡμῶν ἐν ἀρχῇ κύριος καὶ οἱ ἱερεῖς αὐτοῦ καὶ αἱ σάλπιγγες τῆς σημασίας τοῦ σημαίνειν ἐφ’ ὑμᾶς οἱ υἱοὶ τοῦ Ισραηλ πολεμήσετε πρὸς κύριον θεὸν τῶν πατέρων ἡμῶν ὅτι οὐκ εὐοδωθήσεται ὑμῖν
gr_gr_cog~CH2~C013~013~καὶ Ιεροβοαμ ἀπέστρεψεν τὸ ἔνεδρον ἐλθεῖν αὐτῶν ἐκ τῶν ὄπισθεν καὶ ἐγένετο ἔμπροσθεν Ιουδα καὶ τὸ ἔνεδρον ἐκ τῶν ὄπισθεν
gr_gr_cog~CH2~C013~014~καὶ ἀπέστρεψεν Ιουδας καὶ ἰδοὺ αὐτοῖς ὁ πόλεμος ἐκ τῶν ἔμπροσθεν καὶ ἐκ τῶν ὄπισθεν καὶ ἐβόησαν πρὸς κύριον καὶ οἱ ἱερεῖς ἐσάλπισαν ταῖς σάλπιγξιν
gr_gr_cog~CH2~C013~015~καὶ ἐβόησαν ἄνδρες Ιουδα καὶ ἐγένετο ἐν τῷ βοᾶν ἄνδρας Ιουδα καὶ κύριος ἐπάταξεν τὸν Ιεροβοαμ καὶ τὸν Ισραηλ ἐναντίον Αβια καὶ Ιουδα
gr_gr_cog~CH2~C013~016~καὶ ἔφυγον οἱ υἱοὶ Ισραηλ ἀπὸ προσώπου Ιουδα καὶ παρέδωκεν αὐτοὺς κύριος εἰς τὰς χεῖρας αὐτῶν
gr_gr_cog~CH2~C013~017~καὶ ἐπάταξεν ἐν αὐτοῖς Αβια καὶ ὁ λαὸς αὐτοῦ πληγὴν μεγάλην καὶ ἔπεσον τραυματίαι ἀπὸ Ισραηλ πεντακόσιαι χιλιάδες ἄνδρες δυνατοί
gr_gr_cog~CH2~C013~018~καὶ ἐταπεινώθησαν οἱ υἱοὶ Ισραηλ ἐν τῇ ἡμέρᾳ ἐκείνῃ καὶ κατίσχυσαν οἱ υἱοὶ Ιουδα ὅτι ἤλπισαν ἐπὶ κύριον θεὸν τῶν πατέρων αὐτῶν
gr_gr_cog~CH2~C013~019~καὶ κατεδίωξεν Αβια ὀπίσω Ιεροβοαμ καὶ προκατελάβετο παρ’ αὐτοῦ πόλεις τὴν Βαιθηλ καὶ τὰς κώμας αὐτῆς καὶ τὴν Ισανα καὶ τὰς κώμας αὐτῆς καὶ τὴν Εφρων καὶ τὰς κώμας αὐτῆς
gr_gr_cog~CH2~C013~020~καὶ οὐκ ἔσχεν ἰσχὺν Ιεροβοαμ ἔτι πάσας τὰς ἡμέρας Αβια καὶ ἐπάταξεν αὐτὸν κύριος καὶ ἐτελεύτησεν
gr_gr_cog~CH2~C013~021~καὶ κατίσχυσεν Αβια καὶ ἔλαβεν ἑαυτῷ γυναῖκας δέκα τέσσαρας καὶ ἐγέννησεν υἱοὺς εἴκοσι δύο καὶ θυγατέρας δέκα ἕξ
gr_gr_cog~CH2~C013~022~καὶ οἱ λοιποὶ λόγοι Αβια καὶ αἱ πράξεις αὐτοῦ καὶ οἱ λόγοι αὐτοῦ γεγραμμένοι ἐπὶ βιβλίῳ τοῦ προφήτου Αδδω
gr_gr_cog~CH2~C013~023~καὶ ἀπέθανεν Αβια μετὰ τῶν πατέρων αὐτοῦ καὶ ἔθαψαν αὐτὸν ἐν πόλει Δαυιδ καὶ ἐβασίλευσεν Ασα υἱὸς αὐτοῦ ἀντ’ αὐτοῦ ἐν ταῖς ἡμέραις Ασα ἡσύχασεν ἡ γῆ Ιουδα ἔτη δέκα
