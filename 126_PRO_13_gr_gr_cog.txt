gr_gr_cog~PRO~C013~001~υἱὸς πανοῦργος ὑπήκοος πατρί υἱὸς δὲ ἀνήκοος ἐν ἀπωλείᾳ
gr_gr_cog~PRO~C013~002~ἀπὸ καρπῶν δικαιοσύνης φάγεται ἀγαθός ψυχαὶ δὲ παρανόμων ὀλοῦνται ἄωροι
gr_gr_cog~PRO~C013~003~ὃς φυλάσσει τὸ ἑαυτοῦ στόμα τηρεῖ τὴν ἑαυτοῦ ψυχήν ὁ δὲ προπετὴς χείλεσιν πτοήσει ἑαυτόν
gr_gr_cog~PRO~C013~004~ἐν ἐπιθυμίαις ἐστὶν πᾶς ἀεργός χεῖρες δὲ ἀνδρείων ἐν ἐπιμελείᾳ
gr_gr_cog~PRO~C013~005~λόγον ἄδικον μισεῖ δίκαιος ἀσεβὴς δὲ αἰσχύνεται καὶ οὐχ ἕξει παρρησίαν
gr_gr_cog~PRO~C013~006~δικαιοσύνη φυλάσσει ἀκάκους τοὺς δὲ ἀσεβεῖς φαύλους ποιεῖ ἁμαρτία
gr_gr_cog~PRO~C013~007~εἰσὶν οἱ πλουτίζοντες ἑαυτοὺς μηδὲν ἔχοντες καὶ εἰσὶν οἱ ταπεινοῦντες ἑαυτοὺς ἐν πολλῷ πλούτῳ
gr_gr_cog~PRO~C013~008~λύτρον ἀνδρὸς ψυχῆς ὁ ἴδιος πλοῦτος πτωχὸς δὲ οὐχ ὑφίσταται ἀπειλήν
gr_gr_cog~PRO~C013~009~φῶς δικαίοις διὰ παντός φῶς δὲ ἀσεβῶν σβέννυται
gr_gr_cog~PRO~C013~009a~ψυχαὶ δόλιαι πλανῶνται ἐν ἁμαρτίαις δίκαιοι δὲ οἰκτίρουσιν καὶ ἐλεῶσιν
gr_gr_cog~PRO~C013~010~κακὸς μεθ’ ὕβρεως πράσσει κακά οἱ δὲ ἑαυτῶν ἐπιγνώμονες σοφοί
gr_gr_cog~PRO~C013~011~ὕπαρξις ἐπισπουδαζομένη μετὰ ἀνομίας ἐλάσσων γίνεται ὁ δὲ συνάγων ἑαυτῷ μετ’ εὐσεβείας πληθυνθήσεται δίκαιος οἰκτίρει καὶ κιχρᾷ
gr_gr_cog~PRO~C013~012~κρείσσων ἐναρχόμενος βοηθῶν καρδίᾳ τοῦ ἐπαγγελλομένου καὶ εἰς ἐλπίδα ἄγοντος δένδρον γὰρ ζωῆς ἐπιθυμία ἀγαθή
gr_gr_cog~PRO~C013~013~ὃς καταφρονεῖ πράγματος καταφρονηθήσεται ὑπ’ αὐτοῦ ὁ δὲ φοβούμενος ἐντολήν οὗτος ὑγιαίνει
gr_gr_cog~PRO~C013~013a~υἱῷ δολίῳ οὐδὲν ἔσται ἀγαθόν οἰκέτῃ δὲ σοφῷ εὔοδοι ἔσονται πράξεις καὶ κατευθυνθήσεται ἡ ὁδὸς αὐτοῦ
gr_gr_cog~PRO~C013~014~νόμος σοφοῦ πηγὴ ζωῆς ὁ δὲ ἄνους ὑπὸ παγίδος θανεῖται
gr_gr_cog~PRO~C013~015~σύνεσις ἀγαθὴ δίδωσιν χάριν τὸ δὲ γνῶναι νόμον διανοίας ἐστὶν ἀγαθῆς ὁδοὶ δὲ καταφρονούντων ἐν ἀπωλείᾳ
gr_gr_cog~PRO~C013~016~πᾶς πανοῦργος πράσσει μετὰ γνώσεως ὁ δὲ ἄφρων ἐξεπέτασεν ἑαυτοῦ κακίαν
gr_gr_cog~PRO~C013~017~βασιλεὺς θρασὺς ἐμπεσεῖται εἰς κακά ἄγγελος δὲ πιστὸς ῥύσεται αὐτόν
gr_gr_cog~PRO~C013~018~πενίαν καὶ ἀτιμίαν ἀφαιρεῖται παιδεία ὁ δὲ φυλάσσων ἐλέγχους δοξασθήσεται
gr_gr_cog~PRO~C013~019~ἐπιθυμίαι εὐσεβῶν ἡδύνουσιν ψυχήν ἔργα δὲ ἀσεβῶν μακρὰν ἀπὸ γνώσεως
gr_gr_cog~PRO~C013~020~ὁ συμπορευόμενος σοφοῖς σοφὸς ἔσται ὁ δὲ συμπορευόμενος ἄφροσι γνωσθήσεται
gr_gr_cog~PRO~C013~021~ἁμαρτάνοντας καταδιώξεται κακά τοὺς δὲ δικαίους καταλήμψεται ἀγαθά
gr_gr_cog~PRO~C013~022~ἀγαθὸς ἀνὴρ κληρονομήσει υἱοὺς υἱῶν θησαυρίζεται δὲ δικαίοις πλοῦτος ἀσεβὼν
gr_gr_cog~PRO~C013~023~δίκαιοι ποιήσουσιν ἐν πλούτῳ ἔτη πολλά ἄδικοι δὲ ἀπολοῦνται συντόμως
gr_gr_cog~PRO~C013~024~ὃς φείδεται τῆς βακτηρίας μισεῖ τὸν υἱὸν αὐτοῦ ὁ δὲ ἀγαπῶν ἐπιμελῶς παιδεύει
gr_gr_cog~PRO~C013~025~δίκαιος ἔσθων ἐμπιπλᾷ τὴν ψυχὴν αὐτοῦ ψυχαὶ δὲ ἀσεβῶν ἐνδεεῖς
