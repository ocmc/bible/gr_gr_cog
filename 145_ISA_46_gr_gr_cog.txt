gr_gr_cog~ISA~C046~001~ἔπεσε Βηλ συνετρίβη Δαγων ἐγένετο τὰ γλυπτὰ αὐτῶν εἰς θηρία καὶ κτήνη αἴρετε αὐτὰ καταδεδεμένα ὡς φορτίον κοπιῶντι
gr_gr_cog~ISA~C046~002~καὶ πεινῶντι καὶ ἐκλελυμένῳ οὐκ ἰσχύοντι ἅμα οἳ οὐ δυνήσονται σωθῆναι ἀπὸ πολέμου αὐτοὶ δὲ αἰχμάλωτοι ἤχθησαν
gr_gr_cog~ISA~C046~003~ἀκούσατέ μου οἶκος τοῦ Ιακωβ καὶ πᾶν τὸ κατάλοιπον τοῦ Ισραηλ οἱ αἰρόμενοι ἐκ κοιλίας καὶ παιδευόμενοι ἐκ παιδίου
gr_gr_cog~ISA~C046~004~ἕως γήρους ἐγώ εἰμι καὶ ἕως ἂν καταγηράσητε ἐγώ εἰμι ἐγὼ ἀνέχομαι ὑμῶν ἐγὼ ἐποίησα καὶ ἐγὼ ἀνήσω ἐγὼ ἀναλήμψομαι καὶ σώσω ὑμᾶς
gr_gr_cog~ISA~C046~005~τίνι με ὡμοιώσατε ἴδετε τεχνάσασθε οἱ πλανώμενοι
gr_gr_cog~ISA~C046~006~οἱ συμβαλλόμενοι χρυσίον ἐκ μαρσιππίου καὶ ἀργύριον ἐν ζυγῷ στήσουσιν ἐν σταθμῷ καὶ μισθωσάμενοι χρυσοχόον ἐποίησαν χειροποίητα καὶ κύψαντες προσκυνοῦσιν αὐτοῖς
gr_gr_cog~ISA~C046~007~αἴρουσιν αὐτὸ ἐπὶ τῶν ὤμων καὶ πορεύονται ἐὰν δὲ θῶσιν αὐτό ἐπὶ τοῦ τόπου αὐτοῦ μένει οὐ μὴ κινηθῇ καὶ ὃς ἂν βοήσῃ πρὸς αὐτόν οὐ μὴ εἰσακούσῃ ἀπὸ κακῶν οὐ μὴ σώσῃ αὐτόν
gr_gr_cog~ISA~C046~008~μνήσθητε ταῦτα καὶ στενάξατε μετανοήσατε οἱ πεπλανημένοι ἐπιστρέψατε τῇ καρδίᾳ
gr_gr_cog~ISA~C046~009~καὶ μνήσθητε τὰ πρότερα ἀπὸ τοῦ αἰῶνος ὅτι ἐγώ εἰμι ὁ θεός καὶ οὐκ ἔστιν ἔτι πλὴν ἐμοῦ
gr_gr_cog~ISA~C046~010~ἀναγγέλλων πρότερον τὰ ἔσχατα πρὶν αὐτὰ γενέσθαι καὶ ἅμα συνετελέσθη καὶ εἶπα πᾶσά μου ἡ βουλὴ στήσεται καὶ πάντα ὅσα βεβούλευμαι ποιήσω
gr_gr_cog~ISA~C046~011~καλῶν ἀπ’ ἀνατολῶν πετεινὸν καὶ ἀπὸ γῆς πόρρωθεν περὶ ὧν βεβούλευμαι ἐλάλησα καὶ ἤγαγον ἔκτισα καὶ ἐποίησα ἤγαγον αὐτὸν καὶ εὐόδωσα τὴν ὁδὸν αὐτοῦ
gr_gr_cog~ISA~C046~012~ἀκούσατέ μου οἱ ἀπολωλεκότες τὴν καρδίαν οἱ μακρὰν ἀπὸ τῆς δικαιοσύνης
gr_gr_cog~ISA~C046~013~ἤγγισα τὴν δικαιοσύνην μου καὶ τὴν σωτηρίαν τὴν παρ’ ἐμοῦ οὐ βραδυνῶ δέδωκα ἐν Σιων σωτηρίαν τῷ Ισραηλ εἰς δόξασμα
