gr_gr_cog~JOB~C039~001~εἰ ἔγνως καιρὸν τοκετοῦ τραγελάφων πέτρας ἐφύλαξας δὲ ὠδῖνας ἐλάφων
gr_gr_cog~JOB~C039~002~ἠρίθμησας δὲ αὐτῶν μῆνας πλήρεις τοκετοῦ ὠδῖνας δὲ αὐτῶν ἔλυσας
gr_gr_cog~JOB~C039~003~ἐξέθρεψας δὲ αὐτῶν τὰ παιδία ἔξω φόβου ὠδῖνας αὐτῶν ἐξαποστελεῖς
gr_gr_cog~JOB~C039~004~ἀπορρήξουσιν τὰ τέκνα αὐτῶν πληθυνθήσονται ἐν γενήματι ἐξελεύσονται καὶ οὐ μὴ ἀνακάμψουσιν αὐτοῖς
gr_gr_cog~JOB~C039~005~τίς δέ ἐστιν ὁ ἀφεὶς ὄνον ἄγριον ἐλεύθερον δεσμοὺς δὲ αὐτοῦ τίς ἔλυσεν
gr_gr_cog~JOB~C039~006~ἐθέμην δὲ τὴν δίαιταν αὐτοῦ ἔρημον καὶ τὰ σκηνώματα αὐτοῦ ἁλμυρίδα
gr_gr_cog~JOB~C039~007~καταγελῶν πολυοχλίας πόλεως μέμψιν δὲ φορολόγου οὐκ ἀκούων
gr_gr_cog~JOB~C039~008~κατασκέψεται ὄρη νομὴν αὐτοῦ καὶ ὀπίσω παντὸς χλωροῦ ζητεῖ
gr_gr_cog~JOB~C039~009~βουλήσεται δέ σοι μονόκερως δουλεῦσαι ἢ κοιμηθῆναι ἐπὶ φάτνης σου
gr_gr_cog~JOB~C039~010~δήσεις δὲ ἐν ἱμᾶσι ζυγὸν αὐτοῦ ἢ ἑλκύσει σου αὔλακας ἐν πεδίῳ
gr_gr_cog~JOB~C039~011~πέποιθας δὲ ἐπ’ αὐτῷ ὅτι πολλὴ ἡ ἰσχὺς αὐτοῦ ἐπαφήσεις δὲ αὐτῷ τὰ ἔργα σου
gr_gr_cog~JOB~C039~012~πιστεύσεις δὲ ὅτι ἀποδώσει σοι τὸν σπόρον εἰσοίσει δέ σου τὸν ἅλωνα
gr_gr_cog~JOB~C039~013~πτέρυξ τερπομένων νεελασα ἐὰν συλλάβῃ ασιδα καὶ νεσσα
gr_gr_cog~JOB~C039~014~ὅτι ἀφήσει εἰς γῆν τὰ ᾠὰ αὐτῆς καὶ ἐπὶ χοῦν θάλψει
gr_gr_cog~JOB~C039~015~καὶ ἐπελάθετο ὅτι ποὺς σκορπιεῖ καὶ θηρία ἀγροῦ καταπατήσει
gr_gr_cog~JOB~C039~016~ἀπεσκλήρυνεν τὰ τέκνα αὐτῆς ὥστε μὴ ἑαυτῇ εἰς κενὸν ἐκοπίασεν ἄνευ φόβου
gr_gr_cog~JOB~C039~017~ὅτι κατεσιώπησεν αὐτῇ ὁ θεὸς σοφίαν καὶ οὐκ ἐμέρισεν αὐτῇ ἐν τῇ συνέσει
gr_gr_cog~JOB~C039~018~κατὰ καιρὸν ἐν ὕψει ὑψώσει καταγελάσεται ἵππου καὶ τοῦ ἐπιβάτου αὐτοῦ
gr_gr_cog~JOB~C039~019~ἦ σὺ περιέθηκας ἵππῳ δύναμιν ἐνέδυσας δὲ τραχήλῳ αὐτοῦ φόβον
gr_gr_cog~JOB~C039~020~περιέθηκας δὲ αὐτῷ πανοπλίαν δόξαν δὲ στηθέων αὐτοῦ τόλμῃ
gr_gr_cog~JOB~C039~021~ἀνορύσσων ἐν πεδίῳ γαυριᾷ ἐκπορεύεται δὲ εἰς πεδίον ἐν ἰσχύι
gr_gr_cog~JOB~C039~022~συναντῶν βέλει καταγελᾷ καὶ οὐ μὴ ἀποστραφῇ ἀπὸ σιδήρου
gr_gr_cog~JOB~C039~023~ἐπ’ αὐτῷ γαυριᾷ τόξον καὶ μάχαιρα
gr_gr_cog~JOB~C039~024~καὶ ὀργῇ ἀφανιεῖ τὴν γῆν καὶ οὐ μὴ πιστεύσῃ ἕως ἂν σημάνῃ σάλπιγξ
gr_gr_cog~JOB~C039~025~σάλπιγγος δὲ σημαινούσης λέγει εὖγε πόρρωθεν δὲ ὀσφραίνεται πολέμου σὺν ἅλματι καὶ κραυγῇ
gr_gr_cog~JOB~C039~026~ἐκ δὲ τῆς σῆς ἐπιστήμης ἕστηκεν ἱέραξ ἀναπετάσας τὰς πτέρυγας ἀκίνητος καθορῶν τὰ πρὸς νότον
gr_gr_cog~JOB~C039~027~ἐπὶ δὲ σῷ προστάγματι ὑψοῦται ἀετός γὺψ δὲ ἐπὶ νοσσιᾶς αὐτοῦ καθεσθεὶς αὐλίζεται
gr_gr_cog~JOB~C039~028~ἐπ’ ἐξοχῇ πέτρας καὶ ἀποκρύφῳ
gr_gr_cog~JOB~C039~029~ἐκεῖσε ὢν ζητεῖ τὰ σῖτα πόρρωθεν οἱ ὀφθαλμοὶ αὐτοῦ σκοπεύουσιν
gr_gr_cog~JOB~C039~030~νεοσσοὶ δὲ αὐτοῦ φύρονται ἐν αἵματι οὗ δ’ ἂν ὦσι τεθνεῶτες παραχρῆμα εὑρίσκονται
