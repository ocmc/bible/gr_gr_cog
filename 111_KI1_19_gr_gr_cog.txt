gr_gr_cog~KI1~C019~001~καὶ ἀνήγγειλεν Αχααβ τῇ Ιεζαβελ γυναικὶ αὐτοῦ πάντα ἃ ἐποίησεν Ηλιου καὶ ὡς ἀπέκτεινεν τοὺς προφήτας ἐν ῥομφαίᾳ
gr_gr_cog~KI1~C019~002~καὶ ἀπέστειλεν Ιεζαβελ πρὸς Ηλιου καὶ εἶπεν εἰ σὺ εἶ Ηλιου καὶ ἐγὼ Ιεζαβελ τάδε ποιήσαι μοι ὁ θεὸς καὶ τάδε προσθείη ὅτι ταύτην τὴν ὥραν αὔριον θήσομαι τὴν ψυχήν σου καθὼς ψυχὴν ἑνὸς ἐξ αὐτῶν
gr_gr_cog~KI1~C019~003~καὶ ἐφοβήθη Ηλιου καὶ ἀνέστη καὶ ἀπῆλθεν κατὰ τὴν ψυχὴν ἑαυτοῦ καὶ ἔρχεται εἰς Βηρσαβεε τὴν Ιουδα καὶ ἀφῆκεν τὸ παιδάριον αὐτοῦ ἐκεῖ
gr_gr_cog~KI1~C019~004~καὶ αὐτὸς ἐπορεύθη ἐν τῇ ἐρήμῳ ὁδὸν ἡμέρας καὶ ἦλθεν καὶ ἐκάθισεν ὑπὸ ραθμ ἓν καὶ ᾐτήσατο τὴν ψυχὴν αὐτοῦ ἀποθανεῖν καὶ εἶπεν ἱκανούσθω νῦν λαβὲ δὴ τὴν ψυχήν μου ἀπ’ ἐμοῦ κύριε ὅτι οὐ κρείσσων ἐγώ εἰμι ὑπὲρ τοὺς πατέρας μου
gr_gr_cog~KI1~C019~005~καὶ ἐκοιμήθη καὶ ὕπνωσεν ἐκεῖ ὑπὸ φυτόν καὶ ἰδού τις ἥψατο αὐτοῦ καὶ εἶπεν αὐτῷ ἀνάστηθι καὶ φάγε
gr_gr_cog~KI1~C019~006~καὶ ἐπέβλεψεν Ηλιου καὶ ἰδοὺ πρὸς κεφαλῆς αὐτοῦ ἐγκρυφίας ὀλυρίτης καὶ καψάκης ὕδατος καὶ ἀνέστη καὶ ἔφαγεν καὶ ἔπιεν καὶ ἐπιστρέψας ἐκοιμήθη
gr_gr_cog~KI1~C019~007~καὶ ἐπέστρεψεν ὁ ἄγγελος κυρίου ἐκ δευτέρου καὶ ἥψατο αὐτοῦ καὶ εἶπεν αὐτῷ ἀνάστα φάγε ὅτι πολλὴ ἀπὸ σοῦ ἡ ὁδός
gr_gr_cog~KI1~C019~008~καὶ ἀνέστη καὶ ἔφαγεν καὶ ἔπιεν καὶ ἐπορεύθη ἐν τῇ ἰσχύι τῆς βρώσεως ἐκείνης τεσσαράκοντα ἡμέρας καὶ τεσσαράκοντα νύκτας ἕως ὄρους Χωρηβ
gr_gr_cog~KI1~C019~009~καὶ εἰσῆλθεν ἐκεῖ εἰς τὸ σπήλαιον καὶ κατέλυσεν ἐκεῖ καὶ ἰδοὺ ῥῆμα κυρίου πρὸς αὐτὸν καὶ εἶπεν τί σὺ ἐνταῦθα Ηλιου
gr_gr_cog~KI1~C019~010~καὶ εἶπεν Ηλιου ζηλῶν ἐζήλωκα τῷ κυρίῳ παντοκράτορι ὅτι ἐγκατέλιπόν σε οἱ υἱοὶ Ισραηλ τὰ θυσιαστήριά σου κατέσκαψαν καὶ τοὺς προφήτας σου ἀπέκτειναν ἐν ῥομφαίᾳ καὶ ὑπολέλειμμαι ἐγὼ μονώτατος καὶ ζητοῦσι τὴν ψυχήν μου λαβεῖν αὐτήν
gr_gr_cog~KI1~C019~011~καὶ εἶπεν ἐξελεύσῃ αὔριον καὶ στήσῃ ἐνώπιον κυρίου ἐν τῷ ὄρει ἰδοὺ παρελεύσεται κύριος καὶ πνεῦμα μέγα κραταιὸν διαλῦον ὄρη καὶ συντρῖβον πέτρας ἐνώπιον κυρίου οὐκ ἐν τῷ πνεύματι κύριος καὶ μετὰ τὸ πνεῦμα συσσεισμός οὐκ ἐν τῷ συσσεισμῷ κύριος
gr_gr_cog~KI1~C019~012~καὶ μετὰ τὸν συσσεισμὸν πῦρ οὐκ ἐν τῷ πυρὶ κύριος καὶ μετὰ τὸ πῦρ φωνὴ αὔρας λεπτῆς κἀκεῖ κύριος
gr_gr_cog~KI1~C019~013~καὶ ἐγένετο ὡς ἤκουσεν Ηλιου καὶ ἐπεκάλυψεν τὸ πρόσωπον αὐτοῦ ἐν τῇ μηλωτῇ ἑαυτοῦ καὶ ἐξῆλθεν καὶ ἔστη ὑπὸ τὸ σπήλαιον καὶ ἰδοὺ πρὸς αὐτὸν φωνὴ καὶ εἶπεν τί σὺ ἐνταῦθα Ηλιου
gr_gr_cog~KI1~C019~014~καὶ εἶπεν Ηλιου ζηλῶν ἐζήλωκα τῷ κυρίῳ παντοκράτορι ὅτι ἐγκατέλιπον τὴν διαθήκην σου οἱ υἱοὶ Ισραηλ τὰ θυσιαστήριά σου καθεῖλαν καὶ τοὺς προφήτας σου ἀπέκτειναν ἐν ῥομφαίᾳ καὶ ὑπολέλειμμαι ἐγὼ μονώτατος καὶ ζητοῦσι τὴν ψυχήν μου λαβεῖν αὐτήν
gr_gr_cog~KI1~C019~015~καὶ εἶπεν κύριος πρὸς αὐτόν πορεύου ἀνάστρεφε εἰς τὴν ὁδόν σου καὶ ἥξεις εἰς τὴν ὁδὸν ἐρήμου Δαμασκοῦ καὶ χρίσεις τὸν Αζαηλ εἰς βασιλέα τῆς Συρίας
gr_gr_cog~KI1~C019~016~καὶ τὸν Ιου υἱὸν Ναμεσσι χρίσεις εἰς βασιλέα ἐπὶ Ισραηλ καὶ τὸν Ελισαιε υἱὸν Σαφατ ἀπὸ Αβελμαουλα χρίσεις εἰς προφήτην ἀντὶ σοῦ
gr_gr_cog~KI1~C019~017~καὶ ἔσται τὸν σῳζόμενον ἐκ ῥομφαίας Αζαηλ θανατώσει Ιου καὶ τὸν σῳζόμενον ἐκ ῥομφαίας Ιου θανατώσει Ελισαιε
gr_gr_cog~KI1~C019~018~καὶ καταλείψεις ἐν Ισραηλ ἑπτὰ χιλιάδας ἀνδρῶν πάντα γόνατα ἃ οὐκ ὤκλασαν γόνυ τῷ Βααλ καὶ πᾶν στόμα ὃ οὐ προσεκύνησεν αὐτῷ
gr_gr_cog~KI1~C019~019~καὶ ἀπῆλθεν ἐκεῖθεν καὶ εὑρίσκει τὸν Ελισαιε υἱὸν Σαφατ καὶ αὐτὸς ἠροτρία ἐν βουσίν δώδεκα ζεύγη βοῶν ἐνώπιον αὐτοῦ καὶ αὐτὸς ἐν τοῖς δώδεκα καὶ ἐπῆλθεν ἐπ’ αὐτὸν καὶ ἐπέρριψε τὴν μηλωτὴν αὐτοῦ ἐπ’ αὐτόν
gr_gr_cog~KI1~C019~020~καὶ κατέλιπεν Ελισαιε τὰς βόας καὶ κατέδραμεν ὀπίσω Ηλιου καὶ εἶπεν καταφιλήσω τὸν πατέρα μου καὶ ἀκολουθήσω ὀπίσω σου καὶ εἶπεν Ηλιου ἀνάστρεφε ὅτι πεποίηκά σοι
gr_gr_cog~KI1~C019~021~καὶ ἀνέστρεψεν ἐξόπισθεν αὐτοῦ καὶ ἔλαβεν τὰ ζεύγη τῶν βοῶν καὶ ἔθυσεν καὶ ἥψησεν αὐτὰ ἐν τοῖς σκεύεσι τῶν βοῶν καὶ ἔδωκεν τῷ λαῷ καὶ ἔφαγον καὶ ἀνέστη καὶ ἐπορεύθη ὀπίσω Ηλιου καὶ ἐλειτούργει αὐτῷ
