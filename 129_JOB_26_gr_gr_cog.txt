gr_gr_cog~JOB~C026~001~ὑπολαβὼν δὲ Ιωβ λέγει
gr_gr_cog~JOB~C026~002~τίνι πρόσκεισαι ἢ τίνι μέλλεις βοηθεῖν πότερον οὐχ ᾧ πολλὴ ἰσχὺς καὶ ᾧ βραχίων κραταιός ἐστιν
gr_gr_cog~JOB~C026~003~τίνι συμβεβούλευσαι οὐχ ᾧ πᾶσα σοφία ἢ τίνι ἐπακολουθήσεις οὐχ ᾧ μεγίστη δύναμις
gr_gr_cog~JOB~C026~004~τίνι ἀνήγγειλας ῥήματα πνοὴ δὲ τίνος ἐστὶν ἡ ἐξελθοῦσα ἐκ σοῦ
gr_gr_cog~JOB~C026~005~μὴ γίγαντες μαιωθήσονται ὑποκάτωθεν ὕδατος καὶ τῶν γειτόνων αὐτοῦ
gr_gr_cog~JOB~C026~006~γυμνὸς ὁ ᾅδης ἐπώπιον αὐτοῦ καὶ οὐκ ἔστιν περιβόλαιον τῇ ἀπωλείᾳ
gr_gr_cog~JOB~C026~007~ἐκτείνων βορέαν ἐπ’ οὐδέν κρεμάζων γῆν ἐπὶ οὐδενός
gr_gr_cog~JOB~C026~008~δεσμεύων ὕδωρ ἐν νεφέλαις αὐτοῦ καὶ οὐκ ἐρράγη νέφος ὑποκάτω αὐτοῦ
gr_gr_cog~JOB~C026~009~ὁ κρατῶν πρόσωπον θρόνου ἐκπετάζων ἐπ’ αὐτὸν νέφος αὐτοῦ
gr_gr_cog~JOB~C026~010~πρόσταγμα ἐγύρωσεν ἐπὶ πρόσωπον ὕδατος μέχρι συντελείας φωτὸς μετὰ σκότους
gr_gr_cog~JOB~C026~011~στῦλοι οὐρανοῦ ἐπετάσθησαν καὶ ἐξέστησαν ἀπὸ τῆς ἐπιτιμήσεως αὐτοῦ
gr_gr_cog~JOB~C026~012~ἰσχύι κατέπαυσεν τὴν θάλασσαν ἐπιστήμῃ δὲ ἔτρωσε τὸ κῆτος
gr_gr_cog~JOB~C026~013~κλεῖθρα δὲ οὐρανοῦ δεδοίκασιν αὐτόν προστάγματι δὲ ἐθανάτωσεν δράκοντα ἀποστάτην
gr_gr_cog~JOB~C026~014~ἰδοὺ ταῦτα μέρη ὁδοῦ αὐτοῦ καὶ ἐπὶ ἰκμάδα λόγου ἀκουσόμεθα ἐν αὐτῷ σθένος δὲ βροντῆς αὐτοῦ τίς οἶδεν ὁπότε ποιήσει
