gr_gr_cog~JOE~C003~001~καὶ ἔσται μετὰ ταῦτα καὶ ἐκχεῶ ἀπὸ τοῦ πνεύματός μου ἐπὶ πᾶσαν σάρκα καὶ προφητεύσουσιν οἱ υἱοὶ ὑμῶν καὶ αἱ θυγατέρες ὑμῶν καὶ οἱ πρεσβύτεροι ὑμῶν ἐνύπνια ἐνυπνιασθήσονται καὶ οἱ νεανίσκοι ὑμῶν ὁράσεις ὄψονται
gr_gr_cog~JOE~C003~002~καὶ ἐπὶ τοὺς δούλους καὶ ἐπὶ τὰς δούλας ἐν ταῖς ἡμέραις ἐκείναις ἐκχεῶ ἀπὸ τοῦ πνεύματός μου
gr_gr_cog~JOE~C003~003~καὶ δώσω τέρατα ἐν τῷ οὐρανῷ καὶ ἐπὶ τῆς γῆς αἷμα καὶ πῦρ καὶ ἀτμίδα καπνοῦ
gr_gr_cog~JOE~C003~004~ὁ ἥλιος μεταστραφήσεται εἰς σκότος καὶ ἡ σελήνη εἰς αἷμα πρὶν ἐλθεῖν ἡμέραν κυρίου τὴν μεγάλην καὶ ἐπιφανῆ
gr_gr_cog~JOE~C003~005~καὶ ἔσται πᾶς ὃς ἂν ἐπικαλέσηται τὸ ὄνομα κυρίου σωθήσεται ὅτι ἐν τῷ ὄρει Σιων καὶ ἐν Ιερουσαλημ ἔσται ἀνασῳζόμενος καθότι εἶπεν κύριος καὶ εὐαγγελιζόμενοι οὓς κύριος προσκέκληται
