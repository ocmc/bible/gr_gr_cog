gr_gr_cog~EXO~C034~001~καὶ εἶπεν κύριος πρὸς Μωυσῆν λάξευσον σεαυτῷ δύο πλάκας λιθίνας καθὼς καὶ αἱ πρῶται καὶ ἀνάβηθι πρός με εἰς τὸ ὄρος καὶ γράψω ἐπὶ τῶν πλακῶν τὰ ῥήματα ἃ ἦν ἐν ταῖς πλαξὶν ταῖς πρώταις αἷς συνέτριψας
gr_gr_cog~EXO~C034~002~καὶ γίνου ἕτοιμος εἰς τὸ πρωὶ καὶ ἀναβήσῃ ἐπὶ τὸ ὄρος τὸ Σινα καὶ στήσῃ μοι ἐκεῖ ἐπ’ ἄκρου τοῦ ὄρους
gr_gr_cog~EXO~C034~003~καὶ μηδεὶς ἀναβήτω μετὰ σοῦ μηδὲ ὀφθήτω ἐν παντὶ τῷ ὄρει καὶ τὰ πρόβατα καὶ αἱ βόες μὴ νεμέσθωσαν πλησίον τοῦ ὄρους ἐκείνου
gr_gr_cog~EXO~C034~004~καὶ ἐλάξευσεν δύο πλάκας λιθίνας καθάπερ καὶ αἱ πρῶται καὶ ὀρθρίσας Μωυσῆς ἀνέβη εἰς τὸ ὄρος τὸ Σινα καθότι συνέταξεν αὐτῷ κύριος καὶ ἔλαβεν Μωυσῆς τὰς δύο πλάκας τὰς λιθίνας
gr_gr_cog~EXO~C034~005~καὶ κατέβη κύριος ἐν νεφέλῃ καὶ παρέστη αὐτῷ ἐκεῖ καὶ ἐκάλεσεν τῷ ὀνόματι κυρίου
gr_gr_cog~EXO~C034~006~καὶ παρῆλθεν κύριος πρὸ προσώπου αὐτοῦ καὶ ἐκάλεσεν κύριος ὁ θεὸς οἰκτίρμων καὶ ἐλεήμων μακρόθυμος καὶ πολυέλεος καὶ ἀληθινὸς
gr_gr_cog~EXO~C034~007~καὶ δικαιοσύνην διατηρῶν καὶ ποιῶν ἔλεος εἰς χιλιάδας ἀφαιρῶν ἀνομίας καὶ ἀδικίας καὶ ἁμαρτίας καὶ οὐ καθαριεῖ τὸν ἔνοχον ἐπάγων ἀνομίας πατέρων ἐπὶ τέκνα καὶ ἐπὶ τέκνα τέκνων ἐπὶ τρίτην καὶ τετάρτην γενεάν
gr_gr_cog~EXO~C034~008~καὶ σπεύσας Μωυσῆς κύψας ἐπὶ τὴν γῆν προσεκύνησεν
gr_gr_cog~EXO~C034~009~καὶ εἶπεν εἰ εὕρηκα χάριν ἐνώπιόν σου συμπορευθήτω ὁ κύριός μου μεθ’ ἡμῶν ὁ λαὸς γὰρ σκληροτράχηλός ἐστιν καὶ ἀφελεῖς σὺ τὰς ἁμαρτίας ἡμῶν καὶ τὰς ἀνομίας ἡμῶν καὶ ἐσόμεθα σοί
gr_gr_cog~EXO~C034~010~καὶ εἶπεν κύριος πρὸς Μωυσῆν ἰδοὺ ἐγὼ τίθημί σοι διαθήκην ἐνώπιον παντὸς τοῦ λαοῦ σου ποιήσω ἔνδοξα ἃ οὐ γέγονεν ἐν πάσῃ τῇ γῇ καὶ ἐν παντὶ ἔθνει καὶ ὄψεται πᾶς ὁ λαός ἐν οἷς εἶ σύ τὰ ἔργα κυρίου ὅτι θαυμαστά ἐστιν ἃ ἐγὼ ποιήσω σοι
gr_gr_cog~EXO~C034~011~πρόσεχε σὺ πάντα ὅσα ἐγὼ ἐντέλλομαί σοι ἰδοὺ ἐγὼ ἐκβάλλω πρὸ προσώπου ὑμῶν τὸν Αμορραῖον καὶ Χαναναῖον καὶ Χετταῖον καὶ Φερεζαῖον καὶ Ευαῖον καὶ Γεργεσαῖον καὶ Ιεβουσαῖον
gr_gr_cog~EXO~C034~012~πρόσεχε σεαυτῷ μήποτε θῇς διαθήκην τοῖς ἐγκαθημένοις ἐπὶ τῆς γῆς εἰς ἣν εἰσπορεύῃ εἰς αὐτήν μή σοι γένηται πρόσκομμα ἐν ὑμῖν
gr_gr_cog~EXO~C034~013~τοὺς βωμοὺς αὐτῶν καθελεῖτε καὶ τὰς στήλας αὐτῶν συντρίψετε καὶ τὰ ἄλση αὐτῶν ἐκκόψετε καὶ τὰ γλυπτὰ τῶν θεῶν αὐτῶν κατακαύσετε ἐν πυρί
gr_gr_cog~EXO~C034~014~οὐ γὰρ μὴ προσκυνήσητε θεῷ ἑτέρῳ ὁ γὰρ κύριος ὁ θεὸς ζηλωτὸν ὄνομα θεὸς ζηλωτής ἐστιν
gr_gr_cog~EXO~C034~015~μήποτε θῇς διαθήκην τοῖς ἐγκαθημένοις πρὸς ἀλλοφύλους ἐπὶ τῆς γῆς καὶ ἐκπορνεύσωσιν ὀπίσω τῶν θεῶν αὐτῶν καὶ θύσωσι τοῖς θεοῖς αὐτῶν καὶ καλέσωσίν σε καὶ φάγῃς τῶν θυμάτων αὐτῶν
gr_gr_cog~EXO~C034~016~καὶ λάβῃς τῶν θυγατέρων αὐτῶν τοῖς υἱοῖς σου καὶ τῶν θυγατέρων σου δῷς τοῖς υἱοῖς αὐτῶν καὶ ἐκπορνεύσωσιν αἱ θυγατέρες σου ὀπίσω τῶν θεῶν αὐτῶν καὶ ἐκπορνεύσωσιν τοὺς υἱούς σου ὀπίσω τῶν θεῶν αὐτῶν
gr_gr_cog~EXO~C034~017~καὶ θεοὺς χωνευτοὺς οὐ ποιήσεις σεαυτῷ
gr_gr_cog~EXO~C034~018~καὶ τὴν ἑορτὴν τῶν ἀζύμων φυλάξῃ ἑπτὰ ἡμέρας φάγῃ ἄζυμα καθάπερ ἐντέταλμαί σοι εἰς τὸν καιρὸν ἐν μηνὶ τῶν νέων ἐν γὰρ μηνὶ τῶν νέων ἐξῆλθες ἐξ Αἰγύπτου
gr_gr_cog~EXO~C034~019~πᾶν διανοῖγον μήτραν ἐμοί τὰ ἀρσενικά πρωτότοκον μόσχου καὶ πρωτότοκον προβάτου
gr_gr_cog~EXO~C034~020~καὶ πρωτότοκον ὑποζυγίου λυτρώσῃ προβάτῳ ἐὰν δὲ μὴ λυτρώσῃ αὐτό τιμὴν δώσεις πᾶν πρωτότοκον τῶν υἱῶν σου λυτρώσῃ οὐκ ὀφθήσῃ ἐνώπιόν μου κενός
gr_gr_cog~EXO~C034~021~ἓξ ἡμέρας ἐργᾷ τῇ δὲ ἑβδόμῃ καταπαύσεις τῷ σπόρῳ καὶ τῷ ἀμήτῳ καταπαύσεις
gr_gr_cog~EXO~C034~022~καὶ ἑορτὴν ἑβδομάδων ποιήσεις μοι ἀρχὴν θερισμοῦ πυρῶν καὶ ἑορτὴν συναγωγῆς μεσοῦντος τοῦ ἐνιαυτοῦ
gr_gr_cog~EXO~C034~023~τρεῖς καιροὺς τοῦ ἐνιαυτοῦ ὀφθήσεται πᾶν ἀρσενικόν σου ἐνώπιον κυρίου τοῦ θεοῦ Ισραηλ
gr_gr_cog~EXO~C034~024~ὅταν γὰρ ἐκβάλω τὰ ἔθνη πρὸ προσώπου σου καὶ πλατύνω τὰ ὅριά σου οὐκ ἐπιθυμήσει οὐδεὶς τῆς γῆς σου ἡνίκα ἂν ἀναβαίνῃς ὀφθῆναι ἐναντίον κυρίου τοῦ θεοῦ σου τρεῖς καιροὺς τοῦ ἐνιαυτοῦ
gr_gr_cog~EXO~C034~025~οὐ σφάξεις ἐπὶ ζύμῃ αἷμα θυμιαμάτων μου καὶ οὐ κοιμηθήσεται εἰς τὸ πρωὶ θύματα τῆς ἑορτῆς τοῦ πασχα
gr_gr_cog~EXO~C034~026~τὰ πρωτογενήματα τῆς γῆς σου θήσεις εἰς τὸν οἶκον κυρίου τοῦ θεοῦ σου οὐ προσοίσεις ἄρνα ἐν γάλακτι μητρὸς αὐτοῦ
gr_gr_cog~EXO~C034~027~καὶ εἶπεν κύριος πρὸς Μωυσῆν γράψον σεαυτῷ τὰ ῥήματα ταῦτα ἐπὶ γὰρ τῶν λόγων τούτων τέθειμαί σοι διαθήκην καὶ τῷ Ισραηλ
gr_gr_cog~EXO~C034~028~καὶ ἦν ἐκεῖ Μωυσῆς ἐναντίον κυρίου τεσσαράκοντα ἡμέρας καὶ τεσσαράκοντα νύκτας ἄρτον οὖκ ἔφαγεν καὶ ὕδωρ οὐκ ἔπιεν καὶ ἔγραψεν τὰ ῥήματα ταῦτα ἐπὶ τῶν πλακῶν τῆς διαθήκης τοὺς δέκα λόγους
gr_gr_cog~EXO~C034~029~ὡς δὲ κατέβαινεν Μωυσῆς ἐκ τοῦ ὄρους καὶ αἱ δύο πλάκες ἐπὶ τῶν χειρῶν Μωυσῆ καταβαίνοντος δὲ αὐτοῦ ἐκ τοῦ ὄρους Μωυσῆς οὐκ ᾔδει ὅτι δεδόξασται ἡ ὄψις τοῦ χρώματος τοῦ προσώπου αὐτοῦ ἐν τῷ λαλεῖν αὐτὸν αὐτῷ
gr_gr_cog~EXO~C034~030~καὶ εἶδεν Ααρων καὶ πάντες οἱ πρεσβύτεροι Ισραηλ τὸν Μωυσῆν καὶ ἦν δεδοξασμένη ἡ ὄψις τοῦ χρώματος τοῦ προσώπου αὐτοῦ καὶ ἐφοβήθησαν ἐγγίσαι αὐτοῦ
gr_gr_cog~EXO~C034~031~καὶ ἐκάλεσεν αὐτοὺς Μωυσῆς καὶ ἐπεστράφησαν πρὸς αὐτὸν Ααρων καὶ πάντες οἱ ἄρχοντες τῆς συναγωγῆς καὶ ἐλάλησεν αὐτοῖς Μωυσῆς
gr_gr_cog~EXO~C034~032~καὶ μετὰ ταῦτα προσῆλθον πρὸς αὐτὸν πάντες οἱ υἱοὶ Ισραηλ καὶ ἐνετείλατο αὐτοῖς πάντα ὅσα ἐλάλησεν κύριος πρὸς αὐτὸν ἐν τῷ ὄρει Σινα
gr_gr_cog~EXO~C034~033~καὶ ἐπειδὴ κατέπαυσεν λαλῶν πρὸς αὐτούς ἐπέθηκεν ἐπὶ τὸ πρόσωπον αὐτοῦ κάλυμμα
gr_gr_cog~EXO~C034~034~ἡνίκα δ’ ἂν εἰσεπορεύετο Μωυσῆς ἔναντι κυρίου λαλεῖν αὐτῷ περιῃρεῖτο τὸ κάλυμμα ἕως τοῦ ἐκπορεύεσθαι καὶ ἐξελθὼν ἐλάλει πᾶσιν τοῖς υἱοῖς Ισραηλ ὅσα ἐνετείλατο αὐτῷ κύριος
gr_gr_cog~EXO~C034~035~καὶ εἶδον οἱ υἱοὶ Ισραηλ τὸ πρόσωπον Μωυσῆ ὅτι δεδόξασται καὶ περιέθηκεν Μωυσῆς κάλυμμα ἐπὶ τὸ πρόσωπον ἑαυτοῦ ἕως ἂν εἰσέλθῃ συλλαλεῖν αὐτῷ
