gr_gr_cog~JER~C034~001~
gr_gr_cog~JER~C034~002~οὕτως εἶπεν κύριος ποίησον δεσμοὺς καὶ κλοιοὺς καὶ περίθου περὶ τὸν τράχηλόν σου
gr_gr_cog~JER~C034~003~καὶ ἀποστελεῖς αὐτοὺς πρὸς βασιλέα Ιδουμαίας καὶ πρὸς βασιλέα Μωαβ καὶ πρὸς βασιλέα υἱῶν Αμμων καὶ πρὸς βασιλέα Τύρου καὶ πρὸς βασιλέα Σιδῶνος ἐν χερσὶν ἀγγέλων αὐτῶν τῶν ἐρχομένων εἰς ἀπάντησιν αὐτῶν εἰς Ιερουσαλημ πρὸς Σεδεκιαν βασιλέα Ιουδα
gr_gr_cog~JER~C034~004~καὶ συντάξεις αὐτοῖς πρὸς τοὺς κυρίους αὐτῶν εἰπεῖν οὕτως εἶπεν κύριος ὁ θεὸς Ισραηλ οὕτως ἐρεῖτε πρὸς τοὺς κυρίους ὑμῶν
gr_gr_cog~JER~C034~005~ὅτι ἐγὼ ἐποίησα τὴν γῆν ἐν τῇ ἰσχύι μου τῇ μεγάλῃ καὶ ἐν τῷ ἐπιχείρῳ μου τῷ ὑψηλῷ καὶ δώσω αὐτὴν ᾧ ἐὰν δόξῃ ἐν ὀφθαλμοῖς μου
gr_gr_cog~JER~C034~006~ἔδωκα τὴν γῆν τῷ Ναβουχοδονοσορ βασιλεῖ Βαβυλῶνος δουλεύειν αὐτῷ καὶ τὰ θηρία τοῦ ἀγροῦ ἐργάζεσθαι αὐτῷ
gr_gr_cog~JER~C034~007~
gr_gr_cog~JER~C034~008~καὶ τὸ ἔθνος καὶ ἡ βασιλεία ὅσοι ἐὰν μὴ ἐμβάλωσιν τὸν τράχηλον αὐτῶν ὑπὸ τὸν ζυγὸν βασιλέως Βαβυλῶνος ἐν μαχαίρᾳ καὶ ἐν λιμῷ ἐπισκέψομαι αὐτούς εἶπεν κύριος ἕως ἐκλίπωσιν ἐν χειρὶ αὐτοῦ
gr_gr_cog~JER~C034~009~καὶ ὑμεῖς μὴ ἀκούετε τῶν ψευδοπροφητῶν ὑμῶν καὶ τῶν μαντευομένων ὑμῖν καὶ τῶν ἐνυπνιαζομένων ὑμῖν καὶ τῶν οἰωνισμάτων ὑμῶν καὶ τῶν φαρμακῶν ὑμῶν τῶν λεγόντων οὐ μὴ ἐργάσησθε τῷ βασιλεῖ Βαβυλῶνος
gr_gr_cog~JER~C034~010~ὅτι ψευδῆ αὐτοὶ προφητεύουσιν ὑμῖν πρὸς τὸ μακρῦναι ὑμᾶς ἀπὸ τῆς γῆς ὑμῶν
gr_gr_cog~JER~C034~011~καὶ τὸ ἔθνος ὃ ἐὰν εἰσαγάγῃ τὸν τράχηλον αὐτοῦ ὑπὸ τὸν ζυγὸν βασιλέως Βαβυλῶνος καὶ ἐργάσηται αὐτῷ καὶ καταλείψω αὐτὸν ἐπὶ τῆς γῆς αὐτοῦ καὶ ἐργᾶται αὐτῷ καὶ ἐνοικήσει ἐν αὐτῇ
gr_gr_cog~JER~C034~012~καὶ πρὸς Σεδεκιαν βασιλέα Ιουδα ἐλάλησα κατὰ πάντας τοὺς λόγους τούτους λέγων εἰσαγάγετε τὸν τράχηλον ὑμῶν
gr_gr_cog~JER~C034~013~
gr_gr_cog~JER~C034~014~καὶ ἐργάσασθε τῷ βασιλεῖ Βαβυλῶνος ὅτι ἄδικα αὐτοὶ προφητεύουσιν ὑμῖν
gr_gr_cog~JER~C034~015~ὅτι οὐκ ἀπέστειλα αὐτούς φησὶν κύριος καὶ προφητεύουσιν τῷ ὀνόματί μου ἐπ’ ἀδίκῳ πρὸς τὸ ἀπολέσαι ὑμᾶς καὶ ἀπολεῖσθε ὑμεῖς καὶ οἱ προφῆται ὑμῶν οἱ προφητεύοντες ὑμῖν ἐπ’ ἀδίκῳ ψευδῆ
gr_gr_cog~JER~C034~016~ὑμῖν καὶ παντὶ τῷ λαῷ τούτῳ καὶ τοῖς ἱερεῦσιν ἐλάλησα λέγων οὕτως εἶπεν κύριος μὴ ἀκούετε τῶν λόγων τῶν προφητῶν τῶν προφητευόντων ὑμῖν λεγόντων ἰδοὺ σκεύη οἴκου κυρίου ἐπιστρέψει ἐκ Βαβυλῶνος ὅτι ἄδικα αὐτοὶ προφητεύουσιν ὑμῖν οὐκ ἀπέστειλα αὐτούς
gr_gr_cog~JER~C034~017~
gr_gr_cog~JER~C034~018~εἰ προφῆταί εἰσιν καὶ εἰ ἔστιν λόγος κυρίου ἐν αὐτοῖς ἀπαντησάτωσάν μοι
gr_gr_cog~JER~C034~019~ὅτι οὕτως εἶπεν κύριος καὶ τῶν ἐπιλοίπων σκευῶν
gr_gr_cog~JER~C034~020~ὧν οὐκ ἔλαβεν βασιλεὺς Βαβυλῶνος ὅτε ἀπῴκισεν τὸν Ιεχονιαν ἐξ Ιερουσαλημ
gr_gr_cog~JER~C034~021~
gr_gr_cog~JER~C034~022~εἰς Βαβυλῶνα εἰσελεύσεται λέγει κύριος
