gr_gr_cog~EXO~C023~001~οὐ παραδέξῃ ἀκοὴν ματαίαν οὐ συγκαταθήσῃ μετὰ τοῦ ἀδίκου γενέσθαι μάρτυς ἄδικος
gr_gr_cog~EXO~C023~002~οὐκ ἔσῃ μετὰ πλειόνων ἐπὶ κακίᾳ οὐ προστεθήσῃ μετὰ πλήθους ἐκκλῖναι μετὰ πλειόνων ὥστε ἐκκλῖναι κρίσιν
gr_gr_cog~EXO~C023~003~καὶ πένητα οὐκ ἐλεήσεις ἐν κρίσει
gr_gr_cog~EXO~C023~004~ἐὰν δὲ συναντήσῃς τῷ βοὶ τοῦ ἐχθροῦ σου ἢ τῷ ὑποζυγίῳ αὐτοῦ πλανωμένοις ἀποστρέψας ἀποδώσεις αὐτῷ
gr_gr_cog~EXO~C023~005~ἐὰν δὲ ἴδῃς τὸ ὑποζύγιον τοῦ ἐχθροῦ σου πεπτωκὸς ὑπὸ τὸν γόμον αὐτοῦ οὐ παρελεύσῃ αὐτό ἀλλὰ συνεγερεῖς αὐτὸ μετ’ αὐτοῦ
gr_gr_cog~EXO~C023~006~οὐ διαστρέψεις κρίμα πένητος ἐν κρίσει αὐτοῦ
gr_gr_cog~EXO~C023~007~ἀπὸ παντὸς ῥήματος ἀδίκου ἀποστήσῃ ἀθῷον καὶ δίκαιον οὐκ ἀποκτενεῖς καὶ οὐ δικαιώσεις τὸν ἀσεβῆ ἕνεκεν δώρων
gr_gr_cog~EXO~C023~008~καὶ δῶρα οὐ λήμψῃ τὰ γὰρ δῶρα ἐκτυφλοῖ ὀφθαλμοὺς βλεπόντων καὶ λυμαίνεται ῥήματα δίκαια
gr_gr_cog~EXO~C023~009~καὶ προσήλυτον οὐ θλίψετε ὑμεῖς γὰρ οἴδατε τὴν ψυχὴν τοῦ προσηλύτου αὐτοὶ γὰρ προσήλυτοι ἦτε ἐν γῇ Αἰγύπτῳ
gr_gr_cog~EXO~C023~010~ἓξ ἔτη σπερεῖς τὴν γῆν σου καὶ συνάξεις τὰ γενήματα αὐτῆς
gr_gr_cog~EXO~C023~011~τῷ δὲ ἑβδόμῳ ἄφεσιν ποιήσεις καὶ ἀνήσεις αὐτήν καὶ ἔδονται οἱ πτωχοὶ τοῦ ἔθνους σου τὰ δὲ ὑπολειπόμενα ἔδεται τὰ ἄγρια θηρία οὕτως ποιήσεις τὸν ἀμπελῶνά σου καὶ τὸν ἐλαιῶνά σου
gr_gr_cog~EXO~C023~012~ἓξ ἡμέρας ποιήσεις τὰ ἔργα σου τῇ δὲ ἡμέρᾳ τῇ ἑβδόμῃ ἀνάπαυσις ἵνα ἀναπαύσηται ὁ βοῦς σου καὶ τὸ ὑποζύγιόν σου καὶ ἵνα ἀναψύξῃ ὁ υἱὸς τῆς παιδίσκης σου καὶ ὁ προσήλυτος
gr_gr_cog~EXO~C023~013~πάντα ὅσα εἴρηκα πρὸς ὑμᾶς φυλάξασθε καὶ ὄνομα θεῶν ἑτέρων οὐκ ἀναμνησθήσεσθε οὐδὲ μὴ ἀκουσθῇ ἐκ τοῦ στόματος ὑμῶν
gr_gr_cog~EXO~C023~014~τρεῖς καιροὺς τοῦ ἐνιαυτοῦ ἑορτάσατέ μοι
gr_gr_cog~EXO~C023~015~τὴν ἑορτὴν τῶν ἀζύμων φυλάξασθε ποιεῖν ἑπτὰ ἡμέρας ἔδεσθε ἄζυμα καθάπερ ἐνετειλάμην σοι κατὰ τὸν καιρὸν τοῦ μηνὸς τῶν νέων ἐν γὰρ αὐτῷ ἐξῆλθες ἐξ Αἰγύπτου οὐκ ὀφθήσῃ ἐνώπιόν μου κενός
gr_gr_cog~EXO~C023~016~καὶ ἑορτὴν θερισμοῦ πρωτογενημάτων ποιήσεις τῶν ἔργων σου ὧν ἐὰν σπείρῃς ἐν τῷ ἀγρῷ σου καὶ ἑορτὴν συντελείας ἐπ’ ἐξόδου τοῦ ἐνιαυτοῦ ἐν τῇ συναγωγῇ τῶν ἔργων σου τῶν ἐκ τοῦ ἀγροῦ σου
gr_gr_cog~EXO~C023~017~τρεῖς καιροὺς τοῦ ἐνιαυτοῦ ὀφθήσεται πᾶν ἀρσενικόν σου ἐνώπιον κυρίου τοῦ θεοῦ σου
gr_gr_cog~EXO~C023~018~ὅταν γὰρ ἐκβάλω ἔθνη ἀπὸ προσώπου σου καὶ ἐμπλατύνω τὰ ὅριά σου οὐ θύσεις ἐπὶ ζύμῃ αἷμα θυσιάσματός μου οὐδὲ μὴ κοιμηθῇ στέαρ τῆς ἑορτῆς μου ἕως πρωί
gr_gr_cog~EXO~C023~019~τὰς ἀπαρχὰς τῶν πρωτογενημάτων τῆς γῆς σου εἰσοίσεις εἰς τὸν οἶκον κυρίου τοῦ θεοῦ σου οὐχ ἑψήσεις ἄρνα ἐν γάλακτι μητρὸς αὐτοῦ
gr_gr_cog~EXO~C023~020~καὶ ἰδοὺ ἐγὼ ἀποστέλλω τὸν ἄγγελόν μου πρὸ προσώπου σου ἵνα φυλάξῃ σε ἐν τῇ ὁδῷ ὅπως εἰσαγάγῃ σε εἰς τὴν γῆν ἣν ἡτοίμασά σοι
gr_gr_cog~EXO~C023~021~πρόσεχε σεαυτῷ καὶ εἰσάκουε αὐτοῦ καὶ μὴ ἀπείθει αὐτῷ οὐ γὰρ μὴ ὑποστείληταί σε τὸ γὰρ ὄνομά μού ἐστιν ἐπ’ αὐτῷ
gr_gr_cog~EXO~C023~022~ἐὰν ἀκοῇ ἀκούσητε τῆς ἐμῆς φωνῆς καὶ ποιήσῃς πάντα ὅσα ἂν ἐντείλωμαί σοι καὶ φυλάξητε τὴν διαθήκην μου ἔσεσθέ μοι λαὸς περιούσιος ἀπὸ πάντων τῶν ἐθνῶν ἐμὴ γάρ ἐστιν πᾶσα ἡ γῆ ὑμεῖς δὲ ἔσεσθέ μοι βασίλειον ἱεράτευμα καὶ ἔθνος ἅγιον ταῦτα τὰ ῥήματα ἐρεῖς τοῖς υἱοῖς Ισραηλ ἐὰν ἀκοῇ ἀκούσητε τῆς φωνῆς μου καὶ ποιήσῃς πάντα ὅσα ἂν εἴπω σοι ἐχθρεύσω τοῖς ἐχθροῖς σου καὶ ἀντικείσομαι τοῖς ἀντικειμένοις σοι
gr_gr_cog~EXO~C023~023~πορεύσεται γὰρ ὁ ἄγγελός μου ἡγούμενός σου καὶ εἰσάξει σε πρὸς τὸν Αμορραῖον καὶ Χετταῖον καὶ Φερεζαῖον καὶ Χαναναῖον καὶ Γεργεσαῖον καὶ Ευαῖον καὶ Ιεβουσαῖον καὶ ἐκτρίψω αὐτούς
gr_gr_cog~EXO~C023~024~οὐ προσκυνήσεις τοῖς θεοῖς αὐτῶν οὐδὲ μὴ λατρεύσῃς αὐτοῖς οὐ ποιήσεις κατὰ τὰ ἔργα αὐτῶν ἀλλὰ καθαιρέσει καθελεῖς καὶ συντρίβων συντρίψεις τὰς στήλας αὐτῶν
gr_gr_cog~EXO~C023~025~καὶ λατρεύσεις κυρίῳ τῷ θεῷ σου καὶ εὐλογήσω τὸν ἄρτον σου καὶ τὸν οἶνόν σου καὶ τὸ ὕδωρ σου καὶ ἀποστρέψω μαλακίαν ἀφ’ ὑμῶν
gr_gr_cog~EXO~C023~026~οὐκ ἔσται ἄγονος οὐδὲ στεῖρα ἐπὶ τῆς γῆς σου τὸν ἀριθμὸν τῶν ἡμερῶν σου ἀναπληρώσω
gr_gr_cog~EXO~C023~027~καὶ τὸν φόβον ἀποστελῶ ἡγούμενόν σου καὶ ἐκστήσω πάντα τὰ ἔθνη εἰς οὓς σὺ εἰσπορεύῃ εἰς αὐτούς καὶ δώσω πάντας τοὺς ὑπεναντίους σου φυγάδας
gr_gr_cog~EXO~C023~028~καὶ ἀποστελῶ τὰς σφηκίας προτέρας σου καὶ ἐκβαλεῖ τοὺς Αμορραίους καὶ τοὺς Ευαίους καὶ τοὺς Χαναναίους καὶ τοὺς Χετταίους ἀπὸ σοῦ
gr_gr_cog~EXO~C023~029~οὐκ ἐκβαλῶ αὐτοὺς ἐν ἐνιαυτῷ ἑνί ἵνα μὴ γένηται ἡ γῆ ἔρημος καὶ πολλὰ γένηται ἐπὶ σὲ τὰ θηρία τῆς γῆς
gr_gr_cog~EXO~C023~030~κατὰ μικρὸν μικρὸν ἐκβαλῶ αὐτοὺς ἀπὸ σοῦ ἕως ἂν αὐξηθῇς καὶ κληρονομήσῃς τὴν γῆν
gr_gr_cog~EXO~C023~031~καὶ θήσω τὰ ὅριά σου ἀπὸ τῆς ἐρυθρᾶς θαλάσσης ἕως τῆς θαλάσσης τῆς Φυλιστιιμ καὶ ἀπὸ τῆς ἐρήμου ἕως τοῦ μεγάλου ποταμοῦ Εὐφράτου καὶ παραδώσω εἰς τὰς χεῖρας ὑμῶν τοὺς ἐγκαθημένους ἐν τῇ γῇ καὶ ἐκβαλῶ αὐτοὺς ἀπὸ σοῦ
gr_gr_cog~EXO~C023~032~οὐ συγκαταθήσῃ αὐτοῖς καὶ τοῖς θεοῖς αὐτῶν διαθήκην
gr_gr_cog~EXO~C023~033~καὶ οὐκ ἐγκαθήσονται ἐν τῇ γῇ σου ἵνα μὴ ἁμαρτεῖν σε ποιήσωσιν πρός με ἐὰν γὰρ δουλεύσῃς τοῖς θεοῖς αὐτῶν οὗτοι ἔσονταί σοι πρόσκομμα
