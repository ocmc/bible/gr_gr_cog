gr_gr_cog~LEV~C026~001~οὐ ποιήσετε ὑμῖν αὐτοῖς χειροποίητα οὐδὲ γλυπτὰ οὐδὲ στήλην ἀναστήσετε ὑμῖν οὐδὲ λίθον σκοπὸν θήσετε ἐν τῇ γῇ ὑμῶν προσκυνῆσαι αὐτῷ ἐγώ εἰμι κύριος ὁ θεὸς ὑμῶν
gr_gr_cog~LEV~C026~002~τὰ σάββατά μου φυλάξεσθε καὶ ἀπὸ τῶν ἁγίων μου φοβηθήσεσθε ἐγώ εἰμι κύριος
gr_gr_cog~LEV~C026~003~ἐὰν τοῖς προστάγμασίν μου πορεύησθε καὶ τὰς ἐντολάς μου φυλάσσησθε καὶ ποιήσητε αὐτάς
gr_gr_cog~LEV~C026~004~καὶ δώσω τὸν ὑετὸν ὑμῖν ἐν καιρῷ αὐτοῦ καὶ ἡ γῆ δώσει τὰ γενήματα αὐτῆς καὶ τὰ ξύλα τῶν πεδίων ἀποδώσει τὸν καρπὸν αὐτῶν
gr_gr_cog~LEV~C026~005~καὶ καταλήμψεται ὑμῖν ὁ ἀλοητὸς τὸν τρύγητον καὶ ὁ τρύγητος καταλήμψεται τὸν σπόρον καὶ φάγεσθε τὸν ἄρτον ὑμῶν εἰς πλησμονὴν καὶ κατοικήσετε μετὰ ἀσφαλείας ἐπὶ τῆς γῆς ὑμῶν
gr_gr_cog~LEV~C026~006~καὶ πόλεμος οὐ διελεύσεται διὰ τῆς γῆς ὑμῶν καὶ δώσω εἰρήνην ἐν τῇ γῇ ὑμῶν καὶ κοιμηθήσεσθε καὶ οὐκ ἔσται ὑμᾶς ὁ ἐκφοβῶν καὶ ἀπολῶ θηρία πονηρὰ ἐκ τῆς γῆς ὑμῶν
gr_gr_cog~LEV~C026~007~καὶ διώξεσθε τοὺς ἐχθροὺς ὑμῶν καὶ πεσοῦνται ἐναντίον ὑμῶν φόνῳ
gr_gr_cog~LEV~C026~008~καὶ διώξονται ἐξ ὑμῶν πέντε ἑκατόν καὶ ἑκατὸν ὑμῶν διώξονται μυριάδας καὶ πεσοῦνται οἱ ἐχθροὶ ὑμῶν ἐναντίον ὑμῶν μαχαίρᾳ
gr_gr_cog~LEV~C026~009~καὶ ἐπιβλέψω ἐφ’ ὑμᾶς καὶ αὐξανῶ ὑμᾶς καὶ πληθυνῶ ὑμᾶς καὶ στήσω τὴν διαθήκην μου μεθ’ ὑμῶν
gr_gr_cog~LEV~C026~010~καὶ φάγεσθε παλαιὰ καὶ παλαιὰ παλαιῶν καὶ παλαιὰ ἐκ προσώπου νέων ἐξοίσετε
gr_gr_cog~LEV~C026~011~καὶ θήσω τὴν διαθήκην μου ἐν ὑμῖν καὶ οὐ βδελύξεται ἡ ψυχή μου ὑμᾶς
gr_gr_cog~LEV~C026~012~καὶ ἐμπεριπατήσω ἐν ὑμῖν καὶ ἔσομαι ὑμῶν θεός καὶ ὑμεῖς ἔσεσθέ μου λαός
gr_gr_cog~LEV~C026~013~ἐγώ εἰμι κύριος ὁ θεὸς ὑμῶν ὁ ἐξαγαγὼν ὑμᾶς ἐκ γῆς Αἰγύπτου ὄντων ὑμῶν δούλων καὶ συνέτριψα τὸν δεσμὸν τοῦ ζυγοῦ ὑμῶν καὶ ἤγαγον ὑμᾶς μετὰ παρρησίας
gr_gr_cog~LEV~C026~014~ἐὰν δὲ μὴ ὑπακούσητέ μου μηδὲ ποιήσητε τὰ προστάγματά μου ταῦτα
gr_gr_cog~LEV~C026~015~ἀλλὰ ἀπειθήσητε αὐτοῖς καὶ τοῖς κρίμασίν μου προσοχθίσῃ ἡ ψυχὴ ὑμῶν ὥστε ὑμᾶς μὴ ποιεῖν πάσας τὰς ἐντολάς μου ὥστε διασκεδάσαι τὴν διαθήκην μου
gr_gr_cog~LEV~C026~016~καὶ ἐγὼ ποιήσω οὕτως ὑμῖν καὶ ἐπισυστήσω ἐφ’ ὑμᾶς τὴν ἀπορίαν τήν τε ψώραν καὶ τὸν ἴκτερον καὶ σφακελίζοντας τοὺς ὀφθαλμοὺς ὑμῶν καὶ τὴν ψυχὴν ὑμῶν ἐκτήκουσαν καὶ σπερεῖτε διὰ κενῆς τὰ σπέρματα ὑμῶν καὶ ἔδονται οἱ ὑπεναντίοι ὑμῶν
gr_gr_cog~LEV~C026~017~καὶ ἐπιστήσω τὸ πρόσωπόν μου ἐφ’ ὑμᾶς καὶ πεσεῖσθε ἐναντίον τῶν ἐχθρῶν ὑμῶν καὶ διώξονται ὑμᾶς οἱ μισοῦντες ὑμᾶς καὶ φεύξεσθε οὐθενὸς διώκοντος ὑμᾶς
gr_gr_cog~LEV~C026~018~καὶ ἐὰν ἕως τούτου μὴ ὑπακούσητέ μου καὶ προσθήσω τοῦ παιδεῦσαι ὑμᾶς ἑπτάκις ἐπὶ ταῖς ἁμαρτίαις ὑμῶν
gr_gr_cog~LEV~C026~019~καὶ συντρίψω τὴν ὕβριν τῆς ὑπερηφανίας ὑμῶν καὶ θήσω τὸν οὐρανὸν ὑμῖν σιδηροῦν καὶ τὴν γῆν ὑμῶν ὡσεὶ χαλκῆν
gr_gr_cog~LEV~C026~020~καὶ ἔσται εἰς κενὸν ἡ ἰσχὺς ὑμῶν καὶ οὐ δώσει ἡ γῆ ὑμῶν τὸν σπόρον αὐτῆς καὶ τὸ ξύλον τοῦ ἀγροῦ ὑμῶν οὐ δώσει τὸν καρπὸν αὐτοῦ
gr_gr_cog~LEV~C026~021~καὶ ἐὰν μετὰ ταῦτα πορεύησθε πλάγιοι καὶ μὴ βούλησθε ὑπακούειν μου προσθήσω ὑμῖν πληγὰς ἑπτὰ κατὰ τὰς ἁμαρτίας ὑμῶν
gr_gr_cog~LEV~C026~022~καὶ ἀποστελῶ ἐφ’ ὑμᾶς τὰ θηρία τὰ ἄγρια τῆς γῆς καὶ κατέδεται ὑμᾶς καὶ ἐξαναλώσει τὰ κτήνη ὑμῶν καὶ ὀλιγοστοὺς ποιήσει ὑμᾶς καὶ ἐρημωθήσονται αἱ ὁδοὶ ὑμῶν
gr_gr_cog~LEV~C026~023~καὶ ἐπὶ τούτοις ἐὰν μὴ παιδευθῆτε ἀλλὰ πορεύησθε πρός με πλάγιοι
gr_gr_cog~LEV~C026~024~πορεύσομαι κἀγὼ μεθ’ ὑμῶν θυμῷ πλαγίῳ καὶ πατάξω ὑμᾶς κἀγὼ ἑπτάκις ἀντὶ τῶν ἁμαρτιῶν ὑμῶν
gr_gr_cog~LEV~C026~025~καὶ ἐπάξω ἐφ’ ὑμᾶς μάχαιραν ἐκδικοῦσαν δίκην διαθήκης καὶ καταφεύξεσθε εἰς τὰς πόλεις ὑμῶν καὶ ἐξαποστελῶ θάνατον εἰς ὑμᾶς καὶ παραδοθήσεσθε εἰς χεῖρας ἐχθρῶν
gr_gr_cog~LEV~C026~026~ἐν τῷ θλῖψαι ὑμᾶς σιτοδείᾳ ἄρτων καὶ πέψουσιν δέκα γυναῖκες τοὺς ἄρτους ὑμῶν ἐν κλιβάνῳ ἑνὶ καὶ ἀποδώσουσιν τοὺς ἄρτους ὑμῶν ἐν σταθμῷ καὶ φάγεσθε καὶ οὐ μὴ ἐμπλησθῆτε
gr_gr_cog~LEV~C026~027~ἐὰν δὲ ἐπὶ τούτοις μὴ ὑπακούσητέ μου καὶ πορεύησθε πρός με πλάγιοι
gr_gr_cog~LEV~C026~028~καὶ αὐτὸς πορεύσομαι μεθ’ ὑμῶν ἐν θυμῷ πλαγίῳ καὶ παιδεύσω ὑμᾶς ἐγὼ ἑπτάκις κατὰ τὰς ἁμαρτίας ὑμῶν
gr_gr_cog~LEV~C026~029~καὶ φάγεσθε τὰς σάρκας τῶν υἱῶν ὑμῶν καὶ τὰς σάρκας τῶν θυγατέρων ὑμῶν φάγεσθε
gr_gr_cog~LEV~C026~030~καὶ ἐρημώσω τὰς στήλας ὑμῶν καὶ ἐξολεθρεύσω τὰ ξύλινα χειροποίητα ὑμῶν καὶ θήσω τὰ κῶλα ὑμῶν ἐπὶ τὰ κῶλα τῶν εἰδώλων ὑμῶν καὶ προσοχθιεῖ ἡ ψυχή μου ὑμῖν
gr_gr_cog~LEV~C026~031~καὶ θήσω τὰς πόλεις ὑμῶν ἐρήμους καὶ ἐξερημώσω τὰ ἅγια ὑμῶν καὶ οὐ μὴ ὀσφρανθῶ τῆς ὀσμῆς τῶν θυσιῶν ὑμῶν
gr_gr_cog~LEV~C026~032~καὶ ἐξερημώσω ἐγὼ τὴν γῆν ὑμῶν καὶ θαυμάσονται ἐπ’ αὐτῇ οἱ ἐχθροὶ ὑμῶν οἱ ἐνοικοῦντες ἐν αὐτῇ
gr_gr_cog~LEV~C026~033~καὶ διασπερῶ ὑμᾶς εἰς τὰ ἔθνη καὶ ἐξαναλώσει ὑμᾶς ἐπιπορευομένη ἡ μάχαιρα καὶ ἔσται ἡ γῆ ὑμῶν ἔρημος καὶ αἱ πόλεις ὑμῶν ἔσονται ἔρημοι
gr_gr_cog~LEV~C026~034~τότε εὐδοκήσει ἡ γῆ τὰ σάββατα αὐτῆς καὶ πάσας τὰς ἡμέρας τῆς ἐρημώσεως αὐτῆς καὶ ὑμεῖς ἔσεσθε ἐν τῇ γῇ τῶν ἐχθρῶν ὑμῶν τότε σαββατιεῖ ἡ γῆ καὶ εὐδοκήσει τὰ σάββατα αὐτῆς
gr_gr_cog~LEV~C026~035~πάσας τὰς ἡμέρας τῆς ἐρημώσεως αὐτῆς σαββατιεῖ ἃ οὐκ ἐσαββάτισεν ἐν τοῖς σαββάτοις ὑμῶν ἡνίκα κατῳκεῖτε αὐτήν
gr_gr_cog~LEV~C026~036~καὶ τοῖς καταλειφθεῖσιν ἐξ ὑμῶν ἐπάξω δειλίαν εἰς τὴν καρδίαν αὐτῶν ἐν τῇ γῇ τῶν ἐχθρῶν αὐτῶν καὶ διώξεται αὐτοὺς φωνὴ φύλλου φερομένου καὶ φεύξονται ὡς φεύγοντες ἀπὸ πολέμου καὶ πεσοῦνται οὐθενὸς διώκοντος
gr_gr_cog~LEV~C026~037~καὶ ὑπερόψεται ὁ ἀδελφὸς τὸν ἀδελφὸν ὡσεὶ ἐν πολέμῳ οὐθενὸς κατατρέχοντος καὶ οὐ δυνήσεσθε ἀντιστῆναι τοῖς ἐχθροῖς ὑμῶν
gr_gr_cog~LEV~C026~038~καὶ ἀπολεῖσθε ἐν τοῖς ἔθνεσιν καὶ κατέδεται ὑμᾶς ἡ γῆ τῶν ἐχθρῶν ὑμῶν
gr_gr_cog~LEV~C026~039~καὶ οἱ καταλειφθέντες ἀφ’ ὑμῶν καταφθαρήσονται διὰ τὰς ἁμαρτίας ὑμῶν ἐν τῇ γῇ τῶν ἐχθρῶν αὐτῶν τακήσονται
gr_gr_cog~LEV~C026~040~καὶ ἐξαγορεύσουσιν τὰς ἁμαρτίας αὐτῶν καὶ τὰς ἁμαρτίας τῶν πατέρων αὐτῶν ὅτι παρέβησαν καὶ ὑπερεῖδόν με καὶ ὅτι ἐπορεύθησαν ἐναντίον μου πλάγιοι
gr_gr_cog~LEV~C026~041~καὶ ἐγὼ ἐπορεύθην μετ’ αὐτῶν ἐν θυμῷ πλαγίῳ καὶ ἀπολῶ αὐτοὺς ἐν τῇ γῇ τῶν ἐχθρῶν αὐτῶν τότε ἐντραπήσεται ἡ καρδία αὐτῶν ἡ ἀπερίτμητος καὶ τότε εὐδοκήσουσιν τὰς ἁμαρτίας αὐτῶν
gr_gr_cog~LEV~C026~042~καὶ μνησθήσομαι τῆς διαθήκης Ιακωβ καὶ τῆς διαθήκης Ισαακ καὶ τῆς διαθήκης Αβρααμ μνησθήσομαι καὶ τῆς γῆς μνησθήσομαι
gr_gr_cog~LEV~C026~043~καὶ ἡ γῆ ἐγκαταλειφθήσεται ὑπ’ αὐτῶν τότε προσδέξεται ἡ γῆ τὰ σάββατα αὐτῆς ἐν τῷ ἐρημωθῆναι αὐτὴν δι’ αὐτούς καὶ αὐτοὶ προσδέξονται τὰς αὐτῶν ἀνομίας ἀνθ’ ὧν τὰ κρίματά μου ὑπερεῖδον καὶ τοῖς προστάγμασίν μου προσώχθισαν τῇ ψυχῇ αὐτῶν
gr_gr_cog~LEV~C026~044~καὶ οὐδ’ ὧς ὄντων αὐτῶν ἐν τῇ γῇ τῶν ἐχθρῶν αὐτῶν οὐχ ὑπερεῖδον αὐτοὺς οὐδὲ προσώχθισα αὐτοῖς ὥστε ἐξαναλῶσαι αὐτοὺς τοῦ διασκεδάσαι τὴν διαθήκην μου τὴν πρὸς αὐτούς ὅτι ἐγώ εἰμι κύριος ὁ θεὸς αὐτῶν
gr_gr_cog~LEV~C026~045~καὶ μνησθήσομαι αὐτῶν τῆς διαθήκης τῆς προτέρας ὅτε ἐξήγαγον αὐτοὺς ἐκ γῆς Αἰγύπτου ἐξ οἴκου δουλείας ἔναντι τῶν ἐθνῶν τοῦ εἶναι αὐτῶν θεός ἐγώ εἰμι κύριος
gr_gr_cog~LEV~C026~046~ταῦτα τὰ κρίματα καὶ τὰ προστάγματα καὶ ὁ νόμος ὃν ἔδωκεν κύριος ἀνὰ μέσον αὐτοῦ καὶ ἀνὰ μέσον τῶν υἱῶν Ισραηλ ἐν τῷ ὄρει Σινα ἐν χειρὶ Μωυσῆ
