gr_gr_cog~LEV~C013~001~καὶ ἐλάλησεν κύριος πρὸς Μωυσῆν καὶ Ααρων λέγων
gr_gr_cog~LEV~C013~002~ἀνθρώπῳ ἐάν τινι γένηται ἐν δέρματι χρωτὸς αὐτοῦ οὐλὴ σημασίας τηλαυγὴς καὶ γένηται ἐν δέρματι χρωτὸς αὐτοῦ ἁφὴ λέπρας καὶ ἀχθήσεται πρὸς Ααρων τὸν ἱερέα ἢ ἕνα τῶν υἱῶν αὐτοῦ τῶν ἱερέων
gr_gr_cog~LEV~C013~003~καὶ ὄψεται ὁ ἱερεὺς τὴν ἁφὴν ἐν δέρματι τοῦ χρωτὸς αὐτοῦ καὶ ἡ θρὶξ ἐν τῇ ἁφῇ μεταβάλῃ λευκή καὶ ἡ ὄψις τῆς ἁφῆς ταπεινὴ ἀπὸ τοῦ δέρματος τοῦ χρωτός ἁφὴ λέπρας ἐστίν καὶ ὄψεται ὁ ἱερεὺς καὶ μιανεῖ αὐτόν
gr_gr_cog~LEV~C013~004~ἐὰν δὲ τηλαυγὴς λευκὴ ᾖ ἐν τῷ δέρματι τοῦ χρωτός καὶ ταπεινὴ μὴ ᾖ ἡ ὄψις αὐτῆς ἀπὸ τοῦ δέρματος καὶ ἡ θρὶξ αὐτοῦ οὐ μετέβαλεν τρίχα λευκήν αὐτὴ δέ ἐστιν ἀμαυρά καὶ ἀφοριεῖ ὁ ἱερεὺς τὴν ἁφὴν ἑπτὰ ἡμέρας
gr_gr_cog~LEV~C013~005~καὶ ὄψεται ὁ ἱερεὺς τὴν ἁφὴν τῇ ἡμέρᾳ τῇ ἑβδόμῃ καὶ ἰδοὺ ἡ ἁφὴ μένει ἐναντίον αὐτοῦ οὐ μετέπεσεν ἡ ἁφὴ ἐν τῷ δέρματι καὶ ἀφοριεῖ αὐτὸν ὁ ἱερεὺς ἑπτὰ ἡμέρας τὸ δεύτερον
gr_gr_cog~LEV~C013~006~καὶ ὄψεται αὐτὸν ὁ ἱερεὺς τῇ ἡμέρᾳ τῇ ἑβδόμῃ τὸ δεύτερον καὶ ἰδοὺ ἀμαυρὰ ἡ ἁφή οὐ μετέπεσεν ἡ ἁφὴ ἐν τῷ δέρματι καθαριεῖ αὐτὸν ὁ ἱερεύς σημασία γάρ ἐστιν καὶ πλυνάμενος τὰ ἱμάτια καθαρὸς ἔσται
gr_gr_cog~LEV~C013~007~ἐὰν δὲ μεταβαλοῦσα μεταπέσῃ ἡ σημασία ἐν τῷ δέρματι μετὰ τὸ ἰδεῖν αὐτὸν τὸν ἱερέα τοῦ καθαρίσαι αὐτόν καὶ ὀφθήσεται τὸ δεύτερον τῷ ἱερεῖ
gr_gr_cog~LEV~C013~008~καὶ ὄψεται αὐτὸν ὁ ἱερεὺς καὶ ἰδοὺ μετέπεσεν ἡ σημασία ἐν τῷ δέρματι καὶ μιανεῖ αὐτὸν ὁ ἱερεύς λέπρα ἐστίν
gr_gr_cog~LEV~C013~009~καὶ ἁφὴ λέπρας ἐὰν γένηται ἐν ἀνθρώπῳ καὶ ἥξει πρὸς τὸν ἱερέα
gr_gr_cog~LEV~C013~010~καὶ ὄψεται ὁ ἱερεὺς καὶ ἰδοὺ οὐλὴ λευκὴ ἐν τῷ δέρματι καὶ αὕτη μετέβαλεν τρίχα λευκήν καὶ ἀπὸ τοῦ ὑγιοῦς τῆς σαρκὸς τῆς ζώσης ἐν τῇ οὐλῇ
gr_gr_cog~LEV~C013~011~λέπρα παλαιουμένη ἐστίν ἐν τῷ δέρματι τοῦ χρωτός ἐστιν καὶ μιανεῖ αὐτὸν ὁ ἱερεὺς καὶ ἀφοριεῖ αὐτόν ὅτι ἀκάθαρτός ἐστιν
gr_gr_cog~LEV~C013~012~ἐὰν δὲ ἐξανθοῦσα ἐξανθήσῃ ἡ λέπρα ἐν τῷ δέρματι καὶ καλύψῃ ἡ λέπρα πᾶν τὸ δέρμα τῆς ἁφῆς ἀπὸ κεφαλῆς ἕως ποδῶν καθ’ ὅλην τὴν ὅρασιν τοῦ ἱερέως
gr_gr_cog~LEV~C013~013~καὶ ὄψεται ὁ ἱερεὺς καὶ ἰδοὺ ἐκάλυψεν ἡ λέπρα πᾶν τὸ δέρμα τοῦ χρωτός καὶ καθαριεῖ αὐτὸν ὁ ἱερεὺς τὴν ἁφήν ὅτι πᾶν μετέβαλεν λευκόν καθαρόν ἐστιν
gr_gr_cog~LEV~C013~014~καὶ ᾗ ἂν ἡμέρᾳ ὀφθῇ ἐν αὐτῷ χρὼς ζῶν μιανθήσεται
gr_gr_cog~LEV~C013~015~καὶ ὄψεται ὁ ἱερεὺς τὸν χρῶτα τὸν ὑγιῆ καὶ μιανεῖ αὐτὸν ὁ χρὼς ὁ ὑγιής ὅτι ἀκάθαρτός ἐστιν λέπρα ἐστίν
gr_gr_cog~LEV~C013~016~ἐὰν δὲ ἀποκαταστῇ ὁ χρὼς ὁ ὑγιὴς καὶ μεταβάλῃ λευκή καὶ ἐλεύσεται πρὸς τὸν ἱερέα
gr_gr_cog~LEV~C013~017~καὶ ὄψεται ὁ ἱερεὺς καὶ ἰδοὺ μετέβαλεν ἡ ἁφὴ εἰς τὸ λευκόν καὶ καθαριεῖ ὁ ἱερεὺς τὴν ἁφήν καθαρός ἐστιν
gr_gr_cog~LEV~C013~018~καὶ σὰρξ ἐὰν γένηται ἐν τῷ δέρματι αὐτοῦ ἕλκος καὶ ὑγιασθῇ
gr_gr_cog~LEV~C013~019~καὶ γένηται ἐν τῷ τόπῳ τοῦ ἕλκους οὐλὴ λευκὴ ἢ τηλαυγὴς λευκαίνουσα ἢ πυρρίζουσα καὶ ὀφθήσεται τῷ ἱερεῖ
gr_gr_cog~LEV~C013~020~καὶ ὄψεται ὁ ἱερεὺς καὶ ἰδοὺ ἡ ὄψις ταπεινοτέρα τοῦ δέρματος καὶ ἡ θρὶξ αὐτῆς μετέβαλεν εἰς λευκήν καὶ μιανεῖ αὐτὸν ὁ ἱερεύς λέπρα ἐστίν ἐν τῷ ἕλκει ἐξήνθησεν
gr_gr_cog~LEV~C013~021~ἐὰν δὲ ἴδῃ ὁ ἱερεὺς καὶ ἰδοὺ οὐκ ἔστιν ἐν αὐτῷ θρὶξ λευκή καὶ ταπεινὸν μὴ ᾖ ἀπὸ τοῦ δέρματος τοῦ χρωτός καὶ αὐτὴ ᾖ ἀμαυρά ἀφοριεῖ αὐτὸν ὁ ἱερεὺς ἑπτὰ ἡμέρας
gr_gr_cog~LEV~C013~022~ἐὰν δὲ διαχέηται ἐν τῷ δέρματι καὶ μιανεῖ αὐτὸν ὁ ἱερεύς ἁφὴ λέπρας ἐστίν ἐν τῷ ἕλκει ἐξήνθησεν
gr_gr_cog~LEV~C013~023~ἐὰν δὲ κατὰ χώραν μείνῃ τὸ τηλαύγημα καὶ μὴ διαχέηται οὐλὴ τοῦ ἕλκους ἐστίν καὶ καθαριεῖ αὐτὸν ὁ ἱερεύς
gr_gr_cog~LEV~C013~024~καὶ σὰρξ ἐὰν γένηται ἐν τῷ δέρματι αὐτοῦ κατάκαυμα πυρός καὶ γένηται ἐν τῷ δέρματι αὐτοῦ τὸ ὑγιασθὲν τοῦ κατακαύματος αὐγάζον τηλαυγὲς λευκὸν ὑποπυρρίζον ἢ ἔκλευκον
gr_gr_cog~LEV~C013~025~καὶ ὄψεται αὐτὸν ὁ ἱερεὺς καὶ ἰδοὺ μετέβαλεν θρὶξ λευκὴ εἰς τὸ αὐγάζον καὶ ἡ ὄψις αὐτοῦ ταπεινὴ ἀπὸ τοῦ δέρματος λέπρα ἐστίν ἐν τῷ κατακαύματι ἐξήνθησεν καὶ μιανεῖ αὐτὸν ὁ ἱερεύς ἁφὴ λέπρας ἐστίν
gr_gr_cog~LEV~C013~026~ἐὰν δὲ ἴδῃ ὁ ἱερεὺς καὶ ἰδοὺ οὐκ ἔστιν ἐν τῷ αὐγάζοντι θρὶξ λευκή καὶ ταπεινὸν μὴ ᾖ ἀπὸ τοῦ δέρματος αὐτὸ δὲ ἀμαυρόν καὶ ἀφοριεῖ αὐτὸν ὁ ἱερεὺς ἑπτὰ ἡμέρας
gr_gr_cog~LEV~C013~027~καὶ ὄψεται αὐτὸν ὁ ἱερεὺς τῇ ἡμέρᾳ τῇ ἑβδόμῃ ἐὰν δὲ διαχύσει διαχέηται ἐν τῷ δέρματι καὶ μιανεῖ αὐτὸν ὁ ἱερεύς ἁφὴ λέπρας ἐστίν ἐν τῷ ἕλκει ἐξήνθησεν
gr_gr_cog~LEV~C013~028~ἐὰν δὲ κατὰ χώραν μείνῃ τὸ αὐγάζον καὶ μὴ διαχυθῇ ἐν τῷ δέρματι αὐτὴ δὲ ᾖ ἀμαυρά ἡ οὐλὴ τοῦ κατακαύματός ἐστιν καὶ καθαριεῖ αὐτὸν ὁ ἱερεύς ὁ γὰρ χαρακτὴρ τοῦ κατακαύματός ἐστιν
gr_gr_cog~LEV~C013~029~καὶ ἀνδρὶ καὶ γυναικὶ ἐὰν γένηται ἐν αὐτοῖς ἁφὴ λέπρας ἐν τῇ κεφαλῇ ἢ ἐν τῷ πώγωνι
gr_gr_cog~LEV~C013~030~καὶ ὄψεται ὁ ἱερεὺς τὴν ἁφὴν καὶ ἰδοὺ ἡ ὄψις αὐτῆς ἐγκοιλοτέρα τοῦ δέρματος ἐν αὐτῇ δὲ θρὶξ ξανθίζουσα λεπτή καὶ μιανεῖ αὐτὸν ὁ ἱερεύς θραῦσμά ἐστιν λέπρα τῆς κεφαλῆς ἢ λέπρα τοῦ πώγωνός ἐστιν
gr_gr_cog~LEV~C013~031~καὶ ἐὰν ἴδῃ ὁ ἱερεὺς τὴν ἁφὴν τοῦ θραύσματος καὶ ἰδοὺ οὐχ ἡ ὄψις ἐγκοιλοτέρα τοῦ δέρματος καὶ θρὶξ ξανθίζουσα οὐκ ἔστιν ἐν αὐτῇ καὶ ἀφοριεῖ ὁ ἱερεὺς τὴν ἁφὴν τοῦ θραύσματος ἑπτὰ ἡμέρας
gr_gr_cog~LEV~C013~032~καὶ ὄψεται ὁ ἱερεὺς τὴν ἁφὴν τῇ ἡμέρᾳ τῇ ἑβδόμῃ καὶ ἰδοὺ οὐ διεχύθη τὸ θραῦσμα καὶ θρὶξ ξανθίζουσα οὐκ ἔστιν ἐν αὐτῇ καὶ ἡ ὄψις τοῦ θραύσματος οὐκ ἔστιν κοίλη ἀπὸ τοῦ δέρματος
gr_gr_cog~LEV~C013~033~καὶ ξυρηθήσεται τὸ δέρμα τὸ δὲ θραῦσμα οὐ ξυρηθήσεται καὶ ἀφοριεῖ ὁ ἱερεὺς τὸ θραῦσμα ἑπτὰ ἡμέρας τὸ δεύτερον
gr_gr_cog~LEV~C013~034~καὶ ὄψεται ὁ ἱερεὺς τὸ θραῦσμα τῇ ἡμέρᾳ τῇ ἑβδόμῃ καὶ ἰδοὺ οὐ διεχύθη τὸ θραῦσμα ἐν τῷ δέρματι μετὰ τὸ ξυρηθῆναι αὐτόν καὶ ἡ ὄψις τοῦ θραύσματος οὐκ ἔστιν κοίλη ἀπὸ τοῦ δέρματος καὶ καθαριεῖ αὐτὸν ὁ ἱερεύς καὶ πλυνάμενος τὰ ἱμάτια καθαρὸς ἔσται
gr_gr_cog~LEV~C013~035~ἐὰν δὲ διαχύσει διαχέηται τὸ θραῦσμα ἐν τῷ δέρματι μετὰ τὸ καθαρισθῆναι αὐτόν
gr_gr_cog~LEV~C013~036~καὶ ὄψεται ὁ ἱερεὺς καὶ ἰδοὺ διακέχυται τὸ θραῦσμα ἐν τῷ δέρματι οὐκ ἐπισκέψεται ὁ ἱερεὺς περὶ τῆς τριχὸς τῆς ξανθῆς ὅτι ἀκάθαρτός ἐστιν
gr_gr_cog~LEV~C013~037~ἐὰν δὲ ἐνώπιον μείνῃ τὸ θραῦσμα ἐπὶ χώρας καὶ θρὶξ μέλαινα ἀνατείλῃ ἐν αὐτῷ ὑγίακεν τὸ θραῦσμα καθαρός ἐστιν καὶ καθαριεῖ αὐτὸν ὁ ἱερεύς
gr_gr_cog~LEV~C013~038~καὶ ἀνδρὶ ἢ γυναικὶ ἐὰν γένηται ἐν δέρματι τῆς σαρκὸς αὐτοῦ αὐγάσματα αὐγάζοντα λευκαθίζοντα
gr_gr_cog~LEV~C013~039~καὶ ὄψεται ὁ ἱερεὺς καὶ ἰδοὺ ἐν δέρματι τῆς σαρκὸς αὐτοῦ αὐγάσματα αὐγάζοντα λευκαθίζοντα ἀλφός ἐστιν καθαρός ἐστιν ἐξανθεῖ ἐν τῷ δέρματι τῆς σαρκὸς αὐτοῦ καθαρός ἐστιν
gr_gr_cog~LEV~C013~040~ἐὰν δέ τινι μαδήσῃ ἡ κεφαλὴ αὐτοῦ φαλακρός ἐστιν καθαρός ἐστιν
gr_gr_cog~LEV~C013~041~ἐὰν δὲ κατὰ πρόσωπον μαδήσῃ ἡ κεφαλὴ αὐτοῦ ἀναφάλαντός ἐστιν καθαρός ἐστιν
gr_gr_cog~LEV~C013~042~ἐὰν δὲ γένηται ἐν τῷ φαλακρώματι αὐτοῦ ἢ ἐν τῷ ἀναφαλαντώματι αὐτοῦ ἁφὴ λευκὴ ἢ πυρρίζουσα λέπρα ἐστὶν ἐν τῷ φαλακρώματι αὐτοῦ ἢ ἐν τῷ ἀναφαλαντώματι αὐτοῦ
gr_gr_cog~LEV~C013~043~καὶ ὄψεται αὐτὸν ὁ ἱερεὺς καὶ ἰδοὺ ἡ ὄψις τῆς ἁφῆς λευκὴ πυρρίζουσα ἐν τῷ φαλακρώματι αὐτοῦ ἢ ἐν τῷ ἀναφαλαντώματι αὐτοῦ ὡς εἶδος λέπρας ἐν δέρματι τῆς σαρκὸς αὐτοῦ
gr_gr_cog~LEV~C013~044~ἄνθρωπος λεπρός ἐστιν μιάνσει μιανεῖ αὐτὸν ὁ ἱερεύς ἐν τῇ κεφαλῇ αὐτοῦ ἡ ἁφὴ αὐτοῦ
gr_gr_cog~LEV~C013~045~καὶ ὁ λεπρός ἐν ᾧ ἐστιν ἡ ἁφή τὰ ἱμάτια αὐτοῦ ἔστω παραλελυμένα καὶ ἡ κεφαλὴ αὐτοῦ ἀκατακάλυπτος καὶ περὶ τὸ στόμα αὐτοῦ περιβαλέσθω καὶ ἀκάθαρτος κεκλήσεται
gr_gr_cog~LEV~C013~046~πάσας τὰς ἡμέρας ὅσας ἂν ᾖ ἐπ’ αὐτοῦ ἡ ἁφή ἀκάθαρτος ὢν ἀκάθαρτος ἔσται κεχωρισμένος καθήσεται ἔξω τῆς παρεμβολῆς ἔσται αὐτοῦ ἡ διατριβή
gr_gr_cog~LEV~C013~047~καὶ ἱματίῳ ἐὰν γένηται ἐν αὐτῷ ἁφὴ λέπρας ἐν ἱματίῳ ἐρεῷ ἢ ἐν ἱματίῳ στιππυίνῳ
gr_gr_cog~LEV~C013~048~ἢ ἐν στήμονι ἢ ἐν κρόκῃ ἢ ἐν τοῖς λινοῖς ἢ ἐν τοῖς ἐρεοῖς ἢ ἐν δέρματι ἢ ἐν παντὶ ἐργασίμῳ δέρματι
gr_gr_cog~LEV~C013~049~καὶ γένηται ἡ ἁφὴ χλωρίζουσα ἢ πυρρίζουσα ἐν τῷ δέρματι ἢ ἐν τῷ ἱματίῳ ἢ ἐν τῷ στήμονι ἢ ἐν τῇ κρόκῃ ἢ ἐν παντὶ σκεύει ἐργασίμῳ δέρματος ἁφὴ λέπρας ἐστίν καὶ δείξει τῷ ἱερεῖ
gr_gr_cog~LEV~C013~050~καὶ ὄψεται ὁ ἱερεὺς τὴν ἁφήν καὶ ἀφοριεῖ ὁ ἱερεὺς τὴν ἁφὴν ἑπτὰ ἡμέρας
gr_gr_cog~LEV~C013~051~καὶ ὄψεται ὁ ἱερεὺς τὴν ἁφὴν τῇ ἡμέρᾳ τῇ ἑβδόμῃ ἐὰν δὲ διαχέηται ἡ ἁφὴ ἐν τῷ ἱματίῳ ἢ ἐν τῷ στήμονι ἢ ἐν τῇ κρόκῃ ἢ ἐν τῷ δέρματι κατὰ πάντα ὅσα ἂν ποιηθῇ δέρματα ἐν τῇ ἐργασίᾳ λέπρα ἔμμονός ἐστιν ἡ ἁφή ἀκάθαρτός ἐστιν
gr_gr_cog~LEV~C013~052~κατακαύσει τὸ ἱμάτιον ἢ τὸν στήμονα ἢ τὴν κρόκην ἐν τοῖς ἐρεοῖς ἢ ἐν τοῖς λινοῖς ἢ ἐν παντὶ σκεύει δερματίνῳ ἐν ᾧ ἐὰν ᾖ ἐν αὐτῷ ἡ ἁφή ὅτι λέπρα ἔμμονός ἐστιν ἐν πυρὶ κατακαυθήσεται
gr_gr_cog~LEV~C013~053~ἐὰν δὲ ἴδῃ ὁ ἱερεὺς καὶ μὴ διαχέηται ἡ ἁφὴ ἐν τῷ ἱματίῳ ἢ ἐν τῷ στήμονι ἢ ἐν τῇ κρόκῃ ἢ ἐν παντὶ σκεύει δερματίνῳ
gr_gr_cog~LEV~C013~054~καὶ συντάξει ὁ ἱερεύς καὶ πλυνεῖ ἐφ’ οὗ ἐὰν ᾖ ἐπ’ αὐτοῦ ἡ ἁφή καὶ ἀφοριεῖ ὁ ἱερεὺς τὴν ἁφὴν ἑπτὰ ἡμέρας τὸ δεύτερον
gr_gr_cog~LEV~C013~055~καὶ ὄψεται ὁ ἱερεὺς μετὰ τὸ πλυθῆναι αὐτὸ τὴν ἁφήν καὶ ἥδε μὴ μετέβαλεν τὴν ὄψιν ἡ ἁφή καὶ ἡ ἁφὴ οὐ διαχεῖται ἀκάθαρτόν ἐστιν ἐν πυρὶ κατακαυθήσεται ἐστήρισται ἐν τῷ ἱματίῳ ἢ ἐν τῷ στήμονι ἢ ἐν τῇ κρόκῃ
gr_gr_cog~LEV~C013~056~καὶ ἐὰν ἴδῃ ὁ ἱερεὺς καὶ ᾖ ἀμαυρὰ ἡ ἁφὴ μετὰ τὸ πλυθῆναι αὐτό ἀπορρήξει αὐτὸ ἀπὸ τοῦ ἱματίου ἢ ἀπὸ τοῦ δέρματος ἢ ἀπὸ τοῦ στήμονος ἢ ἀπὸ τῆς κρόκης
gr_gr_cog~LEV~C013~057~ἐὰν δὲ ὀφθῇ ἔτι ἐν τῷ ἱματίῳ ἢ ἐν τῷ στήμονι ἢ ἐν τῇ κρόκῃ ἢ ἐν παντὶ σκεύει δερματίνῳ λέπρα ἐξανθοῦσά ἐστιν ἐν πυρὶ κατακαυθήσεται ἐν ᾧ ἐστὶν ἡ ἁφή
gr_gr_cog~LEV~C013~058~καὶ τὸ ἱμάτιον ἢ ὁ στήμων ἢ ἡ κρόκη ἢ πᾶν σκεῦος δερμάτινον ὃ πλυθήσεται καὶ ἀποστήσεται ἀπ’ αὐτοῦ ἡ ἁφή καὶ πλυθήσεται τὸ δεύτερον καὶ καθαρὸν ἔσται
gr_gr_cog~LEV~C013~059~οὗτος ὁ νόμος ἁφῆς λέπρας ἱματίου ἐρεοῦ ἢ στιππυίνου ἢ στήμονος ἢ κρόκης ἢ παντὸς σκεύους δερματίνου εἰς τὸ καθαρίσαι αὐτὸ ἢ μιᾶναι αὐτό
