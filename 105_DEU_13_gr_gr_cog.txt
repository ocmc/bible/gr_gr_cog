gr_gr_cog~DEU~C013~001~πᾶν ῥῆμα ὃ ἐγὼ ἐντέλλομαί σοι σήμερον τοῦτο φυλάξῃ ποιεῖν οὐ προσθήσεις ἐπ’ αὐτὸ οὐδὲ ἀφελεῖς ἀπ’ αὐτοῦ
gr_gr_cog~DEU~C013~002~ἐὰν δὲ ἀναστῇ ἐν σοὶ προφήτης ἢ ἐνυπνιαζόμενος ἐνύπνιον καὶ δῷ σοι σημεῖον ἢ τέρας
gr_gr_cog~DEU~C013~003~καὶ ἔλθῃ τὸ σημεῖον ἢ τὸ τέρας ὃ ἐλάλησεν πρὸς σὲ λέγων πορευθῶμεν καὶ λατρεύσωμεν θεοῖς ἑτέροις οὓς οὐκ οἴδατε
gr_gr_cog~DEU~C013~004~οὐκ ἀκούσεσθε τῶν λόγων τοῦ προφήτου ἐκείνου ἢ τοῦ ἐνυπνιαζομένου τὸ ἐνύπνιον ἐκεῖνο ὅτι πειράζει κύριος ὁ θεὸς ὑμᾶς εἰδέναι εἰ ἀγαπᾶτε κύριον τὸν θεὸν ὑμῶν ἐξ ὅλης τῆς καρδίας ὑμῶν καὶ ἐξ ὅλης τῆς ψυχῆς ὑμῶν
gr_gr_cog~DEU~C013~005~ὀπίσω κυρίου τοῦ θεοῦ ὑμῶν πορεύεσθε καὶ αὐτὸν φοβηθήσεσθε καὶ τὰς ἐντολὰς αὐτοῦ φυλάξεσθε καὶ τῆς φωνῆς αὐτοῦ ἀκούσεσθε καὶ αὐτῷ προστεθήσεσθε
gr_gr_cog~DEU~C013~006~καὶ ὁ προφήτης ἐκεῖνος ἢ ὁ τὸ ἐνύπνιον ἐνυπνιαζόμενος ἐκεῖνος ἀποθανεῖται ἐλάλησεν γὰρ πλανῆσαί σε ἀπὸ κυρίου τοῦ θεοῦ σου τοῦ ἐξαγαγόντος σε ἐκ γῆς Αἰγύπτου τοῦ λυτρωσαμένου σε ἐκ τῆς δουλείας ἐξῶσαί σε ἐκ τῆς ὁδοῦ ἧς ἐνετείλατό σοι κύριος ὁ θεός σου πορεύεσθαι ἐν αὐτῇ καὶ ἀφανιεῖς τὸν πονηρὸν ἐξ ὑμῶν αὐτῶν
gr_gr_cog~DEU~C013~007~ἐὰν δὲ παρακαλέσῃ σε ὁ ἀδελφός σου ἐκ πατρός σου ἢ ἐκ μητρός σου ἢ ὁ υἱός σου ἢ ἡ θυγάτηρ σου ἢ ἡ γυνὴ ἡ ἐν κόλπῳ σου ἢ ὁ φίλος ὁ ἴσος τῆς ψυχῆς σου λάθρᾳ λέγων βαδίσωμεν καὶ λατρεύσωμεν θεοῖς ἑτέροις οὓς οὐκ ᾔδεις σὺ καὶ οἱ πατέρες σου
gr_gr_cog~DEU~C013~008~ἀπὸ τῶν θεῶν τῶν ἐθνῶν τῶν περικύκλῳ ὑμῶν τῶν ἐγγιζόντων σοι ἢ τῶν μακρὰν ἀπὸ σοῦ ἀπ’ ἄκρου τῆς γῆς ἕως ἄκρου τῆς γῆς
gr_gr_cog~DEU~C013~009~οὐ συνθελήσεις αὐτῷ καὶ οὐκ εἰσακούσῃ αὐτοῦ καὶ οὐ φείσεται ὁ ὀφθαλμός σου ἐπ’ αὐτῷ οὐκ ἐπιποθήσεις ἐπ’ αὐτῷ οὐδ’ οὐ μὴ σκεπάσῃς αὐτόν
gr_gr_cog~DEU~C013~010~ἀναγγέλλων ἀναγγελεῖς περὶ αὐτοῦ αἱ χεῖρές σου ἔσονται ἐπ’ αὐτὸν ἐν πρώτοις ἀποκτεῖναι αὐτόν καὶ αἱ χεῖρες παντὸς τοῦ λαοῦ ἐπ’ ἐσχάτῳ
gr_gr_cog~DEU~C013~011~καὶ λιθοβολήσουσιν αὐτὸν ἐν λίθοις καὶ ἀποθανεῖται ὅτι ἐζήτησεν ἀποστῆσαί σε ἀπὸ κυρίου τοῦ θεοῦ σου τοῦ ἐξαγαγόντος σε ἐκ γῆς Αἰγύπτου ἐξ οἴκου δουλείας
gr_gr_cog~DEU~C013~012~καὶ πᾶς Ισραηλ ἀκούσας φοβηθήσεται καὶ οὐ προσθήσουσιν ἔτι ποιῆσαι κατὰ τὸ ῥῆμα τὸ πονηρὸν τοῦτο ἐν ὑμῖν
gr_gr_cog~DEU~C013~013~ἐὰν δὲ ἀκούσῃς ἐν μιᾷ τῶν πόλεών σου ὧν κύριος ὁ θεός σου δίδωσίν σοι κατοικεῖν σε ἐκεῖ λεγόντων
gr_gr_cog~DEU~C013~014~ἐξήλθοσαν ἄνδρες παράνομοι ἐξ ὑμῶν καὶ ἀπέστησαν πάντας τοὺς κατοικοῦντας τὴν πόλιν αὐτῶν λέγοντες πορευθῶμεν καὶ λατρεύσωμεν θεοῖς ἑτέροις οὓς οὐκ ᾔδειτε
gr_gr_cog~DEU~C013~015~καὶ ἐρωτήσεις καὶ ἐραυνήσεις σφόδρα καὶ ἰδοὺ ἀληθὴς σαφῶς ὁ λόγος γεγένηται τὸ βδέλυγμα τοῦτο ἐν ὑμῖν
gr_gr_cog~DEU~C013~016~ἀναιρῶν ἀνελεῖς πάντας τοὺς κατοικοῦντας ἐν τῇ πόλει ἐκείνῃ ἐν φόνῳ μαχαίρας ἀναθέματι ἀναθεματιεῖτε αὐτὴν καὶ πάντα τὰ ἐν αὐτῇ
gr_gr_cog~DEU~C013~017~καὶ πάντα τὰ σκῦλα αὐτῆς συνάξεις εἰς τὰς διόδους αὐτῆς καὶ ἐμπρήσεις τὴν πόλιν ἐν πυρὶ καὶ πάντα τὰ σκῦλα αὐτῆς πανδημεὶ ἐναντίον κυρίου τοῦ θεοῦ σου καὶ ἔσται ἀοίκητος εἰς τὸν αἰῶνα οὐκ ἀνοικοδομηθήσεται ἔτι
gr_gr_cog~DEU~C013~018~οὐ προσκολληθήσεται ἐν τῇ χειρί σου οὐδὲν ἀπὸ τοῦ ἀναθέματος ἵνα ἀποστραφῇ κύριος ἀπὸ θυμοῦ τῆς ὀργῆς αὐτοῦ καὶ δώσει σοι ἔλεος καὶ ἐλεήσει σε καὶ πληθυνεῖ σε ὃν τρόπον ὤμοσεν κύριος τοῖς πατράσιν σου
gr_gr_cog~DEU~C013~019~ἐὰν ἀκούσῃς τῆς φωνῆς κυρίου τοῦ θεοῦ σου φυλάσσειν πάσας τὰς ἐντολὰς αὐτοῦ ὅσας ἐγὼ ἐντέλλομαί σοι σήμερον ποιεῖν τὸ καλὸν καὶ τὸ ἀρεστὸν ἐναντίον κυρίου τοῦ θεοῦ σου
