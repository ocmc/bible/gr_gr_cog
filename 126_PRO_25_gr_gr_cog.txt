gr_gr_cog~PRO~C025~001~αὗται αἱ παιδεῖαι σαλωμῶντος αἱ ἀδιάκριτοι ἃς ἐξεγράψαντο οἱ φίλοι Εζεκιου τοῦ βασιλέως τῆς Ιουδαίας
gr_gr_cog~PRO~C025~002~δόξα θεοῦ κρύπτει λόγον δόξα δὲ βασιλέως τιμᾷ πράγματα
gr_gr_cog~PRO~C025~003~οὐρανὸς ὑψηλός γῆ δὲ βαθεῖα καρδία δὲ βασιλέως ἀνεξέλεγκτος
gr_gr_cog~PRO~C025~004~τύπτε ἀδόκιμον ἀργύριον καὶ καθαρισθήσεται καθαρὸν ἅπαν
gr_gr_cog~PRO~C025~005~κτεῖνε ἀσεβεῖς ἐκ προσώπου βασιλέως καὶ κατορθώσει ἐν δικαιοσύνῃ ὁ θρόνος αὐτοῦ
gr_gr_cog~PRO~C025~006~μὴ ἀλαζονεύου ἐνώπιον βασιλέως μηδὲ ἐν τόποις δυναστῶν ὑφίστασο
gr_gr_cog~PRO~C025~007~κρεῖσσον γάρ σοι τὸ ῥηθῆναι ἀνάβαινε πρός με ἢ ταπεινῶσαί σε ἐν προσώπῳ δυνάστου ἃ εἶδον οἱ ὀφθαλμοί σου λέγε
gr_gr_cog~PRO~C025~008~μὴ πρόσπιπτε εἰς μάχην ταχέως ἵνα μὴ μεταμεληθῇς ἐπ’ ἐσχάτων ἡνίκα ἄν σε ὀνειδίσῃ ὁ σὸς φίλος
gr_gr_cog~PRO~C025~009~ἀναχώρει εἰς τὰ ὀπίσω μὴ καταφρόνει
gr_gr_cog~PRO~C025~010~μή σε ὀνειδίσῃ μὲν ὁ φίλος ἡ δὲ μάχη σου καὶ ἡ ἔχθρα οὐκ ἀπέσται ἀλλ’ ἔσται σοι ἴση θανάτῳ
gr_gr_cog~PRO~C025~010a~χάρις καὶ φιλία ἐλευθεροῖ ἃς τήρησον σεαυτῷ ἵνα μὴ ἐπονείδιστος γένῃ ἀλλὰ φύλαξον τὰς ὁδούς σου εὐσυναλλάκτως
gr_gr_cog~PRO~C025~011~μῆλον χρυσοῦν ἐν ὁρμίσκῳ σαρδίου οὕτως εἰπεῖν λόγον
gr_gr_cog~PRO~C025~012~εἰς ἐνώτιον χρυσοῦν σάρδιον πολυτελὲς δέδεται λόγος σοφὸς εἰς εὐήκοον οὖς
gr_gr_cog~PRO~C025~013~ὥσπερ ἔξοδος χιόνος ἐν ἀμήτῳ κατὰ καῦμα ὠφελεῖ οὕτως ἄγγελος πιστὸς τοὺς ἀποστείλαντας αὐτόν ψυχὰς γὰρ τῶν αὐτῷ χρωμένων ὠφελεῖ
gr_gr_cog~PRO~C025~014~ὥσπερ ἄνεμοι καὶ νέφη καὶ ὑετοὶ ἐπιφανέστατοι οὕτως οἱ καυχώμενοι ἐπὶ δόσει ψευδεῖ
gr_gr_cog~PRO~C025~015~ἐν μακροθυμίᾳ εὐοδία βασιλεῦσιν γλῶσσα δὲ μαλακὴ συντρίβει ὀστᾶ
gr_gr_cog~PRO~C025~016~μέλι εὑρὼν φάγε τὸ ἱκανόν μήποτε πλησθεὶς ἐξεμέσῃς
gr_gr_cog~PRO~C025~017~σπάνιον εἴσαγε σὸν πόδα πρὸς τὸν σεαυτοῦ φίλον μήποτε πλησθείς σου μισήσῃ σε
gr_gr_cog~PRO~C025~018~ῥόπαλον καὶ μάχαιρα καὶ τόξευμα ἀκιδωτόν οὕτως καὶ ἀνὴρ ὁ καταμαρτυρῶν τοῦ φίλου αὐτοῦ μαρτυρίαν ψευδῆ
gr_gr_cog~PRO~C025~019~ὀδοὺς κακοῦ καὶ ποὺς παρανόμου ὀλεῖται ἐν ἡμέρᾳ κακῇ
gr_gr_cog~PRO~C025~020~ὥσπερ ὄξος ἕλκει ἀσύμφορον οὕτως προσπεσὸν πάθος ἐν σώματι καρδίαν λυπεῖ
gr_gr_cog~PRO~C025~020a~ὥσπερ σὴς ἱματίῳ καὶ σκώληξ ξύλῳ οὕτως λύπη ἀνδρὸς βλάπτει καρδίαν
gr_gr_cog~PRO~C025~021~ἐὰν πεινᾷ ὁ ἐχθρός σου τρέφε αὐτόν ἐὰν διψᾷ πότιζε αὐτόν
gr_gr_cog~PRO~C025~022~τοῦτο γὰρ ποιῶν ἄνθρακας πυρὸς σωρεύσεις ἐπὶ τὴν κεφαλὴν αὐτοῦ ὁ δὲ κύριος ἀνταποδώσει σοι ἀγαθά
gr_gr_cog~PRO~C025~023~ἄνεμος βορέας ἐξεγείρει νέφη πρόσωπον δὲ ἀναιδὲς γλῶσσαν ἐρεθίζει
gr_gr_cog~PRO~C025~024~κρεῖττον οἰκεῖν ἐπὶ γωνίας δώματος ἢ μετὰ γυναικὸς λοιδόρου ἐν οἰκίᾳ κοινῇ
gr_gr_cog~PRO~C025~025~ὥσπερ ὕδωρ ψυχρὸν ψυχῇ διψώσῃ προσηνές οὕτως ἀγγελία ἀγαθὴ ἐκ γῆς μακρόθεν
gr_gr_cog~PRO~C025~026~ὥσπερ εἴ τις πηγὴν φράσσοι καὶ ὕδατος ἔξοδον λυμαίνοιτο οὕτως ἄκοσμον δίκαιον πεπτωκέναι ἐνώπιον ἀσεβοῦς
gr_gr_cog~PRO~C025~027~ἐσθίειν μέλι πολὺ οὐ καλόν τιμᾶν δὲ χρὴ λόγους ἐνδόξους
gr_gr_cog~PRO~C025~028~ὥσπερ πόλις τὰ τείχη καταβεβλημένη καὶ ἀτείχιστος οὕτως ἀνὴρ ὃς οὐ μετὰ βουλῆς τι πράσσει
