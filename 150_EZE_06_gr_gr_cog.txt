gr_gr_cog~EZE~C006~001~καὶ ἐγένετο λόγος κυρίου πρός με λέγων
gr_gr_cog~EZE~C006~002~υἱὲ ἀνθρώπου στήρισον τὸ πρόσωπόν σου ἐπὶ τὰ ὄρη Ισραηλ καὶ προφήτευσον ἐπ’ αὐτὰ
gr_gr_cog~EZE~C006~003~καὶ ἐρεῖς τὰ ὄρη Ισραηλ ἀκούσατε λόγον κυρίου τάδε λέγει κύριος τοῖς ὄρεσιν καὶ τοῖς βουνοῖς καὶ ταῖς φάραγξιν καὶ ταῖς νάπαις ἰδοὺ ἐγὼ ἐπάγω ἐφ’ ὑμᾶς ῥομφαίαν καὶ ἐξολεθρευθήσεται τὰ ὑψηλὰ ὑμῶν
gr_gr_cog~EZE~C006~004~καὶ συντριβήσονται τὰ θυσιαστήρια ὑμῶν καὶ τὰ τεμένη ὑμῶν καὶ καταβαλῶ τραυματίας ὑμῶν ἐνώπιον τῶν εἰδώλων ὑμῶν
gr_gr_cog~EZE~C006~005~καὶ διασκορπιῶ τὰ ὀστᾶ ὑμῶν κύκλῳ τῶν θυσιαστηρίων ὑμῶν
gr_gr_cog~EZE~C006~006~ἐν πάσῃ τῇ κατοικίᾳ ὑμῶν αἱ πόλεις ἐξερημωθήσονται καὶ τὰ ὑψηλὰ ἀφανισθήσεται ὅπως ἐξολεθρευθῇ τὰ θυσιαστήρια ὑμῶν καὶ συντριβήσονται τὰ εἴδωλα ὑμῶν καὶ ἐξαρθήσεται τὰ τεμένη ὑμῶν
gr_gr_cog~EZE~C006~007~καὶ πεσοῦνται τραυματίαι ἐν μέσῳ ὑμῶν καὶ ἐπιγνώσεσθε ὅτι ἐγὼ κύριος
gr_gr_cog~EZE~C006~008~ἐν τῷ γενέσθαι ἐξ ὑμῶν ἀνασῳζομένους ἐκ ῥομφαίας ἐν τοῖς ἔθνεσιν καὶ ἐν τῷ διασκορπισμῷ ὑμῶν ἐν ταῖς χώραις
gr_gr_cog~EZE~C006~009~καὶ μνησθήσονταί μου οἱ ἀνασῳζόμενοι ἐξ ὑμῶν ἐν τοῖς ἔθνεσιν οὗ ᾐχμαλωτεύθησαν ἐκεῖ ὀμώμοκα τῇ καρδίᾳ αὐτῶν τῇ ἐκπορνευούσῃ ἀπ’ ἐμοῦ καὶ τοῖς ὀφθαλμοῖς αὐτῶν τοῖς πορνεύουσιν ὀπίσω τῶν ἐπιτηδευμάτων αὐτῶν καὶ κόψονται πρόσωπα αὐτῶν ἐν πᾶσι τοῖς βδελύγμασιν αὐτῶν
gr_gr_cog~EZE~C006~010~καὶ ἐπιγνώσονται διότι ἐγὼ κύριος λελάληκα
gr_gr_cog~EZE~C006~011~τάδε λέγει κύριος κρότησον τῇ χειρὶ καὶ ψόφησον τῷ ποδὶ καὶ εἰπόν εὖγε εὖγε ἐπὶ πᾶσιν τοῖς βδελύγμασιν οἴκου Ισραηλ ἐν ῥομφαίᾳ καὶ ἐν θανάτῳ καὶ ἐν λιμῷ πεσοῦνται
gr_gr_cog~EZE~C006~012~ὁ ἐγγὺς ἐν ῥομφαίᾳ πεσεῖται ὁ δὲ μακρὰν ἐν θανάτῳ τελευτήσει καὶ ὁ περιεχόμενος ἐν λιμῷ συντελεσθήσεται καὶ συντελέσω τὴν ὀργήν μου ἐπ’ αὐτούς
gr_gr_cog~EZE~C006~013~καὶ γνώσεσθε διότι ἐγὼ κύριος ἐν τῷ εἶναι τοὺς τραυματίας ὑμῶν ἐν μέσῳ τῶν εἰδώλων ὑμῶν κύκλῳ τῶν θυσιαστηρίων ὑμῶν ἐπὶ πάντα βουνὸν ὑψηλὸν καὶ ὑποκάτω δένδρου συσκίου οὗ ἔδωκαν ἐκεῖ ὀσμὴν εὐωδίας πᾶσι τοῖς εἰδώλοις αὐτῶν
gr_gr_cog~EZE~C006~014~καὶ ἐκτενῶ τὴν χεῖρά μου ἐπ’ αὐτοὺς καὶ θήσομαι τὴν γῆν εἰς ἀφανισμὸν καὶ εἰς ὄλεθρον ἀπὸ τῆς ἐρήμου Δεβλαθα ἐκ πάσης τῆς κατοικίας καὶ ἐπιγνώσεσθε ὅτι ἐγὼ κύριος
