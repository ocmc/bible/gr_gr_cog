gr_gr_cog~ISA~C013~001~ὅρασις ἣν εἶδεν Ησαιας υἱὸς Αμως κατὰ Βαβυλῶνος
gr_gr_cog~ISA~C013~002~ἐπ’ ὄρους πεδινοῦ ἄρατε σημεῖον ὑψώσατε τὴν φωνὴν αὐτοῖς μὴ φοβεῖσθε παρακαλεῖτε τῇ χειρί ἀνοίξατε οἱ ἄρχοντες
gr_gr_cog~ISA~C013~003~ἐγὼ συντάσσω καὶ ἐγὼ ἄγω αὐτούς ἡγιασμένοι εἰσίν καὶ ἐγὼ ἄγω αὐτούς γίγαντες ἔρχονται πληρῶσαι τὸν θυμόν μου χαίροντες ἅμα καὶ ὑβρίζοντες
gr_gr_cog~ISA~C013~004~φωνὴ ἐθνῶν πολλῶν ἐπὶ τῶν ὀρέων ὁμοία ἐθνῶν πολλῶν φωνὴ βασιλέων καὶ ἐθνῶν συνηγμένων κύριος σαβαωθ ἐντέταλται ἔθνει ὁπλομάχῳ
gr_gr_cog~ISA~C013~005~ἔρχεσθαι ἐκ γῆς πόρρωθεν ἀπ’ ἄκρου θεμελίου τοῦ οὐρανοῦ κύριος καὶ οἱ ὁπλομάχοι αὐτοῦ τοῦ καταφθεῖραι τὴν οἰκουμένην ὅλην
gr_gr_cog~ISA~C013~006~ὀλολύζετε ἐγγὺς γὰρ ἡ ἡμέρα κυρίου καὶ συντριβὴ παρὰ τοῦ θεοῦ ἥξει
gr_gr_cog~ISA~C013~007~διὰ τοῦτο πᾶσα χεὶρ ἐκλυθήσεται καὶ πᾶσα ψυχὴ ἀνθρώπου δειλιάσει
gr_gr_cog~ISA~C013~008~καὶ ταραχθήσονται οἱ πρέσβεις καὶ ὠδῖνες αὐτοὺς ἕξουσιν ὡς γυναικὸς τικτούσης καὶ συμφοράσουσιν ἕτερος πρὸς τὸν ἕτερον καὶ ἐκστήσονται καὶ τὸ πρόσωπον αὐτῶν ὡς φλὸξ μεταβαλοῦσιν
gr_gr_cog~ISA~C013~009~ἰδοὺ γὰρ ἡμέρα κυρίου ἀνίατος ἔρχεται θυμοῦ καὶ ὀργῆς θεῖναι τὴν οἰκουμένην ὅλην ἔρημον καὶ τοὺς ἁμαρτωλοὺς ἀπολέσαι ἐξ αὐτῆς
gr_gr_cog~ISA~C013~010~οἱ γὰρ ἀστέρες τοῦ οὐρανοῦ καὶ ὁ Ὠρίων καὶ πᾶς ὁ κόσμος τοῦ οὐρανοῦ τὸ φῶς οὐ δώσουσιν καὶ σκοτισθήσεται τοῦ ἡλίου ἀνατέλλοντος καὶ ἡ σελήνη οὐ δώσει τὸ φῶς αὐτῆς
gr_gr_cog~ISA~C013~011~καὶ ἐντελοῦμαι τῇ οἰκουμένῃ ὅλῃ κακὰ καὶ τοῖς ἀσεβέσιν τὰς ἁμαρτίας αὐτῶν καὶ ἀπολῶ ὕβριν ἀνόμων καὶ ὕβριν ὑπερηφάνων ταπεινώσω
gr_gr_cog~ISA~C013~012~καὶ ἔσονται οἱ καταλελειμμένοι ἔντιμοι μᾶλλον ἢ τὸ χρυσίον τὸ ἄπυρον καὶ ὁ ἄνθρωπος μᾶλλον ἔντιμος ἔσται ἢ ὁ λίθος ὁ ἐκ Σουφιρ
gr_gr_cog~ISA~C013~013~ὁ γὰρ οὐρανὸς θυμωθήσεται καὶ ἡ γῆ σεισθήσεται ἐκ τῶν θεμελίων αὐτῆς διὰ θυμὸν ὀργῆς κυρίου σαβαωθ τῇ ἡμέρᾳ ᾗ ἂν ἐπέλθῃ ὁ θυμὸς αὐτοῦ
gr_gr_cog~ISA~C013~014~καὶ ἔσονται οἱ καταλελειμμένοι ὡς δορκάδιον φεῦγον καὶ ὡς πρόβατον πλανώμενον καὶ οὐκ ἔσται ὁ συνάγων ὥστε ἄνθρωπον εἰς τὸν λαὸν αὐτοῦ ἀποστραφῆναι καὶ ἄνθρωπον εἰς τὴν χώραν αὐτοῦ διῶξαι
gr_gr_cog~ISA~C013~015~ὃς γὰρ ἂν ἁλῷ ἡττηθήσεται καὶ οἵτινες συνηγμένοι εἰσίν μαχαίρᾳ πεσοῦνται
gr_gr_cog~ISA~C013~016~καὶ τὰ τέκνα αὐτῶν ἐνώπιον αὐτῶν ῥάξουσιν καὶ τὰς οἰκίας αὐτῶν προνομεύσουσιν καὶ τὰς γυναῖκας αὐτῶν ἕξουσιν
gr_gr_cog~ISA~C013~017~ἰδοὺ ἐπεγείρω ὑμῖν τοὺς Μήδους οἳ οὐ λογίζονται ἀργύριον οὐδὲ χρυσίου χρείαν ἔχουσιν
gr_gr_cog~ISA~C013~018~τοξεύματα νεανίσκων συντρίψουσιν καὶ τὰ τέκνα ὑμῶν οὐ μὴ ἐλεήσωσιν οὐδὲ ἐπὶ τοῖς τέκνοις οὐ φείσονται οἱ ὀφθαλμοὶ αὐτῶν
gr_gr_cog~ISA~C013~019~καὶ ἔσται Βαβυλών ἣ καλεῖται ἔνδοξος ὑπὸ βασιλέως Χαλδαίων ὃν τρόπον κατέστρεψεν ὁ θεὸς Σοδομα καὶ Γομορρα
gr_gr_cog~ISA~C013~020~οὐ κατοικηθήσεται εἰς τὸν αἰῶνα χρόνον οὐδὲ μὴ εἰσέλθωσιν εἰς αὐτὴν διὰ πολλῶν γενεῶν οὐδὲ μὴ διέλθωσιν αὐτὴν Ἄραβες οὐδὲ ποιμένες οὐ μὴ ἀναπαύσωνται ἐν αὐτῇ
gr_gr_cog~ISA~C013~021~καὶ ἀναπαύσονται ἐκεῖ θηρία καὶ ἐμπλησθήσονται αἱ οἰκίαι ἤχου καὶ ἀναπαύσονται ἐκεῖ σειρῆνες καὶ δαιμόνια ἐκεῖ ὀρχήσονται
gr_gr_cog~ISA~C013~022~καὶ ὀνοκένταυροι ἐκεῖ κατοικήσουσιν καὶ νοσσοποιήσουσιν ἐχῖνοι ἐν τοῖς οἴκοις αὐτῶν ταχὺ ἔρχεται καὶ οὐ χρονιεῖ
