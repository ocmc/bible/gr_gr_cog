gr_gr_cog~KI2~C003~001~καὶ Ιωραμ υἱὸς Αχααβ ἐβασίλευσεν ἐν Ισραηλ ἐν ἔτει ὀκτωκαιδεκάτῳ Ιωσαφατ βασιλεῖ Ιουδα καὶ ἐβασίλευσεν δώδεκα ἔτη
gr_gr_cog~KI2~C003~002~καὶ ἐποίησεν τὸ πονηρὸν ἐν ὀφθαλμοῖς κυρίου πλὴν οὐχ ὡς ὁ πατὴρ αὐτοῦ καὶ οὐχ ὡς ἡ μήτηρ αὐτοῦ καὶ μετέστησεν τὰς στήλας τοῦ Βααλ ἃς ἐποίησεν ὁ πατὴρ αὐτοῦ
gr_gr_cog~KI2~C003~003~πλὴν ἐν τῇ ἁμαρτίᾳ Ιεροβοαμ υἱοῦ Ναβατ ὃς ἐξήμαρτεν τὸν Ισραηλ ἐκολλήθη οὐκ ἀπέστη ἀπ’ αὐτῆς
gr_gr_cog~KI2~C003~004~καὶ Μωσα βασιλεὺς Μωαβ ἦν νωκηδ καὶ ἐπέστρεφεν τῷ βασιλεῖ Ισραηλ ἐν τῇ ἐπαναστάσει ἑκατὸν χιλιάδας ἀρνῶν καὶ ἑκατὸν χιλιάδας κριῶν ἐπὶ πόκων
gr_gr_cog~KI2~C003~005~καὶ ἐγένετο μετὰ τὸ ἀποθανεῖν Αχααβ καὶ ἠθέτησεν βασιλεὺς Μωαβ ἐν βασιλεῖ Ισραηλ
gr_gr_cog~KI2~C003~006~καὶ ἐξῆλθεν ὁ βασιλεὺς Ιωραμ ἐν τῇ ἡμέρᾳ ἐκείνῃ ἐκ Σαμαρείας καὶ ἐπεσκέψατο τὸν Ισραηλ
gr_gr_cog~KI2~C003~007~καὶ ἐπορεύθη καὶ ἐξαπέστειλεν πρὸς Ιωσαφατ βασιλέα Ιουδα λέγων βασιλεὺς Μωαβ ἠθέτησεν ἐν ἐμοί εἰ πορεύσῃ μετ’ ἐμοῦ εἰς Μωαβ εἰς πόλεμον καὶ εἶπεν ἀναβήσομαι ὅμοιός μοι ὅμοιός σοι ὡς ὁ λαός μου ὁ λαός σου ὡς οἱ ἵπποι μου οἱ ἵπποι σου
gr_gr_cog~KI2~C003~008~καὶ εἶπεν ποίᾳ ὁδῷ ἀναβῶ καὶ εἶπεν ὁδὸν ἔρημον Εδωμ
gr_gr_cog~KI2~C003~009~καὶ ἐπορεύθη ὁ βασιλεὺς Ισραηλ καὶ ὁ βασιλεὺς Ιουδα καὶ ὁ βασιλεὺς Εδωμ καὶ ἐκύκλωσαν ὁδὸν ἑπτὰ ἡμερῶν καὶ οὐκ ἦν ὕδωρ τῇ παρεμβολῇ καὶ τοῖς κτήνεσιν τοῖς ἐν τοῖς ποσὶν αὐτῶν
gr_gr_cog~KI2~C003~010~καὶ εἶπεν ὁ βασιλεὺς Ισραηλ ὦ ὅτι κέκληκεν κύριος τοὺς τρεῖς βασιλεῖς παρερχομένους δοῦναι αὐτοὺς ἐν χειρὶ Μωαβ
gr_gr_cog~KI2~C003~011~καὶ εἶπεν Ιωσαφατ οὐκ ἔστιν ὧδε προφήτης τοῦ κυρίου καὶ ἐπιζητήσωμεν τὸν κύριον παρ’ αὐτοῦ καὶ ἀπεκρίθη εἷς τῶν παίδων βασιλέως Ισραηλ καὶ εἶπεν ὧδε Ελισαιε υἱὸς Σαφατ ὃς ἐπέχεεν ὕδωρ ἐπὶ χεῖρας Ηλιου
gr_gr_cog~KI2~C003~012~καὶ εἶπεν Ιωσαφατ ἔστιν αὐτῷ ῥῆμα κυρίου καὶ κατέβη πρὸς αὐτὸν βασιλεὺς Ισραηλ καὶ Ιωσαφατ βασιλεὺς Ιουδα καὶ βασιλεὺς Εδωμ
gr_gr_cog~KI2~C003~013~καὶ εἶπεν Ελισαιε πρὸς βασιλέα Ισραηλ τί ἐμοὶ καὶ σοί δεῦρο πρὸς τοὺς προφήτας τοῦ πατρός σου καὶ εἶπεν αὐτῷ ὁ βασιλεὺς Ισραηλ μή ὅτι κέκληκεν κύριος τοὺς τρεῖς βασιλεῖς τοῦ παραδοῦναι αὐτοὺς εἰς χεῖρας Μωαβ
gr_gr_cog~KI2~C003~014~καὶ εἶπεν Ελισαιε ζῇ κύριος τῶν δυνάμεων ᾧ παρέστην ἐνώπιον αὐτοῦ ὅτι εἰ μὴ πρόσωπον Ιωσαφατ βασιλέως Ιουδα ἐγὼ λαμβάνω εἰ ἐπέβλεψα πρὸς σὲ καὶ εἶδόν σε
gr_gr_cog~KI2~C003~015~καὶ νυνὶ δὲ λαβέ μοι ψάλλοντα καὶ ἐγένετο ὡς ἔψαλλεν ὁ ψάλλων καὶ ἐγένετο ἐπ’ αὐτὸν χεὶρ κυρίου
gr_gr_cog~KI2~C003~016~καὶ εἶπεν τάδε λέγει κύριος ποιήσατε τὸν χειμάρρουν τοῦτον βοθύνους βοθύνους
gr_gr_cog~KI2~C003~017~ὅτι τάδε λέγει κύριος οὐκ ὄψεσθε πνεῦμα καὶ οὐκ ὄψεσθε ὑετόν καὶ ὁ χειμάρρους οὗτος πλησθήσεται ὕδατος καὶ πίεσθε ὑμεῖς καὶ αἱ κτήσεις ὑμῶν καὶ τὰ κτήνη ὑμῶν
gr_gr_cog~KI2~C003~018~καὶ κούφη αὕτη ἐν ὀφθαλμοῖς κυρίου καὶ παραδώσω τὴν Μωαβ ἐν χειρὶ ὑμῶν
gr_gr_cog~KI2~C003~019~καὶ πατάξετε πᾶσαν πόλιν ὀχυρὰν καὶ πᾶν ξύλον ἀγαθὸν καταβαλεῖτε καὶ πάσας πηγὰς ὕδατος ἐμφράξετε καὶ πᾶσαν μερίδα ἀγαθὴν ἀχρειώσετε ἐν λίθοις
gr_gr_cog~KI2~C003~020~καὶ ἐγένετο τὸ πρωὶ ἀναβαινούσης τῆς θυσίας καὶ ἰδοὺ ὕδατα ἤρχοντο ἐξ ὁδοῦ Εδωμ καὶ ἐπλήσθη ἡ γῆ ὕδατος
gr_gr_cog~KI2~C003~021~καὶ πᾶσα Μωαβ ἤκουσαν ὅτι ἀνέβησαν οἱ βασιλεῖς πολεμεῖν αὐτούς καὶ ἀνεβόησαν ἐκ παντὸς περιεζωσμένου ζώνην καὶ ἐπάνω καὶ ἔστησαν ἐπὶ τοῦ ὁρίου
gr_gr_cog~KI2~C003~022~καὶ ὤρθρισαν τὸ πρωί καὶ ὁ ἥλιος ἀνέτειλεν ἐπὶ τὰ ὕδατα καὶ εἶδεν Μωαβ ἐξ ἐναντίας τὰ ὕδατα πυρρὰ ὡσεὶ αἷμα
gr_gr_cog~KI2~C003~023~καὶ εἶπαν αἷμα τοῦτο τῆς ῥομφαίας ἐμαχέσαντο οἱ βασιλεῖς καὶ ἐπάταξαν ἀνὴρ τὸν πλησίον αὐτοῦ καὶ νῦν ἐπὶ τὰ σκῦλα Μωαβ
gr_gr_cog~KI2~C003~024~καὶ εἰσῆλθον εἰς τὴν παρεμβολὴν Ισραηλ καὶ Ισραηλ ἀνέστησαν καὶ ἐπάταξαν τὴν Μωαβ καὶ ἔφυγον ἀπὸ προσώπου αὐτῶν καὶ εἰσῆλθον εἰσπορευόμενοι καὶ τύπτοντες τὴν Μωαβ
gr_gr_cog~KI2~C003~025~καὶ τὰς πόλεις καθεῖλον καὶ πᾶσαν μερίδα ἀγαθὴν ἔρριψαν ἀνὴρ τὸν λίθον καὶ ἐνέπλησαν αὐτὴν καὶ πᾶσαν πηγὴν ὕδατος ἐνέφραξαν καὶ πᾶν ξύλον ἀγαθὸν κατέβαλον ἕως τοῦ καταλιπεῖν τοὺς λίθους τοῦ τοίχου καθῃρημένους καὶ ἐκύκλευσαν οἱ σφενδονῆται καὶ ἐπάταξαν αὐτήν
gr_gr_cog~KI2~C003~026~καὶ εἶδεν ὁ βασιλεὺς Μωαβ ὅτι ἐκραταίωσεν ὑπὲρ αὐτὸν ὁ πόλεμος καὶ ἔλαβεν μεθ’ ἑαυτοῦ ἑπτακοσίους ἄνδρας ἐσπασμένους ῥομφαίαν διακόψαι πρὸς βασιλέα Εδωμ καὶ οὐκ ἠδυνήθησαν
gr_gr_cog~KI2~C003~027~καὶ ἔλαβεν τὸν υἱὸν αὐτοῦ τὸν πρωτότοκον ὃς ἐβασίλευσεν ἀντ’ αὐτοῦ καὶ ἀνήνεγκεν αὐτὸν ὁλοκαύτωμα ἐπὶ τοῦ τείχους καὶ ἐγένετο μετάμελος μέγας ἐπὶ Ισραηλ καὶ ἀπῆραν ἀπ’ αὐτοῦ καὶ ἐπέστρεψαν εἰς τὴν γῆν
