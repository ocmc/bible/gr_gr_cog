gr_gr_cog~POS~C005~001~κύριε ὁ θεός αἰνέσω τῷ ὀνόματί σου ἐν ἀγαλλιάσει ἐν μέσῳ ἐπισταμένων τὰ κρίματά σου τὰ δίκαια
gr_gr_cog~POS~C005~002~ὅτι σὺ χρηστὸς καὶ ἐλεήμων ἡ καταφυγὴ τοῦ πτωχοῦ ἐν τῷ κεκραγέναι με πρὸς σὲ μὴ παρασιωπήσῃς ἀπ’ ἐμοῦ
gr_gr_cog~POS~C005~003~οὐ γὰρ λήψεταί τις σκῦλα παρὰ ἀνδρὸς δυνατοῦ καὶ τίς λήψεται ἀπὸ πάντων ὧν ἐποίησας ἐὰν μὴ σὺ δῷς
gr_gr_cog~POS~C005~004~ὅτι ἄνθρωπος καὶ ἡ μερὶς αὐτοῦ παρὰ σοῦ ἐν σταθμῷ οὐ προσθήσει τοῦ πλεονάσαι παρὰ τὸ κρίμα σου ὁ θεός
gr_gr_cog~POS~C005~005~ἐν τῷ θλίβεσθαι ἡμᾶς ἐπικαλεσόμεθά σε εἰς βοήθειαν καὶ σὺ οὐκ ἀποστρέψῃ τὴν δέησιν ἡμῶν ὅτι σὺ ὁ θεὸς ἡμῶν εἶ
gr_gr_cog~POS~C005~006~μὴ βαρύνῃς τὴν χεῖρά σου ἐφ’ ἡμᾶς ἵνα μὴ δι’ ἀνάγκην ἁμάρτωμεν
gr_gr_cog~POS~C005~007~καὶ ἐὰν μὴ ἐπιστρέψῃς ἡμᾶς οὐκ ἀφεξόμεθα ἀλλ’ ἐπὶ σὲ ἥξομεν
gr_gr_cog~POS~C005~008~ἐὰν γὰρ πεινάσω πρὸς σὲ κεκράξομαι ὁ θεός καὶ σὺ δώσεις μοι
gr_gr_cog~POS~C005~009~τὰ πετεινὰ καὶ τοὺς ἰχθύας σὺ τρέφεις ἐν τῷ διδόναι σε ὑετὸν ἐρήμοις εἰς ἀνατολὴν χλόης
gr_gr_cog~POS~C005~010~ἡτοίμασας χορτάσματα ἐν ἐρήμῳ παντὶ ζῶντι καὶ ἐὰν πεινάσωσιν πρὸς σὲ ἀροῦσιν πρόσωπον αὐτῶν
gr_gr_cog~POS~C005~011~τοὺς βασιλεῖς καὶ ἄρχοντας καὶ λαοὺς σὺ τρέφεις ὁ θεός καὶ πτωχοῦ καὶ πένητος ἡ ἐλπὶς τίς ἐστιν εἰ μὴ σύ κύριε
gr_gr_cog~POS~C005~012~καὶ σὺ ἐπακούσῃ ὅτι τίς χρηστὸς καὶ ἐπιεικὴς ἀλλ’ ἢ σὺ εὐφρᾶναι ψυχὴν ταπεινοῦ ἐν τῷ ἀνοῖξαι χεῖρά σου ἐν ἐλέει
gr_gr_cog~POS~C005~013~ἡ χρηστότης ἀνθρώπου ἐν φειδοῖ καὶ ἡ αὔριον καὶ ἐὰν δευτερώσῃ ἄνευ γογγυσμοῦ καὶ τοῦτο θαυμάσειας
gr_gr_cog~POS~C005~014~τὸ δὲ δόμα σου πολὺ μετὰ χρηστότητος καὶ πλούσιον καὶ οὗ ἐστιν ἡ ἐλπὶς ἐπὶ σέ οὐ φείσεται ἐν δόματι
gr_gr_cog~POS~C005~015~ἐπὶ πᾶσαν τὴν γῆν τὸ ἔλεός σου κύριε ἐν χρηστότητι
gr_gr_cog~POS~C005~016~μακάριος οὗ μνημονεύει ὁ θεὸς ἐν συμμετρίᾳ αὐταρκείας ἐὰν ὑπερπλεονάσῃ ὁ ἄνθρωπος ἐξαμαρτάνει
gr_gr_cog~POS~C005~017~ἱκανὸν τὸ μέτριον ἐν δικαιοσύνῃ καὶ ἐν τούτῳ ἡ εὐλογία κυρίου εἰς πλησμονὴν ἐν δικαιοσύνῃ
gr_gr_cog~POS~C005~018~εὐφρανθείησαν οἱ φοβούμενοι κύριον ἐν ἀγαθοῖς καὶ ἡ χρηστότης σου ἐπὶ Ισραηλ ἐν τῇ βασιλείᾳ σου
gr_gr_cog~POS~C005~019~εὐλογημένη ἡ δόξα κυρίου ὅτι αὐτὸς βασιλεὺς ἡμῶν
