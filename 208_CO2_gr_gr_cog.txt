gr_gr_cog~CO2~C001~001~Παῦλος, ἀπόστολος ᾿Ιησοῦ Χριστοῦ διὰ θελήματος Θεοῦ, καὶ Τιμόθεος ὁ ἀδελφός, τῇ ἐκκλησίᾳ τοῦ Θεοῦ τῇ οὔσῃ ἐν Κορίνθῳ σὺν τοῖς ἁγίοις πᾶσι τοῖς οὖσιν ἐν ὅλῃ τῇ ᾿Αχαΐᾳ·
gr_gr_cog~CO2~C001~002~χάρις ὑμῖν καὶ εἰρήνη ἀπὸ Θεοῦ πατρὸς ἡμῶν καὶ Κυρίου ᾿Ιησοῦ Χριστοῦ.
gr_gr_cog~CO2~C001~003~Εὐλογητὸς ὁ Θεὸς καὶ πατὴρ τοῦ Κυρίου ἡμῶν ᾿Ιησοῦ Χριστοῦ, ὁ πατὴρ τῶν οἰκτιρμῶν καὶ Θεὸς πάσης παρακλήσεως,
gr_gr_cog~CO2~C001~004~ὁ παρακαλῶν ἡμᾶς ἐν πάσῃ τῇ θλίψει ἡμῶν, εἰς τὸ δύνασθαι ἡμᾶς παρακαλεῖν τοὺς ἐν πάσῃ θλίψει διὰ τῆς παρακλήσεως ἧς παρακαλούμεθα αὐτοὶ ὑπὸ τοῦ Θεοῦ·
gr_gr_cog~CO2~C001~005~ὅτι καθὼς περισσεύει τὰ παθήματα τοῦ Χριστοῦ εἰς ἡμᾶς, οὕτω διὰ Χριστοῦ περισσεύει καὶ ἡ παράκλησις ἡμῶν.
gr_gr_cog~CO2~C001~006~εἴτε δὲ θλιβόμεθα, ὑπὲρ τῆς ὑμῶν παρακλήσεως καὶ σωτηρίας τῆς ἐνεργουμένης ἐν ὑπομονῇ τῶν αὐτῶν παθημάτων ὧν καὶ ἡμεῖς πάσχομεν, καὶ ἡ ἐλπὶς ἡμῶν βεβαία ὑπὲρ ὑμῶν·
gr_gr_cog~CO2~C001~007~εἴτε παρακαλούμεθα, ὑπὲρ τῆς ὑμῶν παρακλήσεως καὶ σωτηρίας, εἰδότες ὅτι ὥσπερ κοινωνοί ἐστε τῶν παθημάτων, οὕτω καὶ τῆς παρακλήσεως.
gr_gr_cog~CO2~C001~008~Οὐ γὰρ θέλομεν ὑμᾶς ἀγνοεῖν, ἀδελφοί, ὑπὲρ τῆς θλίψεως ἡμῶν τῆς γενομένης ἡμῖν ἐν τῇ ᾿Ασίᾳ, ὅτι καθ᾿ ὑπερβολὴν ἐβαρήθημεν ὑπὲρ δύναμιν, ὥστε ἐξαπορηθῆναι ἡμᾶς καὶ τοῦ ζῆν·
gr_gr_cog~CO2~C001~009~ἀλλὰ αὐτοὶ ἐν ἑαυτοῖς τὸ ἀπόκριμα τοῦ θανάτου ἐσχήκαμεν, ἵνα μὴ πεποιθότες ὦμεν ἐφ᾿ ἑαυτοῖς, ἀλλ᾿ ἐπὶ τῷ Θεῷ τῷ ἐγείροντι τοὺς νεκρούς·
gr_gr_cog~CO2~C001~010~ὃς ἐκ τηλικούτου θανάτου ἐρρύσατο ἡμᾶς καὶ ῥύεται, εἰς ὃν ἠλπίκαμεν ὅτι καὶ ἔτι ῥύσεται,
gr_gr_cog~CO2~C001~011~συνυπουργούντων καὶ ὑμῶν ὑπὲρ ἡμῶν τῇ δεήσει, ἵνα ἐκ πολλῶν προσώπων τὸ εἰς ἡμᾶς χάρισμα διὰ πολλῶν εὐχαριστηθῇ ὑπὲρ ἡμῶν.
gr_gr_cog~CO2~C001~012~῾Η γὰρ καύχησις ἡμῶν αὕτη ἐστί, τὸ μαρτύριον τῆς συνειδήσεως ἡμῶν, ὅτι ἐν ἁπλότητι καὶ εἰλικρινείᾳ Θεοῦ, οὐκ ἐν σοφίᾳ σαρκικῇ, ἀλλ᾿ ἐν χάριτι Θεοῦ ἀνεστράφημεν ἐν τῷ κόσμῳ, περισσοτέρως δὲ πρὸς ὑμᾶς.
gr_gr_cog~CO2~C001~013~οὐ γὰρ ἄλλα γράφομεν ὑμῖν, ἀλλ᾿ ἢ ἃ ἀναγινώσκετε ἢ καὶ ἐπιγινώσκετε, ἐλπίζω δὲ ὅτι καὶ ἕως τέλους ἐπιγνώσεσθε,
gr_gr_cog~CO2~C001~014~καθὼς καὶ ἐπέγνωτε ἡμᾶς ἀπὸ μέρους, ὅτι καύχημα ὑμῶν ἐσμεν, καθάπερ καὶ ὑμεῖς ἡμῶν, ἐν τῇ ἡμέρᾳ τοῦ Κυρίου ᾿Ιησοῦ.
gr_gr_cog~CO2~C001~015~Καὶ ταύτῃ τῇ πεποιθήσει ἐβουλόμην πρὸς ὑμᾶς ἐλθεῖν πρότερον, ἵνα δευτέραν χάριν ἔχητε,
gr_gr_cog~CO2~C001~016~καὶ δι᾿ ὑμῶν διελθεῖν εἰς Μακεδονίαν, καὶ πάλιν ἀπὸ Μακεδονίας ἐλθεῖν πρὸς ὑμᾶς καὶ ὑφ᾿ ὑμῶν προπεμφθῆναι εἰς τὴν ᾿Ιουδαίαν.
gr_gr_cog~CO2~C001~017~τοῦτο οὖν βουλόμενος μήτι ἄρα τῇ ἐλαφρίᾳ ἐχρησάμην; ἢ ἃ βουλεύομαι, κατὰ σάρκα βουλεύομαι, ἵνα ᾖ παρ᾿ ἐμοὶ τὸ ναὶ ναὶ καὶ τὸ οὒ οὔ;
gr_gr_cog~CO2~C001~018~πιστὸς δὲ ὁ Θεὸς ὅτι ὁ λόγος ἡμῶν ὁ πρὸς ὑμᾶς οὐκ ἐγένετο ναὶ καὶ οὔ.
gr_gr_cog~CO2~C001~019~ὁ γὰρ τοῦ Θεοῦ υἱὸς ᾿Ιησοῦς Χριστὸς ὁ ἐν ὑμῖν δι᾿ ἡμῶν κηρυχθείς, δι᾿ ἐμοῦ καὶ Σιλουανοῦ καὶ Τιμοθέου, οὐκ ἐγένετο ναὶ καὶ οὔ, ἀλλὰ ναὶ ἐν αὐτῷ γέγονεν·
gr_gr_cog~CO2~C001~020~ὅσαι γὰρ ἐπαγγελίαι Θεοῦ, ἐν αὐτῷ τὸ ναὶ καὶ ἐν αὐτῷ τὸ ἀμήν, τῷ Θεῷ πρὸς δόξαν δι᾿ ἡμῶν.
gr_gr_cog~CO2~C001~021~ὁ δὲ βεβαιῶν ἡμᾶς σὺν ὑμῖν εἰς Χριστὸν καὶ χρίσας ἡμᾶς Θεός,
gr_gr_cog~CO2~C001~022~ὁ καὶ σφραγισάμενος ἡμᾶς καὶ δοὺς τὸν ἀρραβῶνα τοῦ Πνεύματος ἐν ταῖς καρδίαις ἡμῶν.
gr_gr_cog~CO2~C001~023~᾿Εγὼ δὲ μάρτυρα τὸν Θεὸν ἐπικαλοῦμαι ἐπὶ τὴν ἐμὴν ψυχήν, ὅτι φειδόμενος ὑμῶν οὐκέτι ἦλθον εἰς Κόρινθον.
gr_gr_cog~CO2~C001~024~οὐχ ὅτι κυριεύομεν ὑμῶν τῆς πίστεως, ἀλλὰ συνεργοί ἐσμεν τῆς χαρᾶς ὑμῶν· τῇ γὰρ πίστει ἑστήκατε.
gr_gr_cog~CO2~C002~001~Ἔκρινα δὲ ἐμαυτῷ τοῦτο, τὸ μὴ πάλιν ἐν λύπῃ ἐλθεῖν πρὸς ὑμᾶς.
gr_gr_cog~CO2~C002~002~εἰ γὰρ ἐγὼ λυπῶ ὑμᾶς, καὶ τίς ἐστιν ὁ εὐφραίνων με εἰ μὴ ὁ λυπούμενος ἐξ ἐμοῦ;
gr_gr_cog~CO2~C002~003~καὶ ἔγραψα ὑμῖν τοῦτο αὐτό, ἵνα μὴ ἐλθὼν λύπην ἔχω ἀφ᾿ ὧν ἔδει με χαίρειν, πεποιθὼς ἐπὶ πάντας ὑμᾶς ὅτι ἡ ἐμὴ χαρὰ πάντων ὑμῶν ἐστιν.
gr_gr_cog~CO2~C002~004~ἐκ γὰρ πολλῆς θλίψεως καὶ συνοχῆς καρδίας ἔγραψα ὑμῖν διὰ πολλῶν δακρύων, οὐχ ἵνα λυπηθῆτε, ἀλλὰ τὴν ἀγάπην ἵνα γνῶτε ἣν ἔχω περισσοτέρως εἰς ὑμᾶς.
gr_gr_cog~CO2~C002~005~Εἰ δέ τις λελύπηκεν, οὐκ ἐμὲ λελύπηκεν, ἀλλά, ἀπὸ μέρους ἵνα μὴ ἐπιβαρῶ, πάντας ὑμᾶς.
gr_gr_cog~CO2~C002~006~ἱκανὸν τῷ τοιούτῳ ἡ ἐπιτιμία αὕτη ἡ ὑπὸ τῶν πλειόνων·
gr_gr_cog~CO2~C002~007~ὥστε τοὐναντίον μᾶλλον ὑμᾶς χαρίσασθαι καὶ παρακαλέσαι, μήπως τῇ περισσοτέρᾳ λύπῃ καταποθῇ ὁ τοιοῦτος.
gr_gr_cog~CO2~C002~008~διὸ παρακαλῶ ὑμᾶς κυρῶσαι εἰς αὐτὸν ἀγάπην.
gr_gr_cog~CO2~C002~009~εἰς τοῦτο γὰρ καὶ ἔγραψα, ἵνα γνῶ τὴν δοκιμὴν ὑμῶν, εἰ εἰς πάντα ὑπήκοοί ἐστε.
gr_gr_cog~CO2~C002~010~ᾧ δέ τι χαρίζεσθε, καὶ ἐγώ· καὶ γὰρ ἐγὼ εἴ τι κεχάρισμαι ᾧ κεχάρισμαι, δι᾿ ὑμᾶς ἐν προσώπῳ Χριστοῦ,
gr_gr_cog~CO2~C002~011~ἵνα μὴ πλεονεκτηθῶμεν ὑπὸ τοῦ σατανᾶ· οὐ γὰρ αὐτοῦ τὰ νοήματα ἀγνοοῦμεν.
gr_gr_cog~CO2~C002~012~᾿Ελθὼν δὲ εἰς τὴν Τρῳάδα εἰς τὸ εὐαγγέλιον τοῦ Χριστοῦ, καὶ θύρας μοι ἀνεῳγμένης ἐν Κυρίῳ,
gr_gr_cog~CO2~C002~013~οὐκ ἔσχηκα ἄνεσιν τῷ πνεύματί μου τῷ μὴ εὑρεῖν με Τίτον τὸν ἀδελφόν μου, ἀλλὰ ἀποταξάμενος αὐτοῖς ἐξῆλθον εἰς Μακεδονίαν.
gr_gr_cog~CO2~C002~014~Τῷ δὲ Θεῷ χάρις τῷ πάντοτε θριαμβεύοντι ἡμᾶς ἐν τῷ Χριστῷ καὶ τὴν ὀσμὴν τῆς γνώσεως αὐτοῦ φανεροῦντι δι᾿ ἡμῶν ἐν παντὶ τόπῳ·
gr_gr_cog~CO2~C002~015~ὅτι Χριστοῦ εὐωδία ἐσμὲν τῷ Θεῷ ἐν τοῖς σῳζομένοις καὶ ἐν τοῖς ἀπολλυμένοις,
gr_gr_cog~CO2~C002~016~οἷς μὲν ὀσμὴ θανάτου εἰς θάνατον, οἷς δὲ ὀσμὴ ζωῆς εἰς ζωήν. καὶ πρὸς ταῦτα τίς ἱκανός;
gr_gr_cog~CO2~C002~017~οὐ γάρ ἐσμεν ὡς οἱ λοιποὶ καπηλεύοντες τὸν λόγον τοῦ Θεοῦ, ἀλλ᾿ ὡς ἐξ εἰλικρινείας, ἀλλ᾿ ὡς ἐκ Θεοῦ κατενώπιον τοῦ Θεοῦ ἐν Χριστῷ λαλοῦμεν.
gr_gr_cog~CO2~C003~001~Ἀρχόμεθα πάλιν ἑαυτοὺς συνιστάνειν; εἰ μὴ χρῄζομεν ὥς τινες συστατικῶν ἐπιστολῶν πρὸς ὑμᾶς ἢ ἐξ ὑμῶν συστατικῶν;
gr_gr_cog~CO2~C003~002~ἡ ἐπιστολὴ ἡμῶν ὑμεῖς ἐστε, ἐγγεγραμμένη ἐν ταῖς καρδίαις ἡμῶν, γινωσκομένη καὶ ἀναγινωσκομένη ὑπὸ πάντων ἀνθρώπων,
gr_gr_cog~CO2~C003~003~φανερούμενοι ὅτι ἐστὲ ἐπιστολὴ Χριστοῦ διακονηθεῖσα ὑφ᾿ ἡμῶν, ἐγγεγραμμένη οὐ μέλανι, ἀλλὰ Πνεύματι Θεοῦ ζῶντος, οὐκ ἐν πλαξὶ λιθίναις, ἀλλὰ ἐν πλαξὶ καρδίαις σαρκίναις.
gr_gr_cog~CO2~C003~004~Πεποίθησιν δὲ τοιαύτην ἔχομεν διὰ τοῦ Χριστοῦ πρὸς τὸν Θεόν.
gr_gr_cog~CO2~C003~005~οὐχ ὅτι ἱκανοί ἐσμεν ἀφ᾿ ἑαυτῶν λογίσασθαί τι ὡς ἐξ ἑαυτῶν, ἀλλ᾿ ἡ ἱκανότης ἡμῶν ἐκ τοῦ Θεοῦ,
gr_gr_cog~CO2~C003~006~ὃς καὶ ἱκάνωσεν ἡμᾶς διακόνους καινῆς διαθήκης, οὐ γράμματος, ἀλλὰ πνεύματος· τὸ γὰρ γράμμα ἀποκτέννει, τὸ δὲ πνεῦμα ζωοποιεῖ.
gr_gr_cog~CO2~C003~007~Εἰ δὲ ἡ διακονία τοῦ θανάτου ἐν γράμμασιν ἐντετυπωμένη ἐν λίθοις ἐγενήθη ἐν δόξῃ, ὥστε μὴ δύνασθαι ἀτενίσαι τοὺς υἱοὺς ᾿Ισραὴλ εἰς τὸ πρόσωπον Μωϋσέως διὰ τὴν δόξαν τοῦ προσώπου αὐτοῦ τὴν καταργουμένην,
gr_gr_cog~CO2~C003~008~πῶς οὐχὶ μᾶλλον ἡ διακονία τοῦ πνεύματος ἔσται ἐν δόξῃ;
gr_gr_cog~CO2~C003~009~εἰ γὰρ ἡ διακονία τῆς κατακρίσεως δόξα, πολλῷ μᾶλλον περισσεύει ἡ διακονία τῆς δικαιοσύνης ἐν δόξῃ.
gr_gr_cog~CO2~C003~010~καὶ γὰρ οὐδὲ δεδόξασται τὸ δεδοξασμένον ἐν τούτῳ τῷ μέρει ἕνεκεν τῆς ὑπερβαλλούσης δόξης.
gr_gr_cog~CO2~C003~011~εἰ γὰρ τὸ καταργούμενον διὰ δόξης, πολλῷ μᾶλλον τὸ μένον ἐν δόξῃ.
gr_gr_cog~CO2~C003~012~῎Εχοντες οὖν τοιαύτην ἐλπίδα πολλῇ παρρησίᾳ χρώμεθα,
gr_gr_cog~CO2~C003~013~καὶ οὐ καθάπερ Μωϋσῆς ἐτίθει κάλυμμα ἐπὶ τὸ πρόσωπον ἑαυτοῦ πρὸς τὸ μὴ ἀτενίσαι τοὺς υἱοὺς ᾿Ισραὴλ εἰς τὸ τέλος τοῦ καταργουμένου.
gr_gr_cog~CO2~C003~014~ἀλλ᾿ ἐπωρώθη τὰ νοήματα αὐτῶν. ἄχρι γὰρ τῆς σήμερον τὸ αὐτὸ κάλυμμα ἐπὶ τῇ ἀναγνώσει τῆς παλαιᾶς διαθήκης μένει, μὴ ἀνακαλυπτόμενον ὅτι ἐν Χριστῷ καταργεῖται,
gr_gr_cog~CO2~C003~015~ἀλλ᾿ ἕως σήμερον, ἡνίκα ἀναγινώσκεται Μωϋσῆς, κάλυμμα ἐπὶ τὴν καρδίαν αὐτῶν κεῖται·
gr_gr_cog~CO2~C003~016~ἡνίκα δ᾿ ἂν ἐπιστρέψῃ πρὸς Κύριον, περιαιρεῖται τὸ κάλυμμα.
gr_gr_cog~CO2~C003~017~ὁ δὲ Κύριος τὸ Πνεῦμά ἐστιν· οὗ δὲ τὸ Πνεῦμα Κυρίου, ἐκεῖ ἐλευθερία.
gr_gr_cog~CO2~C003~018~ἡμεῖς δὲ πάντες ἀνακεκαλυμμένῳ προσώπῳ τὴν δόξαν Κυρίου κατοπτριζόμενοι τὴν αὐτὴν εἰκόνα μεταμορφούμεθα ἀπὸ δόξης εἰς δόξαν, καθάπερ ἀπὸ Κυρίου Πνεύματος.
gr_gr_cog~CO2~C004~001~Διὰ τοῦτο, ἔχοντες τὴν διακονίαν ταύτην καθὼς ἠλεήθημεν, οὐκ ἐκκακοῦμεν,
gr_gr_cog~CO2~C004~002~ἀλλ᾿ ἀπειπάμεθα τὰ κρυπτὰ τῆς αἰσχύνης, μὴ περιπατοῦντες ἐν πανουργίᾳ μηδὲ δολοῦντες τὸν λόγον τοῦ Θεοῦ, ἀλλὰ τῇ φανερώσει τῆς ἀληθείας συνιστῶντες ἑαυτοὺς πρὸς πᾶσαν συνείδησιν ἀνθρώπων ἐνώπιον τοῦ Θεοῦ.
gr_gr_cog~CO2~C004~003~εἰ δὲ καὶ ἔστι κεκαλυμμένον τὸ εὐαγγέλιον ἡμῶν, ἐν τοῖς ἀπολλυμένοις ἐστὶ κεκαλυμμένον,
gr_gr_cog~CO2~C004~004~ἐν οἷς ὁ θεὸς τοῦ αἰῶνος τούτου ἐτύφλωσε τὰ νοήματα τῶν ἀπίστων εἰς τὸ μὴ αὐγάσαι αὐτοῖς τὸν φωτισμὸν τοῦ εὐαγγελίου τῆς δόξης τοῦ Χριστοῦ, ὅς ἐστιν εἰκὼν τοῦ Θεοῦ.
gr_gr_cog~CO2~C004~005~οὐ γὰρ ἑαυτοὺς κηρύσσομεν, ἀλλὰ Χριστὸν ᾿Ιησοῦν Κύριον, ἑαυτοὺς δὲ δούλους ὑμῶν διὰ ᾿Ιησοῦν.
gr_gr_cog~CO2~C004~006~ὅτι ὁ Θεὸς ὁ εἰπὼν ἐκ σκότους φῶς λάμψαι, ὃς ἔλαμψεν ἐν ταῖς καρδίαις ἡμῶν πρὸς φωτισμὸν τῆς γνώσεως τῆς δόξης τοῦ Θεοῦ ἐν προσώπῳ ᾿Ιησοῦ Χριστοῦ.
gr_gr_cog~CO2~C004~007~῎Εχομεν δὲ τὸν θησαυρὸν τοῦτον ἐν ὀστρακίνοις σκεύεσιν, ἵνα ἡ ὑπερβολὴ τῆς δυνάμεως ᾖ τοῦ Θεοῦ καὶ μὴ ἐξ ἡμῶν,
gr_gr_cog~CO2~C004~008~ἐν παντὶ θλιβόμενοι ἀλλ᾿ οὐ στενοχωρούμενοι, ἀπορούμενοι ἀλλ᾿ οὐκ ἐξαπορούμενοι,
gr_gr_cog~CO2~C004~009~διωκόμενοι ἀλλ᾿ οὐκ ἐγκαταλειπόμενοι, καταβαλλόμενοι ἀλλ᾿ οὐκ ἀπολλύμενοι,
gr_gr_cog~CO2~C004~010~πάντοτε τὴν νέκρωσιν τοῦ Κυρίου ᾿Ιησοῦ ἐν τῷ σώματι περιφέροντες, ἵνα καὶ ἡ ζωὴ τοῦ ᾿Ιησοῦ ἐν τῷ σώματι ἡμῶν φανερωθῇ.
gr_gr_cog~CO2~C004~011~ἀεὶ γὰρ ἡμεῖς οἱ ζῶντες εἰς θάνατον παραδιδόμεθα διὰ ᾿Ιησοῦν, ἵνα καὶ ἡ ζωὴ τοῦ ᾿Ιησοῦ φανερωθῇ ἐν τῇ θνητῇ σαρκὶ ἡμῶν.
gr_gr_cog~CO2~C004~012~ὥστε ὁ μὲν θάνατος ἐν ἡμῖν ἐνεργεῖται, ἡ δὲ ζωὴ ἐν ὑμῖν.
gr_gr_cog~CO2~C004~013~ἔχοντες δὲ τὸ αὐτὸ πνεῦμα τῆς πίστεως κατὰ τὸ γεγραμμένον,ἐπίστευσα, διὸ ἐλάλησα,καὶ ἡμεῖς πιστεύομεν, διὸ καὶ λαλοῦμεν,
gr_gr_cog~CO2~C004~014~εἰδότες ὅτι ὁ ἐγείρας τὸν Κύριον ᾿Ιησοῦν καὶ ἡμᾶς διὰ ᾿Ιησοῦ ἐγερεῖ καὶ παραστήσει σὺν ὑμῖν.
gr_gr_cog~CO2~C004~015~τὰ γὰρ πάντα δι᾿ ὑμᾶς, ἵνα ἡ χάρις πλεονάσασα διὰ τῶν πλειόνων τὴν εὐχαριστίαν περισσεύσῃ εἰς τὴν δόξαν τοῦ Θεοῦ.
gr_gr_cog~CO2~C004~016~Διὸ οὐκ ἐκκακοῦμεν, ἀλλ᾿ εἰ καὶ ὁ ἔξω ἡμῶν ἄνθρωπος διαφθείρεται, ἀλλ᾿ ὁ ἔσωθεν ἀνακαινοῦται ἡμέρᾳ καὶ ἡμέρᾳ.
gr_gr_cog~CO2~C004~017~τὸ γὰρ παραυτίκα ἐλαφρὸν τῆς θλίψεως ἡμῶν καθ᾿ ὑπερβολὴν εἰς ὑπερβολὴν αἰώνιον βάρος δόξης κατεργάζεται ἡμῖν,
gr_gr_cog~CO2~C004~018~μὴ σκοπούντων ἡμῶν τὰ βλεπόμενα, ἀλλὰ τὰ μὴ βλεπόμενα· τὰ γὰρ βλεπόμενα πρόσκαιρα, τὰ δὲ μὴ βλεπόμενα αἰώνια.
gr_gr_cog~CO2~C005~001~Οἴδαμεν γὰρ ὅτι ἐὰν ἡ ἐπίγειος ἡμῶν οἰκία τοῦ σκήνους καταλυθῇ, οἰκοδομὴν ἐκ Θεοῦ ἔχομεν, οἰκίαν ἀχειροποίητον αἰώνιον ἐν τοῖς οὐρανοῖς.
gr_gr_cog~CO2~C005~002~καὶ γὰρ ἐν τούτῳ στενάζομεν, τὸ οἰκητήριον ἡμῶν τὸ ἐξ οὐρανοῦ ἐπενδύσασθαι ἐπιποθοῦντες,
gr_gr_cog~CO2~C005~003~εἴ γε καὶ ἐνδυσάμενοι οὐ γυμνοὶ εὑρεθησόμεθα.
gr_gr_cog~CO2~C005~004~καὶ γὰρ οἱ ὄντες ἐν τῷ σκήνει στενάζομεν, βαρούμενοι ἐφ᾽ ᾧ οὐ θέλομεν ἐκδύσασθαι, ἀλλ᾿ ἐπενδύσασθαι, ἵνα καταποθῇ τὸ θνητὸν ὑπὸ τῆς ζωῆς.
gr_gr_cog~CO2~C005~005~ὁ δὲ κατεργασάμενος ἡμᾶς εἰς αὐτὸ τοῦτο Θεός, ὁ καὶ δοὺς ἡμῖν τὸν ἀρραβῶνα τοῦ Πνεύματος.
gr_gr_cog~CO2~C005~006~Θαρροῦντες οὖν πάντοτε καὶ εἰδότες ὅτι ἐνδημοῦντες ἐν τῷ σώματι ἐκδημοῦμεν ἀπὸ τοῦ Κυρίου·
gr_gr_cog~CO2~C005~007~διὰ πίστεως γὰρ περιπατοῦμεν, οὐ διὰ εἴδους· –
gr_gr_cog~CO2~C005~008~θαρροῦμεν δὲ καὶ εὐδοκοῦμεν μᾶλλον ἐκδημῆσαι ἐκ τοῦ σώματος καὶ ἐνδημῆσαι πρὸς τὸν Κύριον.
gr_gr_cog~CO2~C005~009~διὸ καὶ φιλοτιμούμεθα, εἴτε ἐνδημοῦντες εἴτε ἐκδημοῦντες, εὐάρεστοι αὐτῷ εἶναι.
gr_gr_cog~CO2~C005~010~τοὺς γὰρ πάντας ἡμᾶς φανερωθῆναι δεῖ ἔμπροσθεν τοῦ βήματος τοῦ Χριστοῦ, ἵνα κομίσηται ἕκαστος τὰ διὰ τοῦ σώματος πρὸς ἃ ἔπραξεν, εἴτε ἀγαθὸν εἴτε κακόν.
gr_gr_cog~CO2~C005~011~Εἰδότες οὖν τὸν φόβον τοῦ Κυρίου ἀνθρώπους πείθομεν, Θεῷ δὲ πεφανερώμεθα, ἐλπίζω δὲ καὶ ἐν ταῖς συνειδήσεσιν ὑμῶν πεφανερῶσθαι.
gr_gr_cog~CO2~C005~012~οὐ γὰρ πάλιν ἑαυτοὺς συνιστάνομεν ὑμῖν, ἀλλὰ ἀφορμὴν διδόντες ὑμῖν καυχήματος ὑπὲρ ἡμῶν, ἵνα ἔχητε πρὸς τοὺς ἐν προσώπῳ καυχωμένους καὶ οὐ καρδίᾳ.
gr_gr_cog~CO2~C005~013~εἴτε γὰρ ἐξέστημεν, Θεῷ, εἴτε σωφρονοῦμεν, ὑμῖν.
gr_gr_cog~CO2~C005~014~ἡ γὰρ ἀγάπη τοῦ Χριστοῦ συνέχει ἡμᾶς,
gr_gr_cog~CO2~C005~015~κρίναντας τοῦτο, ὅτι εἰ εἷς ὑπὲρ πάντων ἀπέθανεν, ἄρα οἱ πάντες ἀπέθανον· καὶ ὑπὲρ πάντων ἀπέθανεν, ἵνα οἱ ζῶντες μηκέτι ἑαυτοῖς ζῶσιν, ἀλλὰ τῷ ὑπὲρ αὐτῶν ἀποθανόντι καὶ ἐγερθέντι.
gr_gr_cog~CO2~C005~016~῞Ωστε ἡμεῖς ἀπὸ τοῦ νῦν οὐδένα οἴδαμεν κατὰ σάρκα· εἰ δὲ καὶ ἐγνώκαμεν κατὰ σάρκα Χριστόν, ἀλλὰ νῦν οὐκέτι γινώσκομεν.
gr_gr_cog~CO2~C005~017~ὥστε εἴ τις ἐν Χριστῷ, καινὴ κτίσις· τὰ ἀρχαῖα παρῆλθεν, ἰδοὺ γέγονε καινὰ τὰ πάντα.
gr_gr_cog~CO2~C005~018~τὰ δὲ πάντα ἐκ τοῦ Θεοῦ τοῦ καταλλάξαντος ἡμᾶς ἑαυτῷ διὰ ᾿Ιησοῦ Χριστοῦ καὶ δόντος ἡμῖν τὴν διακονίαν τῆς καταλλαγῆς,
gr_gr_cog~CO2~C005~019~ὡς ὅτι Θεὸς ἦν ἐν Χριστῷ κόσμον καταλλάσσων ἑαυτῷ, μὴ λογιζόμενος αὐτοῖς τὰ παραπτώματα αὐτῶν, καὶ θέμενος ἐν ἡμῖν τὸν λόγον τῆς καταλλαγῆς.
gr_gr_cog~CO2~C005~020~῾Υπὲρ Χριστοῦ οὖν πρεσβεύομεν ὡς τοῦ Θεοῦ παρακαλοῦντος δι᾿ ἡμῶν· δεόμεθα ὑπὲρ Χριστοῦ, καταλλάγητε τῷ Θεῷ·
gr_gr_cog~CO2~C005~021~τὸν γὰρ μὴ γνόντα ἁμαρτίαν ὑπὲρ ἡμῶν ἁμαρτίαν ἐποίησεν, ἵνα ἡμεῖς γενώμεθα δικαιοσύνη Θεοῦ ἐν αὐτῷ.
gr_gr_cog~CO2~C006~001~Συνεργοῦντες δὲ καὶ παρακαλοῦμεν μὴ εἰς κενὸν τὴν χάριν τοῦ Θεοῦ δέξασθαι ὑμᾶς –
gr_gr_cog~CO2~C006~002~λέγει γάρ·καιρῷ δεκτῷ ἐπήκουσά σου καὶ ἐν ἡμέρᾳ σωτηρίας ἐβοήθησά σοι·ἰδοὺ νῦνκαιρὸς εὐπρόσδεκτος,ἰδοὺ νῦνἡμέρα σωτηρίας–
gr_gr_cog~CO2~C006~003~μηδεμίαν ἐν μηδενὶ διδόντες προσκοπήν, ἵνα μὴ μωμηθῇ ἡ διακονία,
gr_gr_cog~CO2~C006~004~ἀλλ᾿ ἐν παντὶ συνιστῶντες ἑαυτοὺς ὡς Θεοῦ διάκονοι, ἐν ὑπομονῇ πολλῇ, ἐν θλίψεσιν, ἐν ἀνάγκαις, ἐν στενοχωρίαις,
gr_gr_cog~CO2~C006~005~ἐν πληγαῖς, ἐν φυλακαῖς, ἐν ἀκαταστασίαις, ἐν κόποις, ἐν ἀγρυπνίαις, ἐν νηστείαις,
gr_gr_cog~CO2~C006~006~ἐν ἁγνότητι, ἐν γνώσει, ἐν μακροθυμίᾳ, ἐν χρηστότητι, ἐν Πνεύματι ῾Αγίῳ, ἐν ἀγάπῃ ἀνυποκρίτῳ,
gr_gr_cog~CO2~C006~007~ἐν λόγῳ ἀληθείας, ἐν δυνάμει Θεοῦ, διὰ τῶν ὅπλων τῆς δικαιοσύνης τῶν δεξιῶν καὶ ἀριστερῶν,
gr_gr_cog~CO2~C006~008~διὰ δόξης καὶ ἀτιμίας, διὰ δυσφημίας καὶ εὐφημίας, ὡς πλάνοι καὶ ἀληθεῖς,
gr_gr_cog~CO2~C006~009~ὡς ἀγνοούμενοι καὶ ἐπιγινωσκόμενοι, ὡς ἀποθνήσκοντες καὶ ἰδοὺ ζῶμεν, ὡς παιδευόμενοι καὶ μὴ θανατούμενοι,
gr_gr_cog~CO2~C006~010~ὡς λυπούμενοι ἀεὶ δὲ χαίροντες, ὡς πτωχοὶ πολλοὺς δὲ πλουτίζοντες, ὡς μηδὲν ἔχοντες καὶ πάντα κατέχοντες.
gr_gr_cog~CO2~C006~011~Τὸ στόμα ἡμῶν ἀνέῳγε πρὸς ὑμᾶς, Κορίνθιοι, ἡ καρδία ἡμῶν πεπλάτυνται·
gr_gr_cog~CO2~C006~012~οὐ στενοχωρεῖσθε ἐν ἡμῖν, στενοχωρεῖσθε δὲ ἐν τοῖς σπλάγχνοις ὑμῶν·
gr_gr_cog~CO2~C006~013~τὴν δὲ αὐτὴν ἀντιμισθίαν, ὡς τέκνοις λέγω, πλατύνθητε καὶ ὑμεῖς.
gr_gr_cog~CO2~C006~014~Μὴ γίνεσθε ἑτεροζυγοῦντες ἀπίστοις· τίς γὰρ μετοχὴ δικαιοσύνῃ καὶ ἀνομίᾳ; τίς δὲ κοινωνία φωτὶ πρὸς σκότος;
gr_gr_cog~CO2~C006~015~τίς δὲ συμφώνησις Χριστῷ πρὸς Βελίαλ; ἢ τίς μερὶς πιστῷ μετὰ ἀπίστου;
gr_gr_cog~CO2~C006~016~τίς δὲ συγκατάθεσις ναῷ Θεοῦ μετὰ εἰδώλων; ὑμεῖς γὰρ ναὸς Θεοῦ ἐστε ζῶντος, καθὼς εἶπεν ὁ Θεὸς ὅτιἐνοικήσω ἐν αὐτοῖς καὶ ἐμπεριπατήσω, καὶ ἔσομαι αὐτῶν Θεός, καὶ αὐτοὶ ἔσονταί μοι λαός.
gr_gr_cog~CO2~C006~017~διὸἐξέλθατε ἐκ μέσου αὐτῶν καὶ ἀφορίσθητε, λέγει Κύριος, καὶ ἀκαθάρτου μὴ ἅπτεσθε, κἀγὼ εἰσδέξομαι ὑμᾶς,
gr_gr_cog~CO2~C006~018~καὶἔσομαιὑμῖνεἰς πατέρα, καὶὑμεῖς ἔσεσθέμοι εἰς υἱοὺςκαὶ θυγατέρας,λέγει Κύριος παντοκράτωρ.
gr_gr_cog~CO2~C007~001~Ταύτας οὖν ἔχοντες τὰς ἐπαγγελίας, ἀγαπητοί, καθαρίσωμεν ἑαυτοὺς ἀπὸ παντὸς μολυσμοῦ σαρκὸς καὶ πνεύματος, ἐπιτελοῦντες ἁγιωσύνην ἐν φόβῳ Θεοῦ.
gr_gr_cog~CO2~C007~002~Χωρήσατε ἡμᾶς· οὐδένα ἠδικήσαμεν, οὐδένα ἐφθείραμεν, οὐδένα ἐπλεονεκτήσαμεν.
gr_gr_cog~CO2~C007~003~οὐ πρὸς κατάκρισιν λέγω· προείρηκα γὰρ ὅτι ἐν ταῖς καρδίαις ἡμῶν ἐστε εἰς τὸ συναποθανεῖν καὶ συζῆν.
gr_gr_cog~CO2~C007~004~πολλή μοι παρρησία πρὸς ὑμᾶς, πολλή μοι καύχησις ὑπὲρ ὑμῶν· πεπλήρωμαι τῇ παρακλήσει, ὑπερπερισσεύομαι τῇ χαρᾷ ἐπὶ πάσῃ τῇ θλίψει ἡμῶν.
gr_gr_cog~CO2~C007~005~Καὶ γὰρ ἐλθόντων ἡμῶν εἰς Μακεδονίαν οὐδεμίαν ἔσχηκεν ἄνεσιν ἡ σὰρξ ἡμῶν, ἀλλ᾿ ἐν παντὶ θλιβόμενοι· ἔξωθεν μάχαι, ἔσωθεν φόβοι.
gr_gr_cog~CO2~C007~006~ἀλλ᾿ ὁ παρακαλῶν τοὺς ταπεινοὺς παρεκάλεσεν ἡμᾶς ὁ Θεὸς ἐν τῇ παρουσίᾳ Τίτου·
gr_gr_cog~CO2~C007~007~οὐ μόνον δὲ ἐν τῇ παρουσίᾳ αὐτοῦ, ἀλλὰ καὶ ἐν τῇ παρακλήσει ᾗ παρεκλήθη ἐφ᾿ ὑμῖν, ἀναγγέλλων ἡμῖν τὴν ὑμῶν ἐπιπόθησιν, τὸν ὑμῶν ὀδυρμόν, τὸν ὑμῶν ζῆλον ὑπὲρ ἐμοῦ, ὥστε με μᾶλλον χαρῆναι,
gr_gr_cog~CO2~C007~008~ὅτι εἰ καὶ ἐλύπησα ὑμᾶς ἐν τῇ ἐπιστολῇ, οὐ μεταμέλομαι, εἰ καὶ μετεμελόμην· βλέπω γὰρ ὅτι ἡ ἐπιστολὴ ἐκείνη, εἰ καὶ πρὸς ὥραν, ἐλύπησεν ὑμᾶς.
gr_gr_cog~CO2~C007~009~νῦν χαίρω, οὐχ ὅτι ἐλυπήθητε, ἀλλ᾿ ὅτι ἐλυπήθητε εἰς μετάνοιαν· ἐλυπήθητε γὰρ κατὰ Θεόν, ἵνα ἐν μηδενὶ ζημιωθῆτε ἐξ ἡμῶν.
gr_gr_cog~CO2~C007~010~ἡ γὰρ κατὰ Θεὸν λύπη μετάνοιαν εἰς σωτηρίαν ἀμεταμέλητον κατεργάζεται· ἡ δὲ τοῦ κόσμου λύπη θάνατον κατεργάζεται.
gr_gr_cog~CO2~C007~011~ἰδοὺ γὰρ αὐτὸ τοῦτο, τὸ κατὰ Θεὸν λυπηθῆναι ὑμᾶς, πόσην κατειργάσατο ὑμῖν σπουδήν, ἀλλὰ ἀπολογίαν, ἀλλὰ ἀγανάκτησιν, ἀλλὰ φόβον, ἀλλὰ ἐπιπόθησιν, ἀλλὰ ζῆλον, ἀλλὰ ἐκδίκησιν! ἐν παντὶ συνεστήσατε ἑαυτοὺς ἁγνοὺς εἶναι ἐν τῷ πράγματι.
gr_gr_cog~CO2~C007~012~ἄρα εἰ καὶ ἔγραψα ὑμῖν, οὐχ εἵνεκεν τοῦ ἀδικήσαντος, οὐδὲ εἵνεκεν τοῦ ἀδικηθέντος, ἀλλ᾿ εἵνεκεν τοῦ φανερωθῆναι τὴν σπουδὴν ὑμῶν τὴν ὑπὲρ ἡμῶν πρὸς ὑμᾶς ἐνώπιον τοῦ Θεοῦ.
gr_gr_cog~CO2~C007~013~Διὰ τοῦτο παρακεκλήμεθα. ἐπὶ δὲ τῇ παρακλήσει ὑμῶν περισσοτέρως μᾶλλον ἐχάρημεν ἐπὶ τῇ χαρᾷ Τίτου, ὅτι ἀναπέπαυται τὸ πνεῦμα αὐτοῦ ἀπὸ πάντων ὑμῶν·
gr_gr_cog~CO2~C007~014~Διὰ τοῦτο παρακεκλήμεθα. ἐπὶ δὲ τῇ παρακλήσει ὑμῶν περισσοτέρως μᾶλλον ἐχάρημεν ἐπὶ τῇ χαρᾷ Τίτου, ὅτι ἀναπέπαυται τὸ πνεῦμα αὐτοῦ ἀπὸ πάντων ὑμῶν·
gr_gr_cog~CO2~C007~015~καὶ τὰ σπλάγχνα αὐτοῦ περισσοτέρως εἰς ὑμᾶς ἐστιν ἀναμιμνησκομένου τὴν πάντων ὑμῶν ὑπακοήν, ὡς μετὰ φόβου καὶ τρόμου ἐδέξασθε αὐτόν.
gr_gr_cog~CO2~C007~016~χαίρω ὅτι ἐν παντὶ θαρρῶ ἐν ὑμῖν.
gr_gr_cog~CO2~C008~001~Γνωρίζω δὲ ὑμῖν, ἀδελφοί, τὴν χάριν τοῦ Θεοῦ τὴν δεδομένην ἐν ταῖς ἐκκλησίαις τῆς Μακεδονίας,
gr_gr_cog~CO2~C008~002~ὅτι ἐν πολλῇ δοκιμῇ θλίψεως ἡ περισσεία τῆς χαρᾶς αὐτῶν καὶ ἡ κατὰ βάθους πτωχεία αὐτῶν ἐπερίσσευσεν εἰς τὸν πλοῦτον τῆς ἁπλότητος αὐτῶν·
gr_gr_cog~CO2~C008~003~ὅτι κατὰ δύναμιν, μαρτυρῶ, καὶ ὑπὲρ δύναμιν, αὐθαίρετοι,
gr_gr_cog~CO2~C008~004~μετὰ πολλῆς παρακλήσεως δεόμενοι ἡμῶν τὴν χάριν καὶ τὴν κοινωνίαν τῆς διακονίας τῆς εἰς τοὺς ἁγίους,
gr_gr_cog~CO2~C008~005~καὶ οὐ καθὼς ἠλπίσαμεν, ἀλλ᾽ ἑαυτοὺς ἔδωκαν πρῶτον τῷ Κυρίῳ καὶ ἡμῖν διὰ θελήματος Θεοῦ,
gr_gr_cog~CO2~C008~006~εἰς τὸ παρακαλέσαι ἡμᾶς Τίτον, ἵνα καθὼς προενήρξατο οὕτω καὶ ἐπιτελέσῃ εἰς ὑμᾶς καὶ τὴν χάριν ταύτην.
gr_gr_cog~CO2~C008~007~ἀλλ᾿ ὥσπερ ἐν παντὶ περισσεύετε, πίστει καὶ λόγῳ καὶ γνώσει καὶ πάσῃ σπουδῇ καὶ τῇ ἐξ ὑμῶν ἐν ἡμῖν ἀγάπῃ, ἵνα καὶ ἐν ταύτῃ τῇ χάριτι περισσεύητε.
gr_gr_cog~CO2~C008~008~Οὐ κατ᾿ ἐπιταγὴν λέγω, ἀλλὰ διὰ τῆς ἑτέρων σπουδῆς καὶ τὸ τῆς ὑμετέρας ἀγάπης γνήσιον δοκιμάζων·
gr_gr_cog~CO2~C008~009~γινώσκετε γὰρ τὴν χάριν τοῦ Κυρίου ἡμῶν ᾿Ιησοῦ Χριστοῦ, ὅτι δι᾿ ὑμᾶς ἐπτώχευσε πλούσιος ὤν, ἵνα ὑμεῖς τῇ ἐκείνου πτωχείᾳ πλουτήσητε.
gr_gr_cog~CO2~C008~010~καὶ γνώμην ἐν τούτῳ δίδωμι· τοῦτο γὰρ ὑμῖν συμφέρει, οἵτινες οὐ μόνον τὸ ποιῆσαι, ἀλλὰ καὶ τὸ θέλειν προενήρξασθε ἀπὸ πέρυσι·
gr_gr_cog~CO2~C008~011~νυνὶ δὲ καὶ τὸ ποιῆσαι ἐπιτελέσατε, ὅπως καθάπερ ἡ προθυμία τοῦ θέλειν, οὕτω καὶ τὸ ἐπιτελέσαι ἐκ τοῦ ἔχειν.
gr_gr_cog~CO2~C008~012~εἰ γὰρ ἡ προθυμία πρόκειται, καθὸ ἐὰν ἔχῃ τις εὐπρόσδεκτος, οὐ καθὸ οὐκ ἔχει.
gr_gr_cog~CO2~C008~013~οὐ γὰρ ἵνα ἄλλοις ἄνεσις, ὑμῖν δὲ θλῖψις, ἀλλ᾿ ἐξ ἰσότητος ἐν τῷ νῦν καιρῷ τὸ ὑμῶν περίσσευμα εἰς τὸ ἐκείνων ὑστέρημα,
gr_gr_cog~CO2~C008~014~ἵνα καὶ τὸ ἐκείνων περίσσευμα γένηται εἰς τὸ ὑμῶν ὑστέρημα, ὅπως γένηται ἰσότης,
gr_gr_cog~CO2~C008~015~καθὼς γέγραπται·ὁ τὸ πολὺ οὐκ ἐπλεόνασε, καὶ ὁ τὸ ὀλίγον οὐκ ἠλαττόνησε.
gr_gr_cog~CO2~C008~016~Χάρις δὲ τῷ Θεῷ τῷ διδόντι τὴν αὐτὴν σπουδὴν ὑπὲρ ὑμῶν ἐν τῇ καρδίᾳ Τίτου,
gr_gr_cog~CO2~C008~017~ὅτι τὴν μὲν παράκλησιν ἐδέξατο, σπουδαιότερος δὲ ὑπάρχων αὐθαίρετος ἐξῆλθε πρὸς ὑμᾶς.
gr_gr_cog~CO2~C008~018~συνεπέμψαμεν δὲ μετ᾿ αὐτοῦ τὸν ἀδελφὸν οὗ ὁ ἔπαινος ἐν τῷ εὐαγγελίῳ διὰ πασῶν τῶν ἐκκλησιῶν· –
gr_gr_cog~CO2~C008~019~οὐ μόνον δέ, ἀλλὰ καὶ χειροτονηθεὶς ὑπὸ τῶν ἐκκλησιῶν συνέκδημος ἡμῶν σὺν τῇ χάριτι ταύτῃ τῇ διακονουμένῃ ὑφ᾿ ἡμῶν πρὸς τὴν αὐτοῦ τοῦ Κυρίου δόξαν καὶ προθυμίαν ἡμῶν· –
gr_gr_cog~CO2~C008~020~στελλόμενοι τοῦτο, μή τις ἡμᾶς μωμήσηται ἐν τῇ ἁδρότητι ταύτῃ τῇ διακονουμένῃ ὑφ᾿ ἡμῶν,
gr_gr_cog~CO2~C008~021~προνοούμενοι καλὰ οὐ μόνον ἐνώπιον Κυρίου, ἀλλὰ καὶ ἐνώπιον ἀνθρώπων.
gr_gr_cog~CO2~C008~022~συνεπέμψαμεν δὲ αὐτοῖς τὸν ἀδελφὸν ἡμῶν, ὃν ἐδοκιμάσαμεν ἐν πολλοῖς πολλάκις σπουδαῖον ὄντα, νυνὶ δὲ πολὺ σπουδαιότερον πεποιθήσει πολλῇ τῇ εἰς ὑμᾶς.
gr_gr_cog~CO2~C008~023~εἴτε ὑπὲρ Τίτου, κοινωνὸς ἐμὸς καὶ εἰς ὑμᾶς συνεργός· εἴτε ἀδελφοὶ ἡμῶν, ἀπόστολοι ἐκκλησιῶν, δόξα Χριστοῦ.
gr_gr_cog~CO2~C008~024~Τὴν οὖν ἔνδειξιν τῆς ἀγάπης ὑμῶν καὶ ἡμῶν καυχήσεως ὑπὲρ ὑμῶν εἰς αὐτοὺς ἐνδείξασθε εἰς πρόσωπον τῶν ἐκκλησιῶν.
gr_gr_cog~CO2~C009~001~Περὶ μὲν γὰρ τῆς διακονίας τῆς εἰς τοὺς ἁγίους περισσόν μοί ἐστι τὸ γράφειν ὑμῖν·
gr_gr_cog~CO2~C009~002~οἶδα γὰρ τὴν προθυμίαν ὑμῶν ἣν ὑπὲρ ἡμῶν καυχῶμαι Μακεδόσιν, ὅτι ᾿Αχαΐα παρεσκεύασται ἀπὸ πέρυσι· καὶ ὁ ἐξ ὑμῶν ζῆλος ἠρέθισε τοὺς πλείονας.
gr_gr_cog~CO2~C009~003~ἔπεμψα δὲ τοὺς ἀδελφούς, ἵνα μὴ τὸ καύχημα ἡμῶν τὸ ὑπὲρ ὑμῶν κενωθῇ ἐν τῷ μέρει τούτῳ, ἵνα, καθὼς ἔλεγον, παρεσκευασμένοι ἦτε,
gr_gr_cog~CO2~C009~004~μήπως ἐὰν ἔλθωσι σὺν ἐμοὶ Μακεδόνες καὶ εὕρωσιν ὑμᾶς ἀπαρασκευάστους, καταισχυνθῶμεν ἡμεῖς, ἵνα μὴ λέγωμεν ὑμεῖς, ἐν τῇ ὑποστάσει ταύτῃ τῆς καυχήσεως.
gr_gr_cog~CO2~C009~005~ἀναγκαῖον οὖν ἡγησάμην παρακαλέσαι τοὺς ἀδελφοὺς ἵνα προέλθωσιν εἰς ὑμᾶς καὶ προκαταρτίσωσι τὴν προκατηγγελμένην εὐλογίαν ὑμῶν, ταύτην ἑτοίμην εἶναι, οὕτως ὡς εὐλογίαν καὶ μὴ ὡς πλεονεξίαν.
gr_gr_cog~CO2~C009~006~Τοῦτο δέ, ὁ σπείρων φειδομένως φειδομένως καὶ θερίσει, καὶ ὁ σπείρων ἐπ᾿ εὐλογίαις ἐπ᾿ εὐλογίαις καὶ θερίσει.
gr_gr_cog~CO2~C009~007~ἕκαστος καθὼς προαιρεῖται τῇ καρδία, μὴ ἐκ λύπης ἢ ἐξ ἀνάγκης·ἱλαρὸν γὰρ δότην ἀγαπᾷ ὁ Θεός.
gr_gr_cog~CO2~C009~008~δυνατὸς δὲ ὁ Θεὸς πᾶσαν χάριν περισσεῦσαι εἰς ὑμᾶς, ἵνα ἐν παντὶ πάντοτε πᾶσαν αὐτάρκειαν ἔχοντες περισσεύητε εἰς πᾶν ἔργον ἀγαθόν,
gr_gr_cog~CO2~C009~009~καθὼς γέγραπται·ἐσκόρπισεν, ἔδωκε τοῖς πένησιν· ἡ δικαιοσύνη αὐτοῦ μένει εἰς τὸν αἰῶνα.
gr_gr_cog~CO2~C009~010~ὁ δὲ ἐπιχορηγῶν σπέρμα τῷ σπείροντι καὶ ἄρτον εἰς βρῶσιν χορηγήσαι καὶ πληθύναι τὸν σπόρον ὑμῶν καὶ αὐξήσαι τὰ γενήματα τῆς δικαιοσύνης ὑμῶν·
gr_gr_cog~CO2~C009~011~ἐν παντὶ πλουτιζόμενοι εἰς πᾶσαν ἁπλότητα, ἥτις κατεργάζεται δι᾿ ἡμῶν εὐχαριστίαν τῷ Θεῷ·
gr_gr_cog~CO2~C009~012~ὅτι ἡ διακονία τῆς λειτουργίας ταύτης οὐ μόνον ἐστὶ προσαναπληροῦσα τὰ ὑστερήματα τῶν ἁγίων, ἀλλὰ καὶ περισσεύουσα διὰ πολλῶν εὐχαριστιῶν τῷ Θεῷ· –
gr_gr_cog~CO2~C009~013~διὰ τῆς δοκιμῆς τῆς διακονίας ταύτης δοξάζοντες τὸν Θεὸν ἐπὶ τῇ ὑποταγῇ τῆς ὁμολογίας ὑμῶν εἰς τὸ εὐαγγέλιον τοῦ Χριστοῦ καὶ ἁπλότητι τῆς κοινωνίας εἰς αὐτοὺς καὶ εἰς πάντας,
gr_gr_cog~CO2~C009~014~καὶ αὐτῶν δεήσει ὑπὲρ ὑμῶν, ἐπιποθούντων ὑμᾶς διὰ τὴν ὑπερβάλλουσαν χάριν τοῦ Θεοῦ ἐφ᾿ ὑμῖν.
gr_gr_cog~CO2~C009~015~χάρις δὲ τῷ Θεῷ ἐπὶ τῇ ἀνεκδιηγήτῳ αὐτοῦ δωρεᾷ.
gr_gr_cog~CO2~C010~001~Αὐτὸς δὲ ἐγὼ Παῦλος παρακαλῶ ὑμᾶς διὰ τῆς πρᾳότητος καὶ ἐπιεικείας τοῦ Χριστοῦ, ὃς κατὰ πρόσωπον μὲν ταπεινὸς ἐν ὑμῖν, ἀπὼν δὲ θαρρῶ εἰς ὑμᾶς·
gr_gr_cog~CO2~C010~002~δέομαι δὲ τὸ μὴ παρὼν θαρρῆσαι τῇ πεποιθήσει ᾗ λογίζομαι τολμῆσαι ἐπί τινας τοὺς λογιζομένους ἡμᾶς ὡς κατὰ σάρκα περιπατοῦντας.
gr_gr_cog~CO2~C010~003~᾿Εν σαρκὶ γὰρ περιπατοῦντες οὐ κατὰ σάρκα στρατευόμεθα·
gr_gr_cog~CO2~C010~004~τὰ γὰρ ὅπλα τῆς στρατείας ἡμῶν οὐ σαρκικά, ἀλλὰ δυνατὰ τῷ Θεῷ πρὸς καθαίρεσιν ὀχυρωμάτων· —
gr_gr_cog~CO2~C010~005~λογισμοὺς καθαιροῦντες καὶ πᾶν ὕψωμα ἐπαιρόμενον κατὰ τῆς γνώσεως τοῦ Θεοῦ, καὶ αἰχμαλωτίζοντες πᾶν νόημα εἰς τὴν ὑπακοὴν τοῦ Χριστοῦ,
gr_gr_cog~CO2~C010~006~καὶ ἐν ἑτοίμῳ ἔχοντες ἐκδικῆσαι πᾶσαν παρακοήν, ὅταν πληρωθῇ ὑμῶν ἡ ὑπακοή.
gr_gr_cog~CO2~C010~007~Τὰ κατὰ πρόσωπον βλέπετε! εἴ τις πέποιθεν ἑαυτῷ Χριστοῦ εἶναι, τοῦτο λογιζέσθω πάλιν ἀφ᾿ ἑαυτοῦ, ὅτι καθὼς αὐτὸς Χριστοῦ, οὕτω καὶ ἡμεῖς Χριστοῦ.
gr_gr_cog~CO2~C010~008~ἐάν τε γὰρ καὶ περισσότερόν τι καυχήσωμαι περὶ τῆς ἐξουσίας ἡμῶν, ἧς ἔδωκεν ὁ Κύριος ἡμῖν εἰς οἰκοδομὴν καὶ οὐκ εἰς καθαίρεσιν ὑμῶν, οὐκ αἰσχυνθήσομαι,
gr_gr_cog~CO2~C010~009~ἵνα μὴ δόξω ὡς ἂν ἐκφοβεῖν ὑμᾶς διὰ τῶν ἐπιστολῶν.
gr_gr_cog~CO2~C010~010~ὅτι αἱ μὲν ἐπιστολαί, φησί, βαρεῖαι καὶ ἰσχυραί, ἡ δὲ παρουσία τοῦ σώματος ἀσθενὴς καὶ ὁ λόγος ἐξουθενημένος.
gr_gr_cog~CO2~C010~011~τοῦτο λογιζέσθω ὁ τοιοῦτος, ὅτι οἷοί ἐσμεν τῷ λόγῳ δι᾿ ἐπιστολῶν ἀπόντες, τοιοῦτοι καὶ παρόντες τῷ ἔργῳ.
gr_gr_cog~CO2~C010~012~Οὐ γὰρ τολμῶμεν ἐγκρῖναι ἢ συγκρῖναι ἑαυτούς τισι τῶν ἑαυτοὺς συνιστανόντων· ἀλλὰ αὐτοὶ ἐν ἑαυτοῖς ἑαυτοὺς μετροῦντες καὶ συγκρίνοντες ἑαυτοὺς ἑαυτοῖς οὐ συνιοῦσιν.
gr_gr_cog~CO2~C010~013~ἡμεῖς δὲ οὐχὶ εἰς τὰ ἄμετρα καυχησόμεθα, ἀλλὰ κατὰ τὸ μέτρον τοῦ κανόνος οὗ ἐμέρισεν ἡμῖν ὁ Θεὸς μέτρου, ἐφικέσθαι ἄχρι καὶ ὑμῶν.
gr_gr_cog~CO2~C010~014~οὐ γὰρ ὡς μὴ ἐφικνούμενοι εἰς ὑμᾶς ὑπερεκτείνομεν ἑαυτούς· ἄχρι γὰρ καὶ ὑμῶν ἐφθάσαμεν ἐν τῷ εὐαγγελίῳ τοῦ Χριστοῦ,
gr_gr_cog~CO2~C010~015~οὐκ εἰς τὰ ἄμετρα καυχώμενοι ἐν ἀλλοτρίοις κόποις, ἐλπίδα δὲ ἔχοντες, αὐξανομένης τῆς πίστεως ὑμῶν, ἐν ὑμῖν μεγαλυνθῆναι κατὰ τὸν κανόνα ἡμῶν εἰς περισσείαν,
gr_gr_cog~CO2~C010~016~εἰς τὰ ὑπερέκεινα ὑμῶν εὐαγγελίσασθαι, οὐκ ἐν ἀλλοτρίῳ κανόνι εἰς τὰ ἕτοιμα καυχήσασθαι.
gr_gr_cog~CO2~C010~017~῾Ο δὲ καυχώμενος ἐν Κυρίῳ καυχάσθω·
gr_gr_cog~CO2~C010~018~οὐ γὰρ ὁ ἑαυτὸν συνιστῶν, ἐκεῖνός ἐστι δόκιμος, ἀλλ᾿ ὃν ὁ Κύριος συνίστησιν.
gr_gr_cog~CO2~C011~001~Ὄφελον ἀνείχεσθέ μου μικρὸν τῇ ἀφροσύνῃ· ἀλλὰ καὶ ἀνέχεσθέ μου·
gr_gr_cog~CO2~C011~002~ζηλῶ γὰρ ὑμᾶς Θεοῦ ζήλῳ· ἡρμοσάμην γὰρ ὑμᾶς ἑνὶ ἀνδρί, παρθένον ἁγνὴν παραστῆσαι τῷ Χριστῷ·
gr_gr_cog~CO2~C011~003~φοβοῦμαι δὲ μήπως, ὡς ὁ ὄφις Εὔαν ἐξηπάτησεν ἐν τῇ πανουργίᾳ αὐτοῦ, οὕτω φθαρῇ τὰ νοήματα ὑμῶν ἀπὸ τῆς ἁπλότητος τῆς εἰς τὸν Χριστόν.
gr_gr_cog~CO2~C011~004~εἰ μὲν γὰρ ὁ ἐρχόμενος ἄλλον ᾿Ιησοῦν κηρύσσει ὃν οὐκ ἐκηρύξαμεν, ἢ πνεῦμα ἕτερον λαμβάνετε ὃ οὐκ ἐλάβετε, ἢ εὐαγγέλιον ἕτερον ὃ οὐκ ἐδέξασθε, καλῶς ἀνείχεσθε.
gr_gr_cog~CO2~C011~005~λογίζομαι γὰρ μηδὲν ὑστερηκέναι τῶν ὑπερλίαν ἀποστόλων.
gr_gr_cog~CO2~C011~006~εἰ δὲ καὶ ἰδιώτης τῷ λόγῳ, ἀλλ᾿ οὐ τῇ γνώσει, ἀλλ᾿ ἐν παντὶ φανερωθέντες ἐν πᾶσιν εἰς ὑμᾶς.
gr_gr_cog~CO2~C011~007~῍Η ἁμαρτίαν ἐποίησα ἐμαυτὸν ταπεινῶν ἵνα ὑμεῖς ὑψωθῆτε, ὅτι δωρεὰν τὸ τοῦ Θεοῦ εὐαγγέλιον εὐηγγελισάμην ὑμῖν;
gr_gr_cog~CO2~C011~008~ἄλλας ἐκκλησίας ἐσύλησα λαβὼν ὀψώνιον πρὸς τὴν ὑμῶν διακονίαν, καὶ παρὼν πρὸς ὑμᾶς καὶ ὑστερηθεὶς οὐ κατενάρκησα οὐδενός·
gr_gr_cog~CO2~C011~009~τὸ γὰρ ὑστέρημά μου προσανεπλήρωσαν οἱ ἀδελφοὶ ἐλθόντες ἀπὸ Μακεδονίας· καὶ ἐν παντὶ ἀβαρῆ ὑμῖν ἐμαυτὸν ἐτήρησα καὶ τηρήσω.
gr_gr_cog~CO2~C011~010~ἔστιν ἀλήθεια Χριστοῦ ἐν ἐμοὶ ὅτι ἡ καύχησις αὕτη οὐ φραγήσεται εἰς ἐμὲ ἐν τοῖς κλίμασι τῆς ᾿Αχαΐας.
gr_gr_cog~CO2~C011~011~διατί; ὅτι οὐκ ἀγαπῶ ὑμᾶς; ὁ Θεὸς οἶδεν·
gr_gr_cog~CO2~C011~012~ὃ δὲ ποιῶ, καὶ ποιήσω, ἵνα ἐκκόψω τὴν ἀφορμὴν τῶν θελόντων ἀφορμήν, ἵνα ἐν ᾧ καυχῶνται εὑρεθῶσι καθὼς καὶ ἡμεῖς.
gr_gr_cog~CO2~C011~013~οἱ γὰρ τοιοῦτοι ψευδαπόστολοι, ἐργάται δόλιοι, μετασχηματιζόμενοι εἰς ἀποστόλους Χριστοῦ.
gr_gr_cog~CO2~C011~014~καὶ οὐ θαυμαστόν· αὐτὸς γὰρ ὁ σατανᾶς μετασχηματίζεται εἰς ἄγγελον φωτός.
gr_gr_cog~CO2~C011~015~οὐ μέγα οὖν εἰ καὶ οἱ διάκονοι αὐτοῦ μετασχηματίζονται ὡς διάκονοι δικαιοσύνης, ὧν τὸ τέλος ἔσται κατὰ τὰ ἔργα αὐτῶν.
gr_gr_cog~CO2~C011~016~Πάλιν λέγω, μή τίς με δόξῃ ἄφρονα εἶναι· εἰ δὲ μή γε, κἂν ὡς ἄφρονα δέξασθέ με, ἵνα κἀγὼ μικρόν τι καυχήσωμαι.
gr_gr_cog~CO2~C011~017~ὃ λαλῶ, οὐ λαλῶ κατὰ Κύριον, ἀλλ᾿ ὡς ἐν ἀφροσύνῃ, ἐν ταύτῃ τῇ ὑποστάσει τῆς καυχήσεως.
gr_gr_cog~CO2~C011~018~ἐπεὶ πολλοὶ καυχῶνται κατὰ τὴν σάρκα, κἀγὼ καυχήσομαι.
gr_gr_cog~CO2~C011~019~ἡδέως γὰρ ἀνέχεσθε τῶν ἀφρόνων φρόνιμοι ὄντες·
gr_gr_cog~CO2~C011~020~ἀνέχεσθε γὰρ εἴ τις ὑμᾶς καταδουλοῖ, εἴ τις κατεσθίει, εἴ τις λαμβάνει, εἴ τις ἐπαίρεται, εἴ τις ὑμᾶς εἰς πρόσωπον δέρει.
gr_gr_cog~CO2~C011~021~κατὰ ἀτιμίαν λέγω, ὡς ὅτι ἡμεῖς ἠσθενήσαμεν. ἐν ᾧ δ᾿ ἄν τις τολμᾷ, ἐν ἀφροσύνῃ λέγω, τολμῶ κἀγώ.
gr_gr_cog~CO2~C011~022~῾Εβραῖοί εἰσι; κἀγώ· ᾿Ισραηλῖταί εἰσι; κἀγώ· σπέρμα ᾿Αβραάμ εἰσι; κἀγώ·
gr_gr_cog~CO2~C011~023~διάκονοι Χριστοῦ εἰσι; παραφρονῶν λαλῶ, ὑπὲρ ἐγώ· ἐν κόποις περισσοτέρως, ἐν πληγαῖς ὑπερβαλλόντως, ἐν φυλακαῖς περισσοτέρως, ἐν θανάτοις πολλάκις·
gr_gr_cog~CO2~C011~024~ὑπὸ ᾿Ιουδαίων πεντάκις τεσσαράκοντα παρὰ μίαν ἔλαβον,
gr_gr_cog~CO2~C011~025~τρὶς ἐρραβδίσθην, ἅπαξ ἐλιθάσθην, τρὶς ἐναυάγησα, νυχθήμερον ἐν τῷ βυθῷ πεποίηκα·
gr_gr_cog~CO2~C011~026~ὁδοιπορίαις πολλάκις, κινδύνοις ποταμῶν, κινδύνοις λῃστῶν, κινδύνοις ἐκ γένους, κινδύνοις ἐξ ἐθνῶν, κινδύνοις ἐν πόλει, κινδύνοις ἐν ἐρημίᾳ, κινδύνοις ἐν θαλάσσῃ, κινδύνοις ἐν ψευδαδέλφοις·
gr_gr_cog~CO2~C011~027~ἐν κόπῳ καὶ μόχθῳ, ἐν ἀγρυπνίαις πολλάκις, ἐν λιμῷ καὶ δίψει, ἐν νηστείαις πολλάκις, ἐν ψύχει καὶ γυμνότητι·
gr_gr_cog~CO2~C011~028~χωρὶς τῶν παρεκτὸς ἡ ἐπισύστασίς μου ἡ καθ᾿ ἡμέραν, ἡ μέριμνα πασῶν τῶν ἐκκλησιῶν.
gr_gr_cog~CO2~C011~029~τίς ἀσθενεῖ, καὶ οὐκ ἀσθενῶ; τίς σκανδαλίζεται, καὶ οὐκ ἐγὼ πυροῦμαι;
gr_gr_cog~CO2~C011~030~εἰ καυχᾶσθαι δεῖ, τὰ τῆς ἀσθενείας μου καυχήσομαι.
gr_gr_cog~CO2~C011~031~ὁ Θεὸς καὶ πατὴρ τοῦ Κυρίου ἡμῶν ᾿Ιησοῦ Χριστοῦ οἶδεν, ὁ ὢν εὐλογητὸς εἰς τοὺς αἰῶνας, ὅτι οὐ ψεύδομαι.
gr_gr_cog~CO2~C011~032~ἐν Δαμασκῷ ὁ ἐθνάρχης ᾿Αρέτα τοῦ βασιλέως ἐφρούρει τὴν Δαμασκηνῶν πόλιν πιάσαι με θέλων,
gr_gr_cog~CO2~C011~033~καὶ διὰ θυρίδος ἐν σαργάνῃ ἐχαλάσθην διὰ τοῦ τείχους καὶ ἐξέφυγον τὰς χεῖρας αὐτοῦ.
gr_gr_cog~CO2~C012~001~Καυχᾶσθαι δὴ οὐ συμφέρει μοι· ἐλεύσομαι γὰρ εἰς ὀπτασίας καὶ ἀποκαλύψεις Κυρίου.
gr_gr_cog~CO2~C012~002~οἶδα ἄνθρωπον ἐν Χριστῷ πρὸ ἐτῶν δεκατεσσάρων· εἴτε ἐν σώματι οὐκ οἶδα, εἴτε ἐκτὸς τοῦ σώματος οὐκ οἶδα, ὁ Θεὸς οἶδεν· ἁρπαγέντα τὸν τοιοῦτον ἕως τρίτου οὐρανοῦ.
gr_gr_cog~CO2~C012~003~καὶ οἶδα τὸν τοιοῦτον ἄνθρωπον· εἴτε ἐν σώματι εἴτε ἐκτὸς τοῦ σώματος οὐκ οἶδα, ὁ Θεὸς οἶδεν·
gr_gr_cog~CO2~C012~004~ὅτι ἡρπάγη εἰς τὸν παράδεισον καὶ ἤκουσεν ἄρρητα ῥήματα, ἃ οὐκ ἐξὸν ἀνθρώπῳ λαλῆσαι.
gr_gr_cog~CO2~C012~005~ὑπὲρ τοῦ τοιούτου καυχήσομαι, ὑπὲρ δὲ ἐμαυτοῦ οὐ καυχήσομαι εἰ μὴ ἐν ταῖς ἀσθενείαις μου.
gr_gr_cog~CO2~C012~006~ἐὰν γὰρ θελήσω καυχήσασθαι, οὐκ ἔσομαι ἄφρων· ἀλήθειαν γὰρ ἐρῶ· φείδομαι δὲ μή τις εἰς ἐμὲ λογίσηται ὑπὲρ ὃ βλέπει με ἢ ἀκούει τι ἐξ ἐμοῦ.
gr_gr_cog~CO2~C012~007~Καὶ τῇ ὑπερβολῇ τῶν ἀποκαλύψεων ἵνα μὴ ὑπεραίρωμαι, ἐδόθη μοι σκόλοψ τῇ σαρκί, ἄγγελος σατᾶν, ἵνα με κολαφίζῃ ἵνα μὴ ὑπεραίρωμαι.
gr_gr_cog~CO2~C012~008~ὑπὲρ τούτου τρὶς τὸν Κύριον παρεκάλεσα ἵνα ἀποστῇ ἀπ᾿ ἐμοῦ·
gr_gr_cog~CO2~C012~009~καὶ εἴρηκέ μοι· ἀρκεῖ σοι ἡ χάρις μου· ἡ γὰρ δύναμίς μου ἐν ἀσθενείᾳ τελειοῦται. ἥδιστα οὖν μᾶλλον καυχήσομαι ἐν ταῖς ἀσθενείαις μου, ἵνα ἐπισκηνώσῃ ἐπ᾿ ἐμὲ ἡ δύναμις τοῦ Χριστοῦ.
gr_gr_cog~CO2~C012~010~διὸ εὐδοκῶ ἐν ἀσθενείαις, ἐν ὕβρεσιν, ἐν ἀνάγκαις, ἐν διωγμοῖς, ἐν στενοχωρίαις, ὑπὲρ Χριστοῦ· ὅταν γὰρ ἀσθενῶ, τότε δυνατός εἰμι.
gr_gr_cog~CO2~C012~011~Γέγονα ἄφρων καυχώμενος! ὑμεῖς με ἠναγκάσατε. ἐγὼ γὰρ ὤφειλον ὑφ᾿ ὑμῶν συνίστασθαι· οὐδὲν γὰρ ὑστέρησα τῶν ὑπερλίαν ἀποστόλων, εἰ καὶ οὐδέν εἰμι.
gr_gr_cog~CO2~C012~012~τὰ μὲν σημεῖα τοῦ ἀποστόλου κατειργάσθη ἐν ὑμῖν ἐν πάσῃ ὑπομονῇ, ἐν σημείοις καὶ τέρασι καὶ δυνάμεσι.
gr_gr_cog~CO2~C012~013~τί γάρ ἐστιν ὃ ἡττήθητε ὑπὲρ τὰς λοιπὰς ἐκκλησίας, εἰ μὴ ὅτι αὐτὸς ἐγὼ οὐ κατενάρκησα ὑμῶν; χαρίσασθέ μοι τὴν ἀδικίαν ταύτην.
gr_gr_cog~CO2~C012~014~᾿Ιδοὺ τρίτον ἑτοίμως ἔχω ἐλθεῖν πρὸς ὑμᾶς, καὶ οὐ καταναρκήσω ὑμῶν· οὐ γὰρ ζητῶ τὰ ὑμῶν, ἀλλὰ ὑμᾶς. οὐ γὰρ ὀφείλει τὰ τέκνα τοῖς γονεῦσι θησαυρίζειν, ἀλλ᾿ οἱ γονεῖς τοῖς τέκνοις.
gr_gr_cog~CO2~C012~015~ἐγὼ δὲ ἥδιστα δαπανήσω καὶ ἐκδαπανηθήσομαι ὑπὲρ τῶν ψυχῶν ὑμῶν, εἰ καὶ περισσοτέρως ὑμᾶς ἀγαπῶν ἧττον ἀγαπῶμαι.
gr_gr_cog~CO2~C012~016~῎Εστω δέ, ἐγὼ οὐ κατεβάρησα ὑμᾶς, ἀλλ᾿ ὑπάρχων πανοῦργος δόλῳ ὑμᾶς ἔλαβον.
gr_gr_cog~CO2~C012~017~μή τινα ὧν ἀπέσταλκα πρὸς ὑμᾶς, δι᾿ αὐτοῦ ἐπλεονέκτησα ὑμᾶς;
gr_gr_cog~CO2~C012~018~παρεκάλεσα Τίτον καὶ συναπέστειλα τὸν ἀδελφόν· μήτι ἐπλεονέκτησεν ὑμᾶς Τίτος; οὐ τῷ αὐτῷ πνεύματι περιεπατήσαμεν; οὐ τοῖς αὐτοῖς ἴχνεσι;
gr_gr_cog~CO2~C012~019~Πάλιν δοκεῖτε ὅτι ὑμῖν ἀπολογούμεθα; κατενώπιον τοῦ Θεοῦ ἐν Χριστῷ λαλοῦμεν· τὰ δὲ πάντα, ἀγαπητοί, ὑπὲρ τῆς ὑμῶν οἰκοδομῆς.
gr_gr_cog~CO2~C012~020~φοβοῦμαι γὰρ μήπως ἐλθὼν οὐχ οἵους θέλω εὕρω ὑμᾶς, κἀγὼ εὑρεθῶ ὑμῖν οἷον οὐ θέλετε, μήπως ἔρεις, ζῆλοι, θυμοί, ἐριθεῖαι, καταλαλιαί, ψιθυρισμοί, φυσιώσεις, ἀκαταστασίαι,
gr_gr_cog~CO2~C012~021~μὴ πάλιν ἐλθόντα με ταπεινώσῃ ὁ Θεός μου πρὸς ὑμᾶς καὶ πενθήσω πολλοὺς τῶν προημαρτηκότων καὶ μὴ μετανοησάντων ἐπὶ τῇ ἀκαθαρσίᾳ καὶ πορνείᾳ καὶ ἀσελγείᾳ ᾗ ἔπραξαν.
gr_gr_cog~CO2~C013~001~Τρίτον τοῦτο ἔρχομαι πρὸς ὑμᾶς·ἐπὶ στόματος δύο μαρτύρων καὶ τριῶν σταθήσεται πᾶν ῥῆμα·
gr_gr_cog~CO2~C013~002~προείρηκα καὶ προλέγω, ὡς παρὼν τὸ δεύτερον, καὶ ἀπὼν νῦν γράφω τοῖς προημαρτηκόσι καὶ τοῖς λοιποῖς πᾶσιν, ὅτι ἐὰν ἔλθω εἰς τὸ πάλιν οὐ φείσομαι,
gr_gr_cog~CO2~C013~003~ἐπεὶ δοκιμὴν ζητεῖτε τοῦ ἐν ἐμοὶ λαλοῦντος Χριστοῦ, ὃς εἰς ὑμᾶς οὐκ ἀσθενεῖ, ἀλλὰ δυνατεῖ ἐν ὑμῖν.
gr_gr_cog~CO2~C013~004~καὶ γὰρ εἰ ἐσταυρώθη ἐξ ἀσθενείας, ἀλλὰ ζῇ ἐκ δυνάμεως Θεοῦ· καὶ γὰρ ἡμεῖς ἀσθενοῦμεν ἐν αὐτῷ, ἀλλὰ ζησόμεθα σὺν αὐτῷ ἐκ δυνάμεως Θεοῦ εἰς ὑμᾶς.
gr_gr_cog~CO2~C013~005~῾Εαυτοὺς πειράζετε εἰ ἐστὲ ἐν τῇ πίστει, ἑαυτοὺς δοκιμάζετε. ἢ οὐκ ἐπιγινώσκετε ἑαυτοὺς ὅτι ᾿Ιησοῦς Χριστὸς ἐν ὑμῖν ἐστιν; εἰ μή τι ἀδόκιμοί ἐστε.
gr_gr_cog~CO2~C013~006~ἐλπίζω δὲ ὅτι γνώσεσθε ὅτι ἡμεῖς οὐκ ἐσμὲν ἀδόκιμοι.
gr_gr_cog~CO2~C013~007~εὔχομαι δὲ πρὸς τὸν Θεὸν μὴ ποιῆσαι ὑμᾶς κακὸν μηδέν, οὐχ ἵνα ἡμεῖς δόκιμοι φανῶμεν, ἀλλ᾿ ἵνα ὑμεῖς τὸ καλὸν ποιῆτε, ἡμεῖς δὲ ὡς ἀδόκιμοι ὦμεν.
gr_gr_cog~CO2~C013~008~οὐ γὰρ δυνάμεθά τι κατὰ τῆς ἀληθείας, ἀλλ᾿ ὑπὲρ τῆς ἀληθείας.
gr_gr_cog~CO2~C013~009~χαίρομεν γὰρ ὅταν ἡμεῖς ἀσθενῶμεν, ὑμεῖς δὲ δυνατοὶ ἦτε· τοῦτο δὲ καὶ εὐχόμεθα, τὴν ὑμῶν κατάρτισιν.
gr_gr_cog~CO2~C013~010~Διὰ τοῦτο ταῦτα ἀπὼν γράφω, ἵνα παρὼν μὴ ἀποτόμως χρήσωμαι κατὰ τὴν ἐξουσίαν ἣν ἔδωκέ μοι ὁ Κύριος εἰς οἰκοδομὴν καὶ οὐκ εἰς καθαίρεσιν.
gr_gr_cog~CO2~C013~011~Λοιπόν, ἀδελφοί, χαίρετε, καταρτίζεσθε, παρακαλεῖσθε, τὸ αὐτὸ φρονεῖτε, εἰρηνεύετε, καὶ ὁ Θεὸς τῆς ἀγάπης καὶ εἰρήνης ἔσται μεθ᾿ ὑμῶν.
gr_gr_cog~CO2~C013~012~᾿Ασπάσασθε ἀλλήλους ἐν ἁγίῳ φιλήματι. ἀσπάζονται ὑμᾶς οἱ ἅγιοι πάντες.
gr_gr_cog~CO2~C013~013~῾Η χάρις τοῦ Κυρίου ᾿Ιησοῦ Χριστοῦ καὶ ἡ ἀγάπη τοῦ Θεοῦ καὶ ἡ κοινωνία τοῦ ῾Αγίου Πνεύματος μετὰ πάντων ὑμῶν· ἀμήν.
